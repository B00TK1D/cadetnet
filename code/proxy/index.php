<?php
    include_once("proxy.php");

    $explode = explode(".", $_SERVER['HTTP_HOST']);
    $subdomain = array_shift(($explode));

    $resolve = [
        "" => "web",
        "www" => "web",
        "cadetnet" => "web",
        "dev" => "web",
        "cs[0-4][0-9]" => "web",
        "cw" => "web",
        "admin" => "web",
        "dns" => "dns",
        "maintainer" => "maintainer:8080",
        "vpn" => "https://vpn:943"
    ];

    if (isset($resolve[$subdomain])) {
        proxy("http://" . $resolve[$subdomain] . $_SERVER["REQUEST_URI"]);
    }

    if (preg_match('/cs[0-9][0-9]/', $subdomain)) {
        proxy("http://" . "web" . $_SERVER["REQUEST_URI"], ["CadetNet-Target-Network: " . substr($subdomain, -2)]);
    }
    //include('unavailable.php');
    //proxy("http://web" . $_SERVER["REQUEST_URI"]);
    //print($subdomain);
?>