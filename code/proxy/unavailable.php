<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8" />
    <title>CadetNet</title>
    <meta content="CadetNet" property="og:title" />
    <meta content="CadetNet" property="twitter:title" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <link href="/style.css?hash=<?php print(md5_file($_SERVER['DOCUMENT_ROOT'] . '/style.css')); ?>" rel="stylesheet" type="text/css" />
    <script src="/js/header.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-02Q4GJRYZR"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-02Q4GJRYZR');
    </script>
    <link href="/img/icon%2032.png" rel="shortcut icon" type="image/x-icon" />
    <link href="/img/icon%20256.png" rel="apple-touch-icon" />
    </head>
    <body class="body-2">
    <div class="div-block-61">
    <div class="div-block-62"></div>
    <div class="div-block-60">
        <h1 class="heading-18">UNAVAILABLE</h1>
    </div>
    <div class="text-block-18">The requested subdomain is not available.</div>
    </body>
</html>