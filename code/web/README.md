# Repository for the backend application for afcgoc.org


# PHP Framework Layout

### config.php
 - Stores all configuration parameters for connections to things such as mySQL, sendgrid, etc.
 - Stores various enums for use across PHP files.

 ### db.php
 - Provides connection to mySQL
 - Stores all queries in single list for simplified management, grouped by schema item

### mail.php
 - Handles all mail send tasks
 - Serves as connection to sendgrid and performs dynamic content mapping

 ### functs.php
  - Contains misc. functions, primarily providing signup and login functionality
  - init() should be called at the start of every API call in order to check credentials
  

