<?php function credentialsBlock($admin) { ?>

    <div class="div-block-63"></div>
    <div class="div-block-64">
      <div>
        <h1 class="heading-19">CREDENTIALS</h1>


        <?php if ($admin["tier"] == config("admin-tier.global")) { foreach (readObject("credentials") as $credential) { ?>
          <?php $network = readObject("networks", ["id" => $credential["network"]], 1); ?>

          <div class="div-block-65">
            <div class="text-block-16"><?php print($network["name"] . ': ' . $credential['name']); ?></div>
            <div class="div-block-66 seperate">
              <div class="cred-holder">
                <div class="text-block-16">••••••••••••••••</div>
                <div class="text-block-16"><?php print($credential['username']); ?></div>
              </div>
            </div>
            <div class="div-block-66 action-container">
              <a href="/admin/credential/show?credential=<?php print($credential['id']); ?>" class="button-5 _2 reveal-button w-button"></a>
              <a href="<?php print($credential['link']); ?>" target="_blank" class="button-5 login-icon-button w-button"></a>
              <a class="button-5 edit-device-button w-button"></a>
              <a class="button-5 _2 delete-button w-button"></a>
              <div class="delete-confirmation delete-confirm">
                <div class="div-block-65">
                  <div class="text-block-16-copy">Delete credential <?php print($credential['name']); ?>?</div>
                  <div class="div-block-66">
                    <a class="cn-button _2 cancel-delete w-button">CANCEL</a>
                    <form action="/api/credential/delete" method="post" target="transFrame">
                      <input type="text" name="credential" value="<?php echo $credential['id'];?>" hidden="">
                      <input type="submit" class="cn-button apply-delete w-button" value="APPLY">
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div class="edit-device">
              <div class="form-block w-form">
              <form action="/api/credential/update" target="transFrame" method="post">
                  <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Service Name"required="" value="<?php print($credential['name']); ?>"/>
                  <input type="text" class="cn-field w-input" maxlength="256" name="link" placeholder="Login Link" required="" value="<?php print($credential['link']); ?>" />
                  <input type="text" class="cn-field w-input" maxlength="256" name="username" placeholder="Username / Email" required="" value="<?php print($credential['username']); ?>" />
                  <input type="password" class="cn-field w-input" maxlength="256" name="password" placeholder="Password (Blank for unchanged)" />
                  <input type="password" class="cn-field w-input" maxlength="256" name="confirm-password" placeholder="Confirm Password" />
                  <input type="text" name="credential" value="<?php echo $credential['id'];?>" hidden="">
                  <div class="div-block-66">
                    <a class="cn-button _2 cancel-edit-device w-button">CANCEL</a>
                    <input type="submit" class="cn-button apply-edit-user w-button" value="APPLY">
                  </div>
                </form>
              </div>
            </div>
          </div>


          <?php } ?>

          <div class="div-block-65 new-device">
            <div class="form-block w-form">
              <form action="/api/credential/create" target="transFrame" method="post">
                <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Service Name" required="" />
                <input type="text" class="cn-field w-input" maxlength="256" name="link" placeholder="Login Link" required="" />
                <input type="text" class="cn-field w-input" maxlength="256" name="username" placeholder="Username / Email" required="" />
                <input type="password" class="cn-field w-input" maxlength="256" name="password" placeholder="Password" required="" />
                <input type="password" class="cn-field w-input" maxlength="256" name="confirm-password" placeholder="Confirm Password" required="" />
                <select name="network" class="cn-field _2 w-select">
                  <option value="0" style="display:none">Select Network</option>
                  <?php foreach ($admin["networks"] as $network) { ?>
                    <option value="<?php print($network['id']); ?>"><?php print($network['name']); ?></option>
                  <?php } ?>
                  </select>
                <div class="div-block-66">
                      <a class="cn-button _2 cancel-new-device w-button">CANCEL</a>
                      <input type="submit" class="cn-button apply-edit-user w-button" value="APPLY">
                    </div>
              </form>
            </div>
          </div>
            <a class="div-block-65 hover new-device-button w-inline-block">
              <div class="div-block-66-copy">
                <div class="button-5 _3"></div>
              </div>
            </a>
          </div>
        </div>


        <?php } else { foreach (readObject("credentials", ["network" => $network]) as $credential) { ?>


          <div class="div-block-65">
            <div class="text-block-16"><?php print($credential['name']); ?></div>
            <div class="div-block-66 seperate">
              <div class="cred-holder">
                <div class="text-block-16">••••••••••••••••</div>
                <div class="text-block-16"><?php print($credential['username']); ?></div>
              </div>
            </div>
            <div class="div-block-66 action-container">
              <a href="/admin/credential/show?c=<?php print($credential['id']); ?>" class="button-5 _2 reveal-button w-button"></a>
              <a href="<?php print($credential['link']); ?>" target="_blank" class="button-5 login-icon-button w-button"></a>
              <a class="button-5 edit-device-button w-button"></a>
              <a class="button-5 _2 delete-button w-button"></a>
              <div class="delete-confirmation delete-confirm">
                <div class="div-block-65">
                  <div class="text-block-16-copy">Delete credential <?php print($credential['name']); ?>?</div>
                  <div class="div-block-66">
                    <a class="cn-button _2 cancel-delete w-button">CANCEL</a>
                    <form action="/api/credential/delete" method="post" target="transFrame">
                      <input type="text" name="credential" value="<?php echo $credential['id'];?>" hidden="">
                      <input type="submit" class="cn-button apply-delete w-button" value="APPLY">
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div class="edit-device">
              <div class="form-block w-form">
              <form action="/api/credential/update" target="transFrame" method="post">
                  <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Service Name"required="" value="<?php print($credential['name']); ?>"/>
                  <input type="text" class="cn-field w-input" maxlength="256" name="link" placeholder="Login Link" required="" value="<?php print($credential['link']); ?>" />
                  <input type="text" class="cn-field w-input" maxlength="256" name="username" placeholder="Username / Email" required="" value="<?php print($credential['username']); ?>" />
                  <input type="password" class="cn-field w-input" maxlength="256" name="password" placeholder="Password (Blank for unchanged)" />
                  <input type="password" class="cn-field w-input" maxlength="256" name="confirm-password" placeholder="Confirm Password" />
                  <input type="text" name="credential" value="<?php echo $credential['id'];?>" hidden="">
                  <div class="div-block-66">
                    <a class="cn-button _2 cancel-edit-device w-button">CANCEL</a>
                    <input type="submit" class="cn-button apply-edit-user w-button" value="APPLY">
                  </div>
                </form>
              </div>
            </div>
          </div>


          <?php } ?>

          <div class="div-block-65 new-device">
            <div class="form-block w-form">
              <form action="/api/credential/create" target="transFrame" method="post">
                <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Service Name" required="" />
                <input type="text" class="cn-field w-input" maxlength="256" name="link" placeholder="Login Link" required="" />
                <input type="text" class="cn-field w-input" maxlength="256" name="username" placeholder="Username / Email" required="" />
                <input type="password" class="cn-field w-input" maxlength="256" name="password" placeholder="Password" required="" />
                <input type="password" class="cn-field w-input" maxlength="256" name="confirm-password" placeholder="Confirm Password" required="" />
                <input type="text" maxlength="256" name="network" value="<?php print($admin["network"]); ?>" required="" hidden="" />
                <div class="div-block-66">
                      <a class="cn-button _2 cancel-new-device w-button">CANCEL</a>
                      <input type="submit" class="cn-button apply-edit-user w-button" value="APPLY">
                    </div>
              </form>
            </div>
          </div>
            <a class="div-block-65 hover new-device-button w-inline-block">
              <div class="div-block-66-copy">
                <div class="button-5 _3"></div>
              </div>
            </a>
          </div>
        </div>


      <?php } ?>

      </div>

      <script src="/js/jquery.js"></script>
      <script>

      // Edit device
      $(".edit-device-button").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "auto";
          });
        });

        $(".cancel-edit-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
          });
        });

        $(".apply-edit-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
          });
        });

        // New device
        $(".new-device-button").each(function(index) {
          this.addEventListener("click", function() {
            this.style.height = "0px";
            this.parentNode.getElementsByClassName("new-device")[0].style.height = "auto";
          });
        });

        $(".cancel-new-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

          });
        });

        $(".apply-new-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

          });
        });


        $(".delete-button").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "flex";
          });
        });

        $(".cancel-delete").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
          });
        });

        $(".apply-delete").each(function(index) {
          this.addEventListener("click", function() {
            //this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
          });
        });


        $('[type=submit]').each(function(index) {
            this.addEventListener("click", function() {
                setTimeout(function () {location.reload()}, 1000);
          });
        });


    </script>

<?php }  function showCredentialBlock($credential) { ?>

    <div class="div-block-63"></div>
    <div class="div-block-64">
      <div>
        <h1 class="heading-19">CREDENTIAL REVEAL</h1>
        <div class="div-block-65">
          <div class="text-block-16"><?php print($credential['name']); ?></div>
          <div class="div-block-66 seperate">
            <div class="cred-holder">
              <div class="text-block-16"><?php print($credential['username']); ?></div>
              <div class="text-block-16"><?php print($credential['decrypted']); ?></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="div-block-68">
      <div>
        <div class="div-block-66"><a href="/admin/credential/list" class="cn-button apply-edit-user w-button">BACK</a></div>
      </div>
    </div>

<?php } ?>