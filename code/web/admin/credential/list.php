<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["functs", "elements"]);

    $admin = authenticate("admins")["admin"];
    
    $admin["networks"] = readObject("networks");

    head("CadetNet Credentials", ["DASHBOARD" => "/admin/dashboard", "LOGOUT" => "/admin/logout"]);
    credentialsBlock($admin);
    foot();
?>