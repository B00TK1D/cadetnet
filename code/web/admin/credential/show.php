<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["functs", "elements"]);
    includeUtil("crypto");

    $credential = param("credential", ["displayError" => true, "sticky" => true]);
    $credential = readObject("credentials", ["id" => $credential], 1);
    
    authenticate("owners", ["network" => $credential["network"]]);

    $credential['decrypted'] = decrypt($credential["cred"]);

    head("CadetNet Credentials", ["DASHBOARD" => "/admin/dashboard", "LOGOUT" => "/admin/logout"]);
    showCredentialBlock($credential);
    foot();
?>