<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["functs", "elements"]);

    $admin = authenticate("admins")["admin"];
    $network = [];
    $select = [];
    $selected = [];
    
    $menu = [];

    $menu["ISSUES"] = "/support/issue/list";

    if ($admin["tier"] == config("admin-tier.global")) {
        $menu["MASTER"] = "/admin/master/dashboard";
        $select["network"] = readObject("networks");
        $network["id"] = param(["network", $admin["network"]], ["sticky" => true]);
    } else {
        $network = readObject("networks", ["id" => $admin["network"]], -1);
    }
    $selected["network"] = $network;

    $menu["LOGOUT"] = "/admin/logout";

    $users = listUsers($network["id"]);
    $network = getUnit($network["id"]);

    head("CadetNet Admin", $menu, $select, $selected);
    adminDashboardBlock($users, $network, $admin);
    foot();
?>