<?php

    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");


    function adminLoginBlock($code, $redirect) {
  
?>

    <div class="div-block-61">
      <div class="div-block-62"></div>
      <div class="div-block-60">
        <h1 class="heading-18">     ADMIN LOGIN      </h1>
          <form id="login" name="login" action="/admin/login" method="post">
            <label for="email" class="cn-label">Email Address</label><input type="email" class="cn-field w-input" maxlength="256" name="email" placeholder="" id="email" required="" />
            <label for="password" class="cn-label">Password</label><input type="password" class="cn-field w-input" maxlength="256" name="password" placeholder="" id="password" required="" />
            <?php if($code == code("invalid-credentials")) { ?>
                <a href="/admin/password/request" class="submit-button-2 _3 w-button">Reset Password</a>
            <?php } ?>
            <?php if($redirect) { ?>
                <input type="text" name="redirect" hidden="" value="<?php print($redirect); ?>"/>
            <?php } ?>
            <input type="submit" value="Login" data-wait="Please wait..." class="submit-button-2 w-button" />
          </form>
          <div class="text-block-18">
            <?php print(message($code)); ?>
          </div>
        <div class="footer">
        <div class="div-block-79">
          <div class="text-block-19">Looking for standard user dashboard?</div><a href="/user/login" class="submit-button-2 _2 w-button">Switch</a>
        </div>
      </div>
    </div>



<?php } function notificationBlock() { ?>


<script type="text/javascript" src="/js/notification.js"></script>
<div class="info-container">
  <div>
    <h1 class="heading-18">NOTIFICATIONS</h1>
  </div>
  <div class="div-block-65">
    <div>
      <div class="text-block-18">Allow Notifications</div>
      <div class="text-block-16"> <br />CadetNet needs to be able to send you notifications as an admin to alert you about issues on your network<br />‍</div>
      <div class="text-block-16"> <br />Please allow notifications in order to ensure we can communicate with you.<br />‍</div>
    </div>
  </div>
  <div class="div-block-68">
    <div>
      <div class="div-block-66"><a class="cn-button apply-edit-user w-button" id="push-subscription-button">ALLOW NOTIFICATIONS</a></div>
    </div>
  </div>
</div>
<div class="footer">
<div class="div-block-79">
  <div class="text-block-19"></div><a id="next" href="/admin/dashboard" class="submit-button-2 _2 w-button">Skip</a>
</div>
</div>



<?php

    } 
    

    function adminOnboardBlock($token) {
      
?>

    <div class="div-block-61">
      <div class="div-block-62"></div>
      <div class="div-block-60">
        <h1 class="heading-18">SET ADMIN PASSWORD</h1>
        <div class="w-form">
          <form action="/api/admin/onboard" method="post">
            <label for="password" class="cn-label">Password</label>
            <input type="password" class="cn-field w-input" maxlength="256" name="password" placeholder="" id="password" required="" />
            <label for="confirm-password" class="cn-label">Confirm Password</label>
            <input type="password" class="cn-field w-input" maxlength="256" name="confirm-password" placeholder="" id="confirm-password" required="" />
            <input type="text" name="token" value="<?php print($token)?>" hidden=""/>
            <input type="submit" value="Finish" data-wait="Please wait..." class="submit-button-2 w-button" />
          </form>
        </div>
        <div class="text-block-18" id="error"></div>
      </div>
    </div>
    <script src="/js/jquery.js"></script>
    <script>
        $("[type=submit]").each(function(index) {
          this.addEventListener("click", function(event) {
            if (document.getElementById('password').value != document.getElementById('confirm-password').value) {
                document.getElementById('error').innerHTML = "Error: Password entries do not match";
                event.preventDefault();
            }
          });
        });
    </script>

<?php

    } 
    
    
    function adminDashboardBlock($users, $network, $admin) {
      
?>

    <div class="div-block-63"></div>
      <div class="div-block-64">
        <div>
          <h1 class="heading-19">USERS</h1>
          
          <?php foreach ($users as $luser) { ?>


          <div class="div-block-65 user-box">
            <div class="div-block-74">
              <div style="margin-right: 10px">
                  <div class="text-block-16 bold"><?php print($luser["name"]); ?></div>
                  <div class="text-block-16 small"><?php print($luser["email"]); ?></div>
                  <?php if ($luser["status"] != config("verification.verified") && $token = readObject("tokens", ["user" => $luser['id'], "purpose" => config('token.purpose.verification')], 1)) { ?>
                      <a class="text-block-16 small" target="_blank" href="mailto:<?php print($luser['email']);?>?subject=CadetNet%20Email%20Verification%20(Manually%20Sent)&body=Please%20copy%20and%20paste%20the%20following%20link%20to%20finish%20setting%20up%20your%20CadetNet%20account:%20https://cadetnet.org/verify?token=<?php print($token['string']);?>">Send Verification</a>
                  <?php } ?>
              </div>
              <div class="div-block-66 small"><a class="button-5 small edit-user-button w-button"></a><a class="button-5 _2 small delete-button w-button"></a>
                <div class="delete-confirmation delete-confirm _2">
                  <div class="div-block-65">
                    <div class="text-block-16-copy">Delete user <?php echo $luser["name"]; ?>?</div>
                    <div class="div-block-66"><a class="cn-button _2 cancel-delete w-button">CANCEL</a><form action="/api/user/delete" method="post" target="transFrame"><input type="text" name="user" value="<?php echo $luser['id'];?>" hidden=""><input type="submit" class="cn-button apply-delete w-button" value="DELETE"></form></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-block _2 edit-user w-form">
              <form action="/api/user/update" target="transFrame" method="post">
                <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="User Name" required="" value="<?php print($luser['name'])?>"/>
                <input type="email" class="cn-field w-input" maxlength="256" name="email" placeholder="Email" required="" value="<?php print($luser['email'])?>"/>
                <input type="text" name="user" value="<?php print($luser['id'])?>" hidden=""/>
                <div class="div-block-66">
                  <a class="cn-button _2 cancel-edit-user w-button">CANCEL</a>
                  <input type="submit" class="cn-button apply-edit-user w-button" value="APPLY">
                </div>
              </form>
            </div>


            <?php foreach ($luser["devices"] as $device) { ?>


            <div class="user-device">
              <div class="div-block-70"></div>
              <div class="div-block-73">
                <div class="div-block-66 small">
                  <a class="button-5 small edit-device-button w-button"></a>
                  <a class="button-5 _2 small delete-button w-button"></a>
                  <div class="delete-confirmation delete-confirm">
                    <div class="div-block-65">
                      <div class="text-block-16-copy">Delete device <?php echo $device["name"]?>?</div>
                      <div class="div-block-66">
                        <a class="cn-button _2 cancel-delete w-button">CANCEL</a>
                        <form action="/api/device/delete" method="post" target="transFrame">
                          <input type="text" name="device" value="<?php echo $device['id'];?>" hidden="">
                          <input type="text" name="user" value="<?php echo $luser['id'];?>" hidden="">
                          <input type="submit" class="cn-button apply-delete w-button" value="APPLY">
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="div-block-72">
                  <div class="text-block-16 small"><?php echo $device["name"]?></div>
                  <div class="text-block-16 small"><?php echo $device["mac"]?></div>
                </div>
              </div>
              <div class="form-block _2 edit-device w-form validate-form">
                <form action="/api/device/update" method="post" target="transFrame">
                  <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Device Name" required="" value="<?php print($device['name'])?>"/>
                  <input type="text" class="cn-field w-input mac-format" maxlength="20" name="mac" placeholder="MAC" required="" value="<?php print($device['mac'])?>"/>
                  <input hidden="" type="text" name="user" value="<?php print($luser['id'])?>"/>
                  <input hidden="" type="text" name="device" value="<?php print($device['id'])?>"/>
                  <div class="div-block-66"><a class="cn-button _2 cancel-edit-device w-button">CANCEL</a><input type="submit"  class="cn-button apply-edit-device w-button" value="APPLY"></div>
                </form>
              </div>
            </div>

          <?php } ?>

          <div class="div-block-76 new-device">
              <div class="form-block _2 new-device w-form">
                <form class="validate-form" action="/api/device/create" method="post" target="transFrame"><input type="text" class="cn-field w-input validate-form" maxlength="256" name="name" placeholder="Device Name" required="" /><input type="text" class="cn-field w-input mac-format" maxlength="20" name="mac" placeholder="MAC" required=""/><input hidden="" type="text" name="user" value="<?php echo $luser['id']?>"/>
                  <div class="div-block-66"><a class="cn-button _2 cancel-new-device w-button">CANCEL</a><input type="submit" class="cn-button apply-new-user w-button" value="APPLY"></div>
                </form>
              </div>
              <a class="div-block-65 hover small new-device-button w-inline-block">
                <div class="div-block-66-copy _2">
                  <div class="button-5 _3"></div>
                </div>
              </a>
            </div>
          </div>


          <?php } ?>


        </div>
        <div class="div-block-69"></div>
        <div class="div-block-67">
          <h1 class="heading-19">SUMMARY</h1>

          <div class="div-block-65-copy">
            <div class="text-block-16">Network <?php print($network["name"])?></div>
            <div class="text-block-16"><?php print($network["userCount"])?> Users</div>
          </div>
          <div class="div-block-65-copy">
              <div class="text-block-16">Balance: $<?php print($network["balance"])?></div>
              <div class="text-block-16">Projected $<?php print($network["projected"])?> Monthly Income</div>
          </div>
          <div class="div-block-65-copy">
            <div class="text-block-16"><a class="text-block-16" href="https://internet.xfinity.com/network/advanced-settings" target="_blank">XFinity Settings</a></div>
          </div>
          <div class="div-block-65-copy">
              <div class="text-block-16"><a class="text-block-16" href="https://instant.arubanetworks.com" target="_blank">Aruba Management</a></div>
          </div>
          <div class="div-block-65-copy">
              <div class="text-block-16"><a class="text-block-16" href="/admin/credential/list" target="_blank">Credentials Portal</a></div>
          </div>
          <div class="div-block-65-copy">
              <div class="text-block-16"><a class="text-block-16" href="/admin/search/user" target="_blank">User Search</a></div>
          </div>
          <div class="div-block-65-copy">
              <div class="text-block-16"><a class="text-block-16" href="/equipment/map" target="_blank">Equipment Map</a></div>
          </div>
          <div class="div-block-65-copy">
            <div class="text-block-16">Saved IP Address: <?php print($network["ip"])?></div>
            <div class="text-block-16">Your IP Address: <?php print($_SERVER["HTTP_X_FORWARDED_HOST"])?></div>
            <form action="/api/network/update" method="post" target="transFrame">
                <input type="text" name="network" value="<?php print($network["id"]) ?>" hidden=""/>
                <input type="text" name="ip" value="<?php print($_SERVER["HTTP_X_FORWARDED_HOST"]) ?>" hidden=""/>
                <div class="div-block-66"><input type="submit" class="cn-button apply-new-user w-button" value="UPDATE IP"></div>
              </form>
          </div>
          <div class="div-block-65-copy">
          </div>
        </div>
      </div>
      <div class="div-block-68"></div>
      <div class="w-embed w-script">
        <script src="/js/jquery.js"></script>

        <script>
          // Edit user
          $(".edit-user-button").each(function(index) {
            this.addEventListener("click", function() {
              this.parentNode.parentNode.parentNode.getElementsByClassName("edit-user")[0].style.height = "auto";
            });
          });

          $(".cancel-edit-user").each(function(index) {
            this.addEventListener("click", function() {
              this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-user")[0].style.height = "0px";
            });
          });

          $(".apply-edit-user").each(function(index) {
            this.addEventListener("click", function() {
              this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-user")[0].style.height = "0px";
            });
          });

          // Edit device
          $(".edit-device-button").each(function(index) {
            this.addEventListener("click", function() {
              this.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "auto";
            });
          });

          $(".cancel-edit-device").each(function(index) {
            this.addEventListener("click", function() {
              this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
            });
          });

          $(".apply-edit-device").each(function(index) {
            this.addEventListener("click", function() {
              this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
            });
          });

          // New device
          $(".new-device-button").each(function(index) {
            this.addEventListener("click", function() {
              this.style.height = "0px";
              this.parentNode.getElementsByClassName("new-device")[0].style.height = "auto";
            });
          });

          $(".cancel-new-device").each(function(index) {
            this.addEventListener("click", function() {
              this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
              this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

            });
          });

          $(".apply-new-device").each(function(index) {
            this.addEventListener("click", function() {
              this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
              this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

            });
          });

          $(".new-user-button").each(function(index) {
            this.addEventListener("click", function() {
              this.style.height = "0px";
              this.parentNode.getElementsByClassName("new-user")[0].style.height = "auto";
            });
          });

          $(".cancel-new-user").each(function(index) {
            this.addEventListener("click", function() {
              this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-user")[0].style.height = "0px";
              this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-user-button")[0].style.height = "auto";
            });
          });

          $(".apply-new-user").each(function(index) {
            this.addEventListener("click", function() {
              this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-user")[0].style.height = "0px";
              this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-user-button")[0].style.height = "auto";
            });
          });

          $(".delete-button").each(function(index) {
            this.addEventListener("click", function() {
              this.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "flex";
            });
          });

          $(".cancel-delete").each(function(index) {
            this.addEventListener("click", function() {
              this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
            });
          });

          $(".apply-delete").each(function(index) {
            this.addEventListener("click", function() {
              //this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
            });
          });

          $(".import-users-button").each(function(index) {
            this.addEventListener("click", function() {
              this.parentNode.getElementsByClassName("import-users")[0].style.display = "flex";
            });
          });

          $(".cancel-import").each(function(index) {
            this.addEventListener("click", function() {
              this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("import-users")[0].style.display = "none";
            });
          });

          $(".apply-import").each(function(index) {
            this.addEventListener("click", function() {
              this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("import-users")[0].style.display = "none";
            });
          });

          $('[type=submit]').each(function(index) {
              this.addEventListener("click", function() {
                  /*$.ajax({                            //Reload current page via ajax
                      url: location.href,
                      success: function(response) {
                          var dummy = $("<div></div>");      //Create a dummy html object to parse response
                          dummy.html(response);
                          new_body = $("body", dummy);
                          $("body").html(new_body);      //Replace 
                      }
                  });*/
                  setTimeout(function () {location.reload()}, 1000);
            });
          });

          $(".validate-form").each(function(index) {
              var form = this;
              $(".mac-format", this).each(function(elem) {
                  this.setAttribute("minLength", "17");
                  this.setAttribute("maxLength", "17");
                  this.addEventListener("input", function (e) {
                                            regex = /[a-fA-F0-9]/g;
                      if (this.value.length > 17 || !regex.test(this.value.slice(-1))) {
                          this.value = this.value.slice(0, -1);
                      } 
                      if (this.value.length % 3 == 0 && this.value.length > 0) {
                          this.value = this.value.slice(0, -1) + ":" + this.value.slice(-1);
                      }
                      this.value = this.value.toLowerCase();
                      if (this.value.length == 17) {
                        form.elements[form.elements.length - 1].disabled = false;
                      } else {
                          form.elements[form.elements.length - 1].disabled = true;
                      }
                  });
              });
          });

        </script>
        </div>

<?php

    }
    
?>