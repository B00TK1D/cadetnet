<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    function loginAdmin($email, $password) {
        $admin = readObject("admins", ["email" => $email], 1);
        if ($admin == null) {
            redirect("/admin/login?code=" . code("invalid-credentials"));
        }
        if ($admin["status"] < config("verification.verified")) {
            redirect("/admin/login?code=" . code("unverified"));
        }
        if (checkHash($password, $admin["password"])) {
            $_SESSION["admin"] = $admin["id"];
            renewTimeout();
            return code("good");
        } else {
            redirect("/admin/login?code=" . code("invalid-credentials"));
        }
        redirect("/admin/login?code=" . code("error"));
    }

    function logout() {
        $_SESSION["admin"] = -1;
        redirect("/admin/login?code=" . code("logged-out"));
    }


    function listUsers($network) {
        $users = [];
        $tempUsers = readObject("users", ["network" => $network]);
        foreach ($tempUsers as $tempUser) {
            $user = $tempUser;
            $user["devices"] = readObject("devices", ["user" => $user["id"]]);
            $users[] = $user;
        }
        return $users;
    }

    function getUnit($id) {
        $network = readObject("networks", ["id" => $id], 1);
        if ($network != null) {
            $network["balance"] = number_format((readObject("networks", ["id" => $id], 1)["balance"]/100), 2, '.', ' ');
            $network["projected"] = count(readObject("users", ["network" => $id, "status" => config("verification.verified")])) * 4.45;
        } else {
            $network = [];
            $network["name"] = "nonexistent";
            $network["network"] = 0;
            $network["balance"] = 0;
            $network["projected"] = 0;
            $network["ip"] = "0.0.0.0";
        }
        $network["userCount"] = count(readObject("users", ["network" => $id, "status" => config("verification.verified")]));
        return $network;
    }
?>