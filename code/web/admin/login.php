<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil(["message"]);
    includeAll(["elements", "functs"]);

    $email = param(["email", false]);
    $password = param(["password", false]);

    $code = param(["code", false]);
    $redirect = param(["redirect", false]);

    if ($email && $password) {
        loginAdmin($email, $password);
        if (param("notification-subscribed-admin", ["sticky" => true])) {
            redirect("/user/dashboard", $redirect);
        } else {
            setParam("notification-subscribed-admin", true, true);
            redirect("/user/register", $redirect);
        }
        
    }

    head("CadetNet Admin Login");
    adminLoginBlock($code, $redirect);
    foot();
?>