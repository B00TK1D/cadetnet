<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll("functs");
    includeUtil("message");

    logout();

    redirect("/admin/login?code=" . code("logged-out"));
?>