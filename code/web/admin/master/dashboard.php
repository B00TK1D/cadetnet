<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll("elements");
    includeUtil("composer");

    authenticate("global-admins");

    head("CadetNet Master", ["ADMIN" => "/admin/dashboard", "LOGOUT" => "/admin/logout"]);
    masterBlock();
    foot();
?>