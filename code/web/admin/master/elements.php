<?php function masterBlock() { ?>


    <div class="div-block-63"></div>
    <div class="div-block-64">
    <div>
        <h1 class="heading-19">SQUADS</h1>
        
        
        <?php foreach (readObject("networks") as $lnetwork) { ?>

        <div class="div-block-65 user-box">
          <div class="div-block-74">
            <div style="margin-right: 10px">
                <div class="text-block-16 bold"><?php print($lnetwork["name"]); ?></div>
                <div class="text-block-16 small">Balance: $<?php print(number_format(($lnetwork["balance"]) /100, 2, '.', ' ')); ?></div>
                <div class="text-block-16 small"><?php print($lnetwork["ip"]); ?></div>
                <div class="text-block-16 small">Floor <?php print($lnetwork["floor"]); ?></div>
                <div class="text-block-16 small">IP <?php print($lnetwork["ip"]); ?></div>
            </div>
            <div class="div-block-66 small"><a class="button-5 small edit-user-button w-button"></a><a class="button-5 _2 small delete-button w-button"></a>
              <div class="delete-confirmation delete-confirm _2">
                <div class="div-block-65">
                  <div class="text-block-16-copy">Delete network <?php echo $lnetwork["name"]; ?>?</div>
                  <div class="div-block-66"><a class="cn-button _2 cancel-delete w-button">CANCEL</a>
                  <form action="/api/network/delete" method="post" target="transFrame">
                    <input type="text" name="network" value="<?php echo $lnetwork['id'];?>" hidden="">
                    <input type="submit" class="cn-button apply-delete w-button" value="DELETE">
                  </form></div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-block _2 edit-user w-form">
            <form action="/api/network/update" target="transFrame" method="post">
              <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Network Name" value="<?php print($lnetwork['name'])?>"/>
              <input type="number" class="cn-field w-input" maxlength="256" name="balance" placeholder="Balance" required="" value="<?php print($lnetwork['balance'])?>"/>
              <input type="text" class="cn-field w-input" maxlength="256" name="card" placeholder="Card ID" value="<?php print($lnetwork['card'])?>"/>
              <input type="text" class="cn-field w-input" maxlength="256" name="ip" placeholder="IP Address" value="<?php print($lnetwork['ip'])?>"/>
              <input type="number" class="cn-field w-input" maxlength="4" name="floor" placeholder="Floor" value="<?php print($lnetwork['floor'])?>"/>
              <input type="text" name="network" value="<?php print($lnetwork['id'])?>" hidden=""/>
              <div class="div-block-66"><a class="cn-button _2 cancel-edit-user w-button">CANCEL</a><input type="submit" class="cn-button apply-edit-user w-button" value="APPLY"></div>
            </form>
          </div>
        </div>

        <?php } ?>

        <div class="div-block-65 new-user">
          <div class="form-block w-form">
            <form action="/api/network/create" method="post" target="transFrame">
              <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Network Name" required=""/>
              <input type="text" class="cn-field w-input" maxlength="16" name="card" placeholder="IP Address"/>
              <input type="number" class="cn-field w-input" maxlength="4" name="floor" placeholder="Floor"/>
              <input type="text" class="cn-field w-input" maxlength="256" name="card" placeholder="Card ID"/>
              <div class="div-block-66"><a class="cn-button _2 cancel-new-user w-button">CANCEL</a><input type="submit" class="cn-button apply-new-user w-button" value="APPLY"></div>
            </form>
          </div>
        </div><a class="div-block-65 hover new-user-button w-inline-block">
          <div class="div-block-66-copy _2">
            <div class="button-5 _3"></div>
          </div>
        </a>
      </div>


      <div class="div-block-69"></div>

      <div>
        <h1 class="heading-19">ADMINS</h1>
        
        
        <?php foreach (readObject("admins") as $luser) { ?>
        <?php
          $network = readObject("networks", ["id" => $luser["network"]], 1);
          if (!$network) $network["name"] = "Unset";
        ?>


        <div class="div-block-65 user-box">
          <div class="div-block-74">
            <div style="margin-right: 10px">
                <div class="text-block-16 bold"><?php print($luser["name"]); ?></div>
                <div class="text-block-16 small"><?php print($luser["email"]); ?></div>
                <div class="text-block-16 small"><?php print($network["name"]); ?></div>
            </div>
            <div class="div-block-66 small"><a class="button-5 small edit-user-button w-button"></a><a class="button-5 _2 small delete-button w-button"></a>
              <div class="delete-confirmation delete-confirm _2">
                <div class="div-block-65">
                  <div class="text-block-16-copy">Delete admin <?php echo $luser["name"]; ?>?</div>
                  <div class="div-block-66"><a class="cn-button _2 cancel-delete w-button">CANCEL</a><form action="/api/admin/delete" method="post" target="transFrame"><input type="text" name="admin" value="<?php echo $luser['id'];?>" hidden=""><input type="submit" class="cn-button apply-delete w-button" value="DELETE"></form></div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-block _2 edit-user w-form">
            <form action="/api/admin/update" target="transFrame" method="post">
              <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Admin Name" required="" value="<?php print($luser['name'])?>"/>
              <input type="email" class="cn-field w-input" maxlength="256" name="email" placeholder="Email" required="" value="<?php print($luser['email'])?>"/>
              <select name="tier" class="cn-field _2 w-select">
                <option <?php if ($luser['tier'] == config('admin-tier.global')) print('selected'); ?> value="<?php print(config('admin-tier.global')); ?>">Global Admin</option>
                <option <?php if ($luser['tier'] == config('admin-tier.network')) print('selected'); ?> value="<?php print(config('admin-tier.network')); ?>">Network Admin</option>
              </select>
              <select name="network" class="cn-field _2 w-select">
                <?php foreach (readObject("networks") as $lnetwork) { ?>
                  <option <?php if ($luser['network'] == $lnetwork['id']) print('selected'); ?> value="<?php print($lnetwork['id']); ?>"><?php print($lnetwork['name']); ?></option>
                <?php } ?>
              </select>
              <input type="text" name="admin" value="<?php print($luser['id'])?>" hidden=""/>
              <div class="div-block-66"><a class="cn-button _2 cancel-edit-user w-button">CANCEL</a><input type="submit" class="cn-button apply-edit-user w-button" value="APPLY"></div>
            </form>
          </div>
        </div>

        <?php } ?>

        <div class="div-block-65 new-user">
          <div class="form-block w-form">
            <form action="/api/admin/create" method="post" target="transFrame">
              <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Admin Name" required=""/>
              <input type="email" class="cn-field w-input" maxlength="256" name="email" placeholder="Email" required=""/>
              <select name="tier" class="cn-field _2 w-select">
                <option value="0" style="display:hidden">Select Tier</option>
                <option value="<?php print(config('admin-tier.global')); ?>">Global Admin</option>
                <option value="<?php print(config('admin-tier.network')); ?>">Network Admin</option>
              </select>
              <select name="network" class="cn-field _2 w-select">
                <option value="0" style="display:none">Select Network</option>
                <?php foreach (readObject("networks") as $lnetwork) { ?>
                  <option value="<?php print($lnetwork['id']); ?>"><?php print($lnetwork['name']); ?></option>
                <?php } ?>
              </select>
              <div class="div-block-66"><a class="cn-button _2 cancel-new-user w-button">CANCEL</a><input type="submit" class="cn-button apply-new-user w-button" value="APPLY"></div>
            </form>
          </div>
        </div><a class="div-block-65 hover new-user-button w-inline-block">
          <div class="div-block-66-copy _2">
            <div class="button-5 _3"></div>
          </div>
        </a>
      </div>

    <div class="div-block-69"></div>
      <div class="div-block-67">
        <h1 class="heading-19">SUMMARY</h1>
        <div class="div-block-65-copy">
          <div class="text-block-16">Global Info</div>
          <div class="text-block-16"><?php print(count(readObject("admins")))?> Admins</div>
          <div class="text-block-16"><?php print(count(readObject("users")))?> Total Users</div>
          <div class="text-block-16"><?php print(count(readObject("users", ["status" => config("verification.verified")])))?> Active Users</div>
          <div class="text-block-16"><?php print(count(readObject("devices")))?> Devices</div>
        </div>
        
        
        <div class="div-block-65-copy">
          <div class="text-block-16"><a class="text-block-16" href="https://internet.xfinity.com/network/advanced-settings" target="_blank">XFinity Settings</a></div>
        </div>
        <div class="div-block-65-copy">
            <div class="text-block-16"><a class="text-block-16" href="https://instant.arubanetworks.com" target="_blank">Aruba Management</a></div>
        </div>
        <div class="div-block-65-copy">
            <div class="text-block-16"><a class="text-block-16" href="/admin/credential/list" target="_blank">Credentials Portal</a></div>
        </div>
        <div class="div-block-65-copy">
            <div class="text-block-16"><a class="text-block-16" href="/admin/search/user" target="_blank">User Search</a></div>
        </div>
        <div class="div-block-65-copy">
            <div class="text-block-16"><a class="text-block-16" href="/equipment/map" target="_blank">Equipment Map</a></div>
        </div>
        <div class="div-block-65-copy">
            <div class="text-block-16">
              <form action="/api/notification/send" method="post" target="transFrame">
                <input type="title" class="cn-field w-input" maxlength="256" name="title" placeholder="Title" required="" hidden="" value="Service Outage"/>
                <input type="msg" class="cn-field w-input" maxlength="256" name="msg" placeholder="Message" required="" hidden="" value="CadetNet outage detected in your area"/>
                <input type="submit" class="cn-button apply-edit-user w-button" value="BLAST NOTIFICATION">
              </form>
            </div>
        </div>
      </div>
    </div>
    <div class="div-block-68"></div>
    <div class="w-embed w-script">
      <script src="/js/jquery.js"></script>

      <script>
        // Edit user
        $(".edit-user-button").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.getElementsByClassName("edit-user")[0].style.height = "auto";
          });
        });

        $(".cancel-edit-user").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-user")[0].style.height = "0px";
          });
        });

        $(".apply-edit-user").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-user")[0].style.height = "0px";
          });
        });

        // Edit device
        $(".edit-device-button").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "auto";
          });
        });

        $(".cancel-edit-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
          });
        });

        $(".apply-edit-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
          });
        });

        // New device
        $(".new-device-button").each(function(index) {
          this.addEventListener("click", function() {
            this.style.height = "0px";
            this.parentNode.getElementsByClassName("new-device")[0].style.height = "auto";
          });
        });

        $(".cancel-new-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

          });
        });

        $(".apply-new-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

          });
        });

        $(".new-user-button").each(function(index) {
          this.addEventListener("click", function() {
            this.style.height = "0px";
            this.parentNode.getElementsByClassName("new-user")[0].style.height = "auto";
          });
        });

        $(".cancel-new-user").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-user")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-user-button")[0].style.height = "auto";
          });
        });

        $(".apply-new-user").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-user")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-user-button")[0].style.height = "auto";
          });
        });

        $(".delete-button").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "flex";
          });
        });

        $(".cancel-delete").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
          });
        });

        $(".apply-delete").each(function(index) {
          this.addEventListener("click", function() {
            //this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
          });
        });

        $(".import-users-button").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.getElementsByClassName("import-users")[0].style.display = "flex";
          });
        });

        $(".cancel-import").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("import-users")[0].style.display = "none";
          });
        });

        $(".apply-import").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("import-users")[0].style.display = "none";
          });
        });

        $('[type=submit]').each(function(index) {
            this.addEventListener("click", function() {
                setTimeout(function () {location.reload()}, 1500);
          });
        });
      </script>
    </div>

<?php } ?>