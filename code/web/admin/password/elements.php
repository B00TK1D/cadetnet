<?php function passwordResetBlock($token) { ?>

<div class="div-block-61">
  <div class="div-block-62"></div>
  <div class="div-block-60">
    <h1 class="heading-18">SET NEW ADMIN PASSWORD</h1>
    <div class="w-form">
      <form id="password-reset" name="password-reset" action="/api/admin/password/reset" method="post"><label for="password" class="cn-label">New Password</label><input type="password" class="cn-field w-input" maxlength="256" name="password" placeholder="" required="" /><label for="password-confirm" class="cn-label">Confirm New Password</label><input type="password" class="cn-field w-input" maxlength="256" name="password-confirm" placeholder="" required="" /><input type="text" name="token" value="<?php print($token); ?>" hidden=""/><input type="submit" value="Set" data-wait="Please wait..." class="submit-button-2 w-button" /></form>
    </div>
  </div>
</div>

<?php }  function passwordRequestBlock() { ?>

<div class="div-block-61">
  <div class="div-block-62"></div>
  <div class="div-block-60">
    <h1 class="heading-18">     PASSWORD RESET      </h1>
    <div class="w-form">
      <form id="reset" name="reset" action="/api/admin/password/reset" method="post">
        <label for="email" class="cn-label">Email Address</label><input type="email" class="cn-field w-input" maxlength="256" name="email" placeholder="" id="email" required="" />
        <input type="submit" value="Reset Password" data-wait="Please wait..." class="submit-button-2 w-button" />
      </form>
    </div>
    <div class="footer">
    <div class="div-block-79">
      <div class="text-block-19">Need an account?</div><a href="/public/signup" class="submit-button-2 _2 w-button">Signup</a>
    </div>
  </div>
</div>

<?php } function passwordPendingBlock() { ?>

<div class="div-block-61">
  <div class="div-block-62"></div>
  <div class="div-block-60">
    <h1 class="heading-18">RESET SENT</h1>
  </div>
  <div class="text-block-18 centered">Please check your email to reset your password.</div>
</div>

<?php } ?>