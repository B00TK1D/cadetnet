<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["functs", "elements"]);
    includeUtil(["token"]);
    
    $token = param("token", ["displayError" => true]);

    checkToken($token, "password-reset-admin", true);

    head("Reset Password");
    passwordResetBlock($token);
    foot();
?>