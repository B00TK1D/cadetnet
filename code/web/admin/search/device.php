<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["functs", "elements"]);

    $admin = authenticate("admins")["admin"];

    $menu = [];

    if ($admin["tier"] == config("admin-tier.global")) {
        $menu["MASTER"] = "/admin/master/dashboard";
    }

    $menu["DASHBOARD"] = "/admin/dashboard";
    $menu["LOGOUT"] = "/admin/logout";

    $search = param(["q", "Search for device..."], ["sticky" => true]);
    $search = str_replace("-", ":", $search);

    $devices = searchDevices($search, $admin);

    head("Device Search", $menu);
    deviceSearchBlock($search, $users);
    foot();
?>