<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");


    function userSearchBlock($search, $users) {

?>

    <div class="div-block-63"></div>
    <div class="div-block-64">
        <div>
        <h1 class="heading-19">USER SEARCH</h1>

        <div class="div-block-65 user-box">
            <div class="div-block-74">
            <div style="margin-right: 10px">
                <form action="/admin/search/user" method="get">
                <input type="text" class="cn-field w-input" maxlength="256" name="q" placeholder="Search Query" required="" value="<?php print($search)?>"/>
                </form>
            </div>
            </div>
        </div>



        <?php foreach ($users as $luser) { ?>


        <div class="div-block-65 user-box">
            <div class="div-block-74">
            <div style="margin-right: 10px">
                <div class="text-block-16 bold"><?php print($luser["name"]); ?></div>
                <div class="text-block-16 small"><?php print($luser["email"]); ?></div>
                <div class="text-block-16 small"><?php print($luser["network-name"]); ?></div>
            </div>
            <div class="div-block-66 small"><a class="button-5 small edit-user-button w-button"></a><a class="button-5 _2 small delete-button w-button"></a>
                <div class="delete-confirmation delete-confirm _2">
                <div class="div-block-65">
                    <div class="text-block-16-copy">Delete user <?php echo $luser["name"]; ?>?</div>
                    <div class="div-block-66"><a class="cn-button _2 cancel-delete w-button">CANCEL</a><form action="/api/user/delete" method="post" target="transFrame"><input type="text" name="user" value="<?php echo $luser['id'];?>" hidden=""><input type="submit" class="cn-button apply-delete w-button" value="DELETE"></form></div>
                </div>
                </div>
            </div>
            </div>
            <div class="form-block _2 edit-user w-form">
            <form action="/api/user/update" target="transFrame" method="post"><input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="User Name" required="" value="<?php print($luser['name'])?>"/><input type="email" class="cn-field w-input" maxlength="256" name="email" placeholder="Email" required="" value="<?php print($luser['email'])?>"/><input type="number" class="cn-field w-input" maxlength="2" name="network" placeholder="Network" required="" value="<?php print($luser['network'])?>"/><input type="text" name="user" value="<?php print($luser['id'])?>" hidden=""/>
                <div class="div-block-66"><a class="cn-button _2 cancel-edit-user w-button">CANCEL</a><input type="submit" class="cn-button apply-edit-user w-button" value="APPLY"></div>
            </form>
            </div>


            <?php foreach ($luser["devices"] as $device) { ?>


            <div class="user-device">
            <div class="div-block-70"></div>
            <div class="div-block-73">
                <div class="div-block-66 small"><a class="button-5 small edit-device-button w-button"></a>
                <a class="button-5 _2 small delete-button w-button"></a>
                <a class="button-5 _4 small troubleshoot-button w-button" href="/utils/loader?redirect=/support/troubleshoot/device%3Fm=<?php print($device['mac'])?>" target="_blank"></a>
                <div class="delete-confirmation delete-confirm">
                    <div class="div-block-65">
                    <div class="text-block-16-copy">Delete device <?php echo $device["name"]?>?</div>
                    <div class="div-block-66"><a class="cn-button _2 cancel-delete w-button">CANCEL</a><form action="/api/device/delete" method="post" target="transFrame"><input type="text" name="deviceID" value="<?php echo $device['id'];?>" hidden=""><input type="text" name="user" value="<?php echo $luser['id'];?>" hidden=""><<input type="submit" class="cn-button apply-delete w-button" value="APPLY"></form></div>
                    </div>
                </div>
                </div>
                <div class="div-block-72">
                <div class="text-block-16 small"><?php echo $device["name"]?></div>
                <div class="text-block-16 small"><?php echo $device["mac"]?></div>
                </div>
            </div>
            <div class="form-block _2 edit-device w-form validate-form">
                <form action="/api/device/update" method="post" target="transFrame"><input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Device Name" required="" value="<?php print($device['name'])?>"/><input type="text" class="cn-field w-input mac-format" maxlength="20" name="mac" placeholder="MAC" required="" value="<?php print($device['mac'])?>"/><input hidden="" type="text" name="user" value="<?php print($luser['id'])?>"/><input hidden="" type="text" name="deviceID" value="<?php print($device['id'])?>"/>
                <div class="div-block-66"><a class="cn-button _2 cancel-edit-device w-button">CANCEL</a><input type="submit"  class="cn-button apply-edit-device w-button" value="APPLY"></div>
                </form>
            </div>
            </div>

        <?php } ?>

        <div class="div-block-76 new-device">
            <div class="div-block-70"></div>
            <div class="form-block _2 new-device w-form">
                <form class="validate-form" action="/api/device/create" method="post" target="transFrame"><input type="text" class="cn-field w-input validate-form" maxlength="256" name="name" placeholder="Device Name" required="" /><input type="text" class="cn-field w-input mac-format" maxlength="20" name="mac" placeholder="MAC" required=""/><input hidden="" type="text" name="user" value="<?php echo $luser['id']?>"/>
                <div class="div-block-66"><a class="cn-button _2 cancel-new-device w-button">CANCEL</a><input type="submit" class="cn-button apply-new-user w-button" value="APPLY"></div>
                </form>
            </div><a class="div-block-65 hover small new-device-button w-inline-block">
                <div class="div-block-66-copy _2">
                <div class="button-5 _3"></div>
                </div>
            </a>
            </div>
        </div>


        <?php } ?>

        



    <div class="div-block-68"></div>
    <div class="w-embed w-script">
        <script src="/js/jquery.js"></script>

        <script>
        // Edit user
        $(".edit-user-button").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.getElementsByClassName("edit-user")[0].style.height = "auto";
            });
        });

        $(".cancel-edit-user").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-user")[0].style.height = "0px";
            });
        });

        $(".apply-edit-user").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-user")[0].style.height = "0px";
            });
        });

        // Edit device
        $(".edit-device-button").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "auto";
            });
        });

        $(".cancel-edit-device").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
            });
        });

        $(".apply-edit-device").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
            });
        });

        // New device
        $(".new-device-button").each(function(index) {
            this.addEventListener("click", function() {
            this.style.height = "0px";
            this.parentNode.getElementsByClassName("new-device")[0].style.height = "auto";
            });
        });

        $(".cancel-new-device").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

            });
        });

        $(".apply-new-device").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

            });
        });

        $(".new-user-button").each(function(index) {
            this.addEventListener("click", function() {
            this.style.height = "0px";
            this.parentNode.getElementsByClassName("new-user")[0].style.height = "auto";
            });
        });

        $(".cancel-new-user").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-user")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-user-button")[0].style.height = "auto";
            });
        });

        $(".apply-new-user").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-user")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-user-button")[0].style.height = "auto";
            });
        });

        $(".delete-button").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "flex";
            });
        });

        $(".cancel-delete").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
            });
        });

        $(".apply-delete").each(function(index) {
            this.addEventListener("click", function() {
            //this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
            });
        });

        $(".import-users-button").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.getElementsByClassName("import-users")[0].style.display = "flex";
            });
        });

        $(".cancel-import").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("import-users")[0].style.display = "none";
            });
        });

        $(".apply-import").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("import-users")[0].style.display = "none";
            });
        });

        $('[type=submit]').each(function(index) {
            this.addEventListener("click", function() {
                setTimeout(function () {location.reload()}, 1000);
            });
        });
        </script>

<?php } function deviceSearchBlock($search, $devices) { ?>

    <div class="div-block-63"></div>
    <div class="div-block-64">
        <div>
        <h1 class="heading-19">DEVICE SEARCH</h1>

        <div class="div-block-65 user-box">
            <div class="div-block-74">
            <div style="margin-right: 10px">
                <form action="/admin/search/device" method="get">
                <input type="text" class="cn-field w-input" maxlength="256" name="q" placeholder="Search Query" required="" value="<?php print($search)?>"/>
                </form>
            </div>
            </div>
        </div>



        <?php foreach ($devices as $device) { ?>


        <div class="div-block-65 user-box">
            <div class="div-block-74">
            <div style="margin-right: 10px">
                <div class="text-block-16 bold"><?php print($device["name"]); ?></div>
                <div class="text-block-16 small"><?php print($device["mac"]); ?></div>
            </div>

            <div class="user-device">
            <div class="div-block-70"></div>
            <div class="div-block-73">
                <div class="div-block-66 small"><a class="button-5 small edit-device-button w-button"></a><a class="button-5 _2 small delete-button w-button"></a><a class="button-5 _4 small troubleshoot-button w-button" href="/support/troubleshoot/device?m=<?php print($device['mac'])?>" target="_blank"></a>
                <div class="delete-confirmation delete-confirm">
                    <div class="div-block-65">
                    <div class="text-block-16-copy">Delete device <?php echo $device["name"]?>?</div>
                    <div class="div-block-66"><a class="cn-button _2 cancel-delete w-button">CANCEL</a><form action="/api/device/delete" method="post" target="transFrame"><input type="text" name="deviceID" value="<?php echo $device['id'];?>" hidden=""><input type="text" name="user" value="<?php echo $luser['id'];?>" hidden=""><<input type="submit" class="cn-button apply-delete w-button" value="APPLY"></form></div>
                    </div>
                </div>
                </div>
                <div class="div-block-72">
                <div class="text-block-16 small"><?php echo $device["name"]?></div>
                <div class="text-block-16 small"><?php echo $device["mac"]?></div>
                </div>
            </div>
            <div class="form-block _2 edit-device w-form validate-form">
                <form action="/api/device/update" method="post" target="transFrame"><input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Device Name" required="" value="<?php print($device['name'])?>"/><input type="text" class="cn-field w-input mac-format" maxlength="20" name="mac" placeholder="MAC" required="" value="<?php print($device['mac'])?>"/><input hidden="" type="text" name="user" value="<?php print($luser['id'])?>"/><input hidden="" type="text" name="deviceID" value="<?php print($device['id'])?>"/>
                <div class="div-block-66"><a class="cn-button _2 cancel-edit-device w-button">CANCEL</a><input type="submit"  class="cn-button apply-edit-device w-button" value="APPLY"></div>
                </form>
            </div>
            </div>

        <div class="div-block-76 new-device">
            <div class="div-block-70"></div>
            <div class="form-block _2 new-device w-form">
                <form class="validate-form" action="/api/device/create" method="post" target="transFrame"><input type="text" class="cn-field w-input validate-form" maxlength="256" name="name" placeholder="Device Name" required="" /><input type="text" class="cn-field w-input mac-format" maxlength="20" name="mac" placeholder="MAC" required=""/><input hidden="" type="text" name="user" value="<?php echo $luser['id']?>"/>
                <div class="div-block-66"><a class="cn-button _2 cancel-new-device w-button">CANCEL</a><input type="submit" class="cn-button apply-new-user w-button" value="APPLY"></div>
                </form>
            </div><a class="div-block-65 hover small new-device-button w-inline-block">
                <div class="div-block-66-copy _2">
                <div class="button-5 _3"></div>
                </div>
            </a>
            </div>
        </div>


        <?php } ?>

        



    <div class="div-block-68"></div>
    <div class="w-embed w-script">
        <script src="/js/jquery.js"></script>

        <script>
        // Edit user
        $(".edit-user-button").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.getElementsByClassName("edit-user")[0].style.height = "auto";
            });
        });

        $(".cancel-edit-user").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-user")[0].style.height = "0px";
            });
        });

        $(".apply-edit-user").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-user")[0].style.height = "0px";
            });
        });

        // Edit device
        $(".edit-device-button").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "auto";
            });
        });

        $(".cancel-edit-device").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
            });
        });

        $(".apply-edit-device").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
            });
        });

        // New device
        $(".new-device-button").each(function(index) {
            this.addEventListener("click", function() {
            this.style.height = "0px";
            this.parentNode.getElementsByClassName("new-device")[0].style.height = "auto";
            });
        });

        $(".cancel-new-device").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

            });
        });

        $(".apply-new-device").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

            });
        });

        $(".new-user-button").each(function(index) {
            this.addEventListener("click", function() {
            this.style.height = "0px";
            this.parentNode.getElementsByClassName("new-user")[0].style.height = "auto";
            });
        });

        $(".cancel-new-user").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-user")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-user-button")[0].style.height = "auto";
            });
        });

        $(".apply-new-user").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-user")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-user-button")[0].style.height = "auto";
            });
        });

        $(".delete-button").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "flex";
            });
        });

        $(".cancel-delete").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
            });
        });

        $(".apply-delete").each(function(index) {
            this.addEventListener("click", function() {
            //this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
            });
        });

        $(".import-users-button").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.getElementsByClassName("import-users")[0].style.display = "flex";
            });
        });

        $(".cancel-import").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("import-users")[0].style.display = "none";
            });
        });

        $(".apply-import").each(function(index) {
            this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("import-users")[0].style.display = "none";
            });
        });

        $('[type=submit]').each(function(index) {
            this.addEventListener("click", function() {
                setTimeout(function () {location.reload()}, 1000);
            });
        });
        </script>

<?php } ?>