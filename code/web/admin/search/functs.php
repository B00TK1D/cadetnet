<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    function searchUsers($search, $admin) {
        $users = [];
        $tempUsers = [];
        if ($admin["tier"] == config("admin-tier.global")) {
            $tempUsers = searchObject("users", ["name" => $search, "email" => $search]);
        } else {
            $tempUsers = searchObject("users", ["name" => $search, "email" => $search], ["network" => $admin["network"]]);
        }
        foreach ($tempUsers as $tempUser) {
            $user = $tempUser;
            $user["devices"] = readObject("devices", ["user" => $user["id"]]);
            $network = readObject("networks", ["id" => $user["network"]], 1);
            if ($network == null) {
                $user["network-name"] = "";
            } else {
                $user["network-name"] = $network["name"];
            }
            $users[] = $user;
        }
        return $users;
    }


    function searchDevices($search, $admin) {
        $devices = [];
        if ($admin["tier"] == config("admin-tier.global")) {
            $devices = searchObject("devices", ["name" => $search, "mac" => $search]);
        } else {
            $devices = searchObject("devices", ["name" => $search, "mac" => $search], ["network" => $admin["network"]]);
        }
        return $devices;
    }
?>