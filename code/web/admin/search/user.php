<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["functs", "elements"]);

    $admin = authenticate("admins")["admin"];

    $menu = [];

    if ($admin["tier"] == config("admin-tier.global")) {
        $menu["MASTER"] = "/admin/master/dashboard";
    }

    $menu["DASHBOARD"] = "/admin/dashboard";
    $menu["LOGOUT"] = "/admin/logout";

    $search = param(["q", "Search for user..."], ["sticky" => true]);

    $users = searchUsers($search, $admin);

    head("User Search", $menu);
    userSearchBlock($search, $users);
    foot();
?>