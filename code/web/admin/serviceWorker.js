self.addEventListener('push', function (event) {
    if (event.data) {
        const payload = JSON.parse(event.data.text());
        if (payload.type == "push") {
            if (!(self.Notification && self.Notification.permission === 'granted')) {
                return;
            }
            const sendNotification = body => {    
                return self.registration.showNotification(payload.title, {
                    body: payload.msg,
                    tag: payload.tag,
                    icon: payload.icon,
                    badge: payload.icon,
                    image: payload.img,
                    vibrate: [100, 100, 300, 100, 100, 100, 300],
                    data: {url: payload.url},
                });
            };
            event.waitUntil(sendNotification(payload.msg));
        }
    }
});


self.addEventListener('notificationclick', function(event) {
    clients.openWindow(event.notification.data.url);
    event.notification.close();
}
, false);