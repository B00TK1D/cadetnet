<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["elements"]);

    $token = param("token", true);

    head("CadetNet Admin Onboard");
    adminOnboardBlock($token);
    foot();
?>