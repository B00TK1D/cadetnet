<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $email = param("user");
    $password = param("pass");
    $admin = readObject("admins", ["email" => $email], 1);
    if ($admin != null && $admin["status"] >= config("verification.verified") && checkHash($password, $admin["password"])) {
        die("1");
    }
    die("0");
?>