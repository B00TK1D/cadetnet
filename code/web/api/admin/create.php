<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil(["token", "mail"]);

    authenticate("global-admins");

    $admin = schemaParam(objectFields("admins"), ["name", "email", "tier"]);

    if(readObject("admins", ["email" => $admin["email"]], 1) == null) {
        $admin["id"] = createObject("admins", $admin);

        $token = generateToken("verification");
        $token["user"] = $admin["id"];
        createObject("tokens", $token);
        
        $admin["token"] = $token["string"];
        $admin["link"] = "https://" . config("deployment.domain") . "/admin/verify?token=" . $token["string"];
        mailTemplate("admin-onboard", $admin);
    }
?>