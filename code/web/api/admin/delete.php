<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $id = param("admin", false);

    authenticate("owners", ["admin" => $id]);
    
    deleteObject("admins", ["id" => $id]);
?>