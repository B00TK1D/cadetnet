<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil("message");

    $password = getHash(param("password", ["sanitize" => false]));
    $signupToken = param("token", true);

    $token = readObject("tokens", ["string" => $signupToken], 1);
    if ($token == null) error("invalid-token");

    $admin["id"] = $token["user"];
    $update = [
        "password" => $password,
        "status" => config("verification.verified"),
    ];

    updateObject("admins", $update, $admin);
    deleteObject("tokens", $token);
    redirect("/admin/login?code" . code("account-verified"));
?>