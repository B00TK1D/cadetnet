<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    
    function updatePasswordAdmin($admin, $oldPass, $newPass) {
        if (checkHash($oldPass, readObject("admins", ["id" => $admin["id"]], 1)["password"])) {
            $newHash = getHash($newPass);
            updateObject("admins", ["password" => $newHash], ["id" => $admin["id"]]);
            logout();
        }
        error("invalid-credentials");
    }

    function logout() {
        resetSession();
        redirect("/admin/login?code=" . code("password-reset"));
    }
?>