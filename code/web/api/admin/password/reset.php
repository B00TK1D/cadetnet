<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil(["mail","token"]);
    includeAll("functs");

    $token = param(["token", false]);
    $email = param(["email", false]);

    if ($token) {
        $token = checkToken($token, "password-reset-admin");
        $id = $token["user"];
        $password = getHash(param("password", ["sanitize" => false]));
        updateObject("admins", ["password" => $password], ["id" => $id]);
        redirect("/admin/login?code=" . code("password-set"));
    }

    if ($email) {
        $email = strtolower($email);
        $admin = readObject("admins", ["email" => $email], 1);
        if ($admin != null) {
            $token = generateToken("password-reset-admin");
            $token["user"] = $admin["id"];
            createObject("tokens", $token);
            $admin["token"] = $token["string"];
            $admin["link"] = "https://" . config("deployment.domain") . "/admin/password/reset?token=" . $token["string"];
            mailTemplate("password-reset", $admin);
        }
        redirect("/admin/password/pending");
    }

    error();

    
?>