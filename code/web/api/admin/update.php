<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $id = param("admin");
    $admin = readObject("admins", ["id" => $id], 1);
    if ($admin == null) die();

    authenticate("owners", ["admin" => $admin["id"]]);

    $update = schemaParam(objectFields("admins"));

    if (isset($update["network"])) authenticate("owners", ["network" => $update["network"]]);
    $admin = array_merge($admin, $update);

    updateObject("admins", $admin, ["id" => $id]);
?>