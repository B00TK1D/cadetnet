<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil(["crypto"]);

    authenticate("global-admins");

    $filters["network"] = param("network");

    $filters["type"] = config("equipment.type.modem");
    $modems = readObjects("equipment", $filters);

    if ($modems == null) die();

    foreach ($modems as $modem) {
        $cred = reset(readObject("credentials", ["id" => $modem["creds"]]));
        $decrypted = decrypt($cred["cred"]);
        exec("bash /etc/scripts/reboot-modems.bash " . $modem["ip"] . " " . $decrypted);
    }
?>