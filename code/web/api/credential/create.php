<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil("crypto");

    $credential = schemaParam(objectFields("credentials"), ["name", "network"]);
    $credential["cred"] = encrypt(param("password", ["sanitize" => false]));

    $admin = authenticate("owners", ["network" => $credential["network"]])["admin"];

    $credential["creator"] = $admin["id"];
    $credential["updator"] = $admin["id"];

    $credential["admin"] = $admin["id"];

    if (!(substr($credential["link"], 0, 4) === "http")) {
        $credential["link"] = "https://" . $credential["link"];
    }

    createObject("credentials", $credential);
?>