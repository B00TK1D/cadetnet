<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil("crypto");

    $id = param("credential");
    $credential = readObject("credentials", ["id" => $id], 1);
    if ($credential == null) die();
    
    authenticate("owners", ["network" => $credential["network"]]);

    $cred = decrypt($credential["cred"]);

    print($cred);
?>