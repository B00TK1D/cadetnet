<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil("crypto");

    $id = param("credential");
    $credential = readObject("credentials", ["id" => $id], 1);
    if ($credential == null) die();
    
    $admin = authenticate("owners", ["network" => $credential["network"]])["admin"];
    
    $update = schemaParam(objectFields("credentials"));
    $update["updator"] = $admin["id"];
    $credential = array_merge($credential, $update);

    updateObject("credentials", $credential, ["id" => $id]);
?>