<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $device = schemaParam(objectFields("devices"));
    $device["mac"] = str_replace("-", ":", $device["mac"]);

    authenticate("owners", ["user" => $device["user"]]);

    createObject("devices", $device);
?>