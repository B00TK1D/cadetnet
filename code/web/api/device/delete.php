<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $id = param("device");
    $device = readObject("devices", ["id" => $id], 1);
    if ($device == null) die();

    authenticate("owners", ["user" => $device["user"]]);

    deleteObject("devices", $device);
?>