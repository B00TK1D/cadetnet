<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $id = param("device");
    $device = readObject("devices", ["id" => $id], 1);
    if ($device == null) die();

    authenticate("owners", ["user" => $device["user"]]);

    $update = schemaParam(objectFields("devices"));
    $update["mac"] = strtolower(str_replace("-", ":", $update["mac"]));
    $device = array_merge($device, $update);
    authenticate("owners", ["user" => $device["user"]]);

    updateObject("devices", $device, ["id" => $id]);
?>