<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $equipment = schemaParam(objectFields("equipment"), ["tag", "network"]);

    $admin = authenticate("owners", ["network" => $equipment["network"]])["admin"];

    if (isset($equipment["model"])) $equipment["type"] = readObject("models", ["id" => $equipment["model"]], 1)["type"];
    if (isset($equipment["network"])) $equipment["floor"] = readObject("networks", ["id" => $equipment["network"]], 1)["floor"];
    $equipment["creator"] = $admin["id"];
    $equipment["updator"] = $admin["id"];

    createObject("equipment", $equipment);

    $return = param(["return", "/equipment/success?code=" . code("equipment-created")]);
    redirect($return);
?>