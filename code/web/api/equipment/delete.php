<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $id = param("equipment");
    $equipment = readObject("equipment", ["id" => $id], 1);
    if ($equipment == null) die();

    authenticate("owners", ["network" => $equipment["network"]]);

    deleteObject("equipment", ["id" => $id]);

    $return = param(["return", "/equipment/success?code=" . code("equipment-created")]);
    redirect($return);
?>