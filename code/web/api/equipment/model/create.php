<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $model = schemaParam(objectFields("models"));

    if(isset($model["cost"])) $model["cost"] = round($model["cost"], 2) * 100;

    $admin = authenticate("global-admins")["admin"];
    $model["creator"] = $admin["id"];
    $model["updator"] = $admin["id"];

    createObject("models", $model);
?>