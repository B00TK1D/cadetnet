<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $id = param("model");
    $model = readObject("models", ["id" => $id], 1);
    if ($model == null) die();

    authenticate("global-admins");

    deleteObject("models", $model);
?>