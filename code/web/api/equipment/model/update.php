<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $id = param("model");
    $model = readObject("models", ["id" => $id], 1);
    if ($model == null) die();

    $admin = authenticate("global-admins")["admin"];

    $update = schemaParam(objectFields("models"));
    $update["updator"] = $admin["id"];
    if(isset($update["cost"])) $update["cost"] = round($update["cost"], 2) * 100;
    if(!isset($update["notes"])) $update["notes"] = "";
    $model = array_merge($model, $update);

    updateObject("models", $model, ["id" => $id]);
?>