<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $id = param("equipment");
    $equipment = readObject("equipment", ["id" => $id], 1);
    if ($equipment == null) die();

    $admin = authenticate("owners", ["network" => $equipment["network"]])["admin"];

    $update = schemaParam(objectFields("equipment"));
    $update["updator"] = $admin["id"];
    if (isset($update["model"])) $update["type"] = readObject("models", ["id" => $update["model"]], 1)["type"];
    if (isset($update["network"])) $update["floor"] = readObject("networks", ["id" => $update["network"]], 1)["floor"];
    if (isset($update["lat"]) && $update["lat"] == 0) unset($update["lat"]);
    if (isset($update["lon"]) && $update["lon"] == 0) unset($update["lon"]);
    $equipment = array_merge($equipment, $update);
    $admin = authenticate("owners", ["network" => $equipment["network"]])["admin"];
    $equipment["updator"] = $admin["id"];

    updateObject("equipment", $equipment, ["id" => $id]);

    $return = param(["return", "/equipment/success?code=" . code("equipment-updated")]);
    redirect($return);
?>