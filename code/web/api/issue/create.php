<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["functs"]);
    includeUtil(["issue"]);
    
    $issue = schemaParam(objectFields("issues"));

    if (!isset($issue["network"])) die();
    if (isset($issue["user"])) authenticate("owners", ["user" => $issue["user"]]);

    $issue["id"] = createObject("issues", $issue);
    $issue = readObject("issues", ["id" => $issue["id"]], 1);

    notifyReport($issue);
    $prediction = processReport($issue);
    $priority = getPriority($issue);

    updateObject("issues", ["priority" => $priority], ["id" => $issue["id"]]);

    redirect("/support/solution/suggestion?prediction=" . $prediction);
?>