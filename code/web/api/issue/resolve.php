<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $id = param("issue");
    $issue = readObject("issues", ["id" => $id], 1);
    if ($issue == null) die();

    authenticate("owners", ["network" => $issue["network"]]);

    updateObject("issues", ["status" => config("issue.status.resolved")], ["id" => $id]);

    redirect("/support/issue/list");
?>