<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $id = param("issue");
    $issue = readObject("issues", ["id" => $id], 1);
    if ($issue == null) die();

    authenticate("owners", ["user" => $issue["user"]]);

    $update = schemaParam(objectFields("issues"));
    $issue = array_merge($issue, $update);

    updateObject("issues", $issue, ["id" => $id]);
?>