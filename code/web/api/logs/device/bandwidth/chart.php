<?php

    include_once('functs.php');

    $bandwidth = deviceBandwidth();

    print('{ "xData": [');
    
    $delim = "";
    foreach ($bandwidth["device"]["stats"] as $stat) {
        print($delim . fromUTC($stat["Timestamp"]) . "000");
        $delim = ",";
    }

    print('], "datasets": [');

    print('{"name": "Device Bandwidth", "type": "bandwidth", "min": null, "max": null, "series": [{"name": "Download", "network": "bps", "type": "spline", "valueDecimals": 1, "data": [');

    $delim = "";
    foreach ($bandwidth["device"]["stats"] as $stat) {
        print($delim . $stat['Throughput [In] (bps)']);
        $delim = ",";
    }

    print(']}, {"name": "Upload", "network": "bps", "type": "spline", "valueDecimals": 1, "data": [');

    $delim = "";
    foreach ($bandwidth["device"]["stats"] as $stat) {
        print($delim . $stat['Throughput [Out] (bps)']);
        $delim = ",";
    }

    print(']}]}]}');



?>