<?php
    include_once($_SERVER['DOCUMENT_ROOT'] . '/db.php');
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    include_once($_SERVER['DOCUMENT_ROOT'] . '/config.php');
    include_once($_SERVER['DOCUMENT_ROOT'] . '/aruba.php');
    include_once($_SERVER['DOCUMENT_ROOT'] . '/authenticate.php');

    
    function deviceBandwidth($mac = null) {
        
        authenticate($authenticate_level["global-admins"]);

        $bandwidth["device"] = showClient(list_networks(), $mac);
        $bandwidth["device"]["mac"] = $mac;

        if (isset($bandwidth["device"]["data"])) {
            $bandwidth["device"]["stats"] = expandMerged($bandwidth["device"]["Swarm Client Stats"]);
        } else {
            $bandwidth["device"]["stats"] = [];
        }


        $bandwidth["device"]["stats"] = sortBy($bandwidth["device"]["stats"], "Timestamp");

        return $bandwidth;
    }

    

?>