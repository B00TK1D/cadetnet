<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil(["aruba"]);

    $mac = param("m");
    $device["db"] = readObject("devices", ["mac" => $mac], 1);
    if ($device == null) die();

    $admin = authenticate("owners", ["user" => $device["db"]["user"]]);
    
    $device["owner"] = readObject("users", ["id" => $device["db"]["user"]], 1);
    if ($device["owner"] == null) die();
    $device["name"] = $device["db"]["name"];
    $device["user"] = $device["owner"]["name"];

    $device = showClient(readObject("networks"), $mac);
    $device["mac"] = $mac;

    if (isset($device["data"])) {
        $device["network"] = listMergedString($device["data"]["Network"]);
        $device["ip"] = listMergedString($device["data"]["IP Address"]);
        $device["ap"] = listMergedString($device["data"]["Access Point"]);
        $device["ch"] = listMergedString($device["data"]["Channel"]);
        $device["os"] = listMergedString($device["data"]["OS"]);
        $device["connectionTime"] = listMergedString($device["data"]["Connection Time"]);
        $device["stats"] = expandMerged($device["Swarm Client Stats"]);
    } else {
        $device["network"] = "";
        $device["ip"] = "";
        $device["ap"] = "Disconnected";
        $device["ch"] = "Not currently connected";
        $device["os"] = "";
        $device["connectionTime"] = "Disconnected";
        $device["stats"] = [];
    }


    $device["stats"] = sortBy($device["stats"], "Timestamp");



    print('{ "xData": [');
    
    $delim = "";
    foreach ($device["stats"] as $stat) {
        print($delim . fromUTC($stat["Timestamp"]) . "000");
        $delim = ",";
    }

    print('], "datasets": [');

    print('{"name": "Signal Strength", "type": "linear", "min": 0, "max": null, "series": [{"name": "Signal", "network": "dB", "type": "spline", "valueDecimals": 0, "data": [');
    
    $delim = "";
    foreach ($device["stats"] as $stat) {
        print($delim . $stat['Signal (dB)']);
        $delim = ",";
    }

    print(']}]}, {"name": "Connection Speed", "type": "linear", "min": 0, "max": null, "series": [{"name": "Speed", "network": "Mbps", "type": "spline", "valueDecimals": 0, "data": [');

    $delim = "";
    foreach ($device["stats"] as $stat) {
        print($delim . $stat['Speed (mbps)']);
        $delim = ",";
    }

    print(']}]}, {"name": "Packet Error", "min": null, "max": null, "series": [{"name": "Error", "network": "%", "type": "spline", "valueDecimals": 1, "data": [');

    $delim = "";
    foreach ($device["stats"] as $stat) {
        $error = $stat['Frames [Retries In] (fps)'] + $stat['Frames [Retries Out] (fps)'];
        $total = $stat['Frames [In] (fps)'] + $stat['Frames [Out] (fps)'];
        $val = ($total) ? (($error /  $total) * 100) : 0;
        print($delim . $val);
        $delim = ",";
    }

    print(']}]}]}');
?>