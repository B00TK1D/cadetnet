<?php
    include_once('functs.php');

    $bandwidth = globalBandwidth();

    

    print('{ "xData": [');
    
    $delim = "";
    foreach ($bandwidth["global"]["stats"] as $stat) {
        print($delim . fromUTC($stat["Timestamp"]) . "000");
        $delim = ",";
    }

    print('], "datasets": [');

    print('{"name": "Global (Wing-wide) Bandwidth", "type": "bandwidth", "min": null, "max": null, "series": [{"name": "Download", "network": "bps", "type": "spline", "valueDecimals": 1, "data": [');

    $delim = "";
    foreach ($bandwidth["global"]["stats"] as $stat) {
        print($delim . $stat['Throughput [Out] (bps)']);
        $delim = ",";
    }

    print(']}, {"name": "Upload", "network": "bps", "type": "spline", "valueDecimals": 1, "data": [');

    $delim = "";
    foreach ($bandwidth["global"]["stats"] as $stat) {
        print($delim . $stat['Throughput [In] (bps)']);
        $delim = ",";
    }

    print(']}]}]}');

    
    
?>