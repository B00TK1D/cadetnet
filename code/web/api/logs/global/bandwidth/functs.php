<?php
    include_once($_SERVER['DOCUMENT_ROOT'] . '/db.php');
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    include_once($_SERVER['DOCUMENT_ROOT'] . '/config.php');
    include_once($_SERVER['DOCUMENT_ROOT'] . '/aruba.php');
    include_once($_SERVER['DOCUMENT_ROOT'] . '/authenticate.php');


    function globalBandwidth() {

        authenticate($authenticate_level["global-admins"]);

        $bandwidth = [];

        $networks = list_networks();
        $bandwidth["global"]["data"] = showNetwork($networks)["Swarm Global Stats"];

        $uniqueUnits = getUnique($bandwidth["global"]["data"], "Network");


        foreach ($bandwidth["global"]["data"] as $data) {
            $global = [];
            foreach ($uniqueUnits as $uniqueUnit) {
                $closest = getClosest($bandwidth["global"]["data"], ["Timestamp" => $data["Timestamp"]], ["Network" => $uniqueUnit]);
                foreach ($closest as $key => $value) {
                    if (isset($global[$key])) {
                        $global[$key] += $value;
                    } else {
                        $global[$key] = $value;
                    }
                }
                $global["Timestamp"] = $data["Timestamp"];
                $global["Network"] = $data["Network"];
            }
            $bandwidth["global"]["stats"][] = $global;
        }

        $bandwidth["global"]["stats"] = sortBy($bandwidth["global"]["stats"], "Timestamp");

        return $bandwidth;
    }

?>

