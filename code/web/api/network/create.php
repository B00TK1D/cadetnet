<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    
    $network = schemaParam(objectFields("networks"), ["name"]);

    authenticate("global-admins");

    createObject("networks", $network);
?>