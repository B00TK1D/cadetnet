<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $id = param("network");

    authenticate("global-admins");
    
    deleteObject("networks", ["id" => $id]);
?>