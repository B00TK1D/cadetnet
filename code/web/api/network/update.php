<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    authenticate("global-admins");

    $id = param("network");
    $network = readObject("networks", ["id" => $id], 1);
    if ($network == null) die();

    $update = schemaParam(objectFields("networks"));

    $network = array_merge($network, $update);

    updateObject("networks", $network, ["id" => $id]);
?>