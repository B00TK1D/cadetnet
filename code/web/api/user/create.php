<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $user = schemaParam(objectFields("users"), ["name", "email", "network"]);
    if (param(["password", false])) {
        $user["password"] = getHash(param(["password"]));
    }

    authenticate("open");

    createObject("users", $user);
?>