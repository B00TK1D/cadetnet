<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["functs", "elements"]);
    includeUtil("mail", "composer");

    $id = param("user");
    $user = readObject("users", ["id" => $id], 1);
    if ($user == null) error();
    
    authenticate("owners", ["user" => $user["id"]]);

    $stripe = new \Stripe\StripeClient(config("stripe.secret_key"));

    if($user["subscription"] != "") {
        try {
            $stripe->subscriptions->cancel(
                $user["subscription"]
            );
        } catch (\Stripe\Exception\ApiErrorException $e) {

        }
    }

    if($user["customer"] != "") {
        try {
            $stripe->customers->delete(
                $user["customer"]
              );
        } catch (\Stripe\Exception\ApiErrorException $e) {

        }
    }

    mailTemplate("user-deleted", $user);

    deleteObject("users", ["id" => $id]);
    deleteObject("devices", ["user" => $id]);
    deleteObject("notification_endpoints", ["user" => $id]);

    redirect("/user/login?code=" . code("user-deleted"));
?>