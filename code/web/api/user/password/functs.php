<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    
    function updatePassword($user, $oldPass, $newPass) {
        if (checkHash($oldPass, readObject("users", ["id" => $user["id"]], 1)["password"])) {
            $newHash = getHash($newPass);
            updateObject("users", ["password" => $newHash], ["id" => $user["id"]]);
            logout();
        }
        error("invalid-credentials");
    }

    function logout() {
        resetSession();
        redirect("/user/login?code=" . code("password-reset"));
    }
?>