<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil(["mail","token"]);
    includeAll("functs");

    $token = param(["token", false]);
    $email = param(["email", false]);

    if ($token) {
        $token = checkToken($token, "password-reset");
        $id = $token["user"];
        $password = getHash(param("password", ["sanitize" => false]));
        updateObject("users", ["password" => $password], ["id" => $id]);
        redirect("/user/login?code=" . code("password-set"));
    }

    if ($email) {
        $email = strtolower($email);
        $user = readObject("users", ["email" => $email], 1);
        if ($user != null) {
            $token = generateToken("password-reset");
            $token["user"] = $user["id"];
            createObject("tokens", $token);
            $user["token"] = $token["string"];
            $user["link"] = "https://" . config("deployment.domain") . "/user/password/reset?token=" . $token["string"];
            mailTemplate("password-reset", $user);
        }
        redirect("/user/password/pending");
    }

    error();

    
?>