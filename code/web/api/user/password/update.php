<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $oldPass = param("old-password", ["sanitize" => false]);
    $newPass = param("new-password", ["sanitize" => false]);
    $id = param("user");

    $user = authenticate("owners", ["user" => $id])["user"];
    if (!checkHash($oldPass, readObject("users", $user["id"], 1)["password"])) error("invalid-credentials");

    $user["password"] = getHash($newPass);
    updateObject("users", $user, $user["id"]);
    redirect("/login.php?code=" . code("password-reset"));
?>