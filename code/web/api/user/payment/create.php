<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil(["composer", "token", "message"]);

    $signupToken = param("token", ["displayError" => true]);
    $stripeToken = param("stripe-token", ["displayError" => true, "sanitize" => false]);

    $token = checkToken($signupToken, "verification", true);
    $user["id"] = $token["user"];
    $user = readObject("users", ["id" => $user["id"]], 1);

    $price = "";
    if (isDevEnv()) {
        $price = config("stripe.prices.basic_plan_dev");
    } else {
        $price = config("stripe.prices.basic_plan");
    }

    $stripe = new \Stripe\StripeClient(
        config("stripe.secret_key")
    );

    try {
        $customer = $stripe->customers->create([
            "name" => $user["name"],
            "email" => $user["email"],
            "source"  => $stripeToken
        ]);

        $subscription = $stripe->subscriptions->create([
            "customer" => $customer->id,
            "items" => [["price" => $price]]
        ]);
    } catch (\Stripe\Exception\CardException $e) {
        redirect("/user/verify?token=" . $signupToken . "&code=" . code("payment-failed"));
    } catch (\Stripe\Exception\ApiErrorException $e) {
        redirect("/user/verify?token=" . $signupToken . "&code=" . code("payment-failed"));
    }

    $last4Token = $stripe->tokens->retrieve($stripeToken);
    $user["last4"] = $last4Token["card"]["last4"];
    $user["customer"] = $customer->id;
    $user["subscription"] = $subscription->id;
    $user["status"] = config("verification.verified");

    updateObject("users", $user, ["id" => $user["id"]]);
    deleteObject("tokens", ["user" => $user["id"], "purpose" => config("token.verification")]);

    redirect("/user/login?code=" . code("setup-complete"));
?>