<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil("composer");

    $id = param("user");
    $stripeToken = param("stripe-token");

    $user = readObject("users", ["id" => $id], 1);

    $stripe = new \Stripe\StripeClient(
        config("stripe.secret_key")
    );

    $customer = $stripe->customers->retrieve($user["customer"]);

    $source = $stripe->customers->update(
        $customer->id,
        ['source' => $stripeToken]
    );

    $token = $stripe->tokens->retrieve($stripeToken);
    $user["last4"] = $token["card"]["last4"];
    if($user["status"] == config("verification.payment-error")) $user["status"] = config("verification.verified");
    updateObject("users", $user, ["id" => $id]);

    redirect("/user/dashboard");
?>