<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    $id = param("user");
    $user = reset(readObject("users", ["id" => $id]));
    if ($user == null) die();

    authenticate("owners", ["user" => $user["id"]]);

    $update = schemaParam(objectFields("users"));
    $user = array_merge($user, $update);
    $user["email"] = strtolower($user["email"]);

    updateObject("users", $user, ["id" => $id]);
?>