<?php function head($title = "CadetNet", $navs = [], $selectors = [], $selected = []) { ?>

    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8" />
        <title><?php print($title); ?></title>
        <meta content="<?php print($title); ?>" property="og:title" />
        <meta content="<?php print($title); ?>" property="twitter:title" />
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <link href="/style.css?hash=<?php print(md5_file($_SERVER['DOCUMENT_ROOT'] . '/style.css')); ?>" rel="stylesheet" type="text/css" />
        <script src="/js/header.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-02Q4GJRYZR"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'G-02Q4GJRYZR');
        </script>
        <link href="/img/icon%2032.png" rel="shortcut icon" type="image/x-icon" />
        <link href="/img/icon%20256.png" rel="apple-touch-icon" />
      </head>
      <body class="body-2">
        <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar-2 w-nav">
          <div class="w-container"><a class="brand-2 w-nav-brand"></a>
            <nav role="navigation" class="w-nav-menu">

                <?php if($selectors) { 
                    foreach ($selectors as $name => $selector) { ?>
                  
                  <script>
                      function loadSelect() {
                        url = window.location.href;
                        value = document.getElementById('select-<?php print($name); ?>').value;
                        if (url.indexOf('?') > -1) {
                          url +='&<?php print($name); ?>=' + value;
                        } else {
                          url +='?<?php print($name); ?>=' + value;
                        }
                        window.location.href = url;
                      }
                  </script>

                  <select name="network" id="select-<?php print($name); ?>" class="nav-link-2 w-nav-link" onchange="loadSelect()">
                    <option value="0" style="display:none">Select <?php print($name); ?></option>
                    <?php foreach ($selector as $option) { ?>
                      <option <?php if ($option['id'] == $selected[$name]) print('selected'); ?> value="<?php print($option['id']); ?>"><?php print($option['name']); ?></option>
                    <?php } ?>
                  </select>

                <?php }  }
                    foreach ($navs as $nav => $link) {
                        $target = "";
                        if (str_starts_with($link, "http://") || str_starts_with($link, "https://")) {
                            $target = 'target="_blank"';
                        }
                ?>

                    <a href="<?php print($link); ?>" <?php print($target); ?> class="nav-link-2 w-nav-link"><?php print($nav); ?></a>
                <?php } ?>

            </nav>
            <div class="w-nav-button">
              <div class="w-icon-nav-menu"></div>
            </div>
          </div>
        </div>


<?php

    }

    function foot() {
  
?>

    <iframe hidden="" name="transFrame" id="transFrame"></iframe>
    <script src="/js/footer.js"></script>
    </body>
    </html>

<?php } ?>