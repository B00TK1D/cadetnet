<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["elements"]);

    $admin = authenticate("admins")["admin"];

    $equipment["tag"] = param("a", ["displayError" => true, "sticky" => true]);
    $equipment["padded"] = str_pad($equipment["tag"], 4, "0", STR_PAD_LEFT);

    head("Register Label");
    equipmentCreateBlock($equipment, $admin);
    foot();
?>