<?php function equipmentMapBlock($points, $floor) { ?>

    <style type="text/css">
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }

      /* Optional: Makes the sample page fill the window. */
      html,
      body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .firstHeading {
        font-family: Exo, sans-serif;
        color: #fa3379;
      }
      .bodyContent {
        font-family: Exo, sans-serif;
        color: #323232;
      }
    </style>
    <script>

      var TILE_SIZE = 256;


      //Mercator --BEGIN--
      function bound(value, opt_min, opt_max) {
          if (opt_min !== null) value = Math.max(value, opt_min);
          if (opt_max !== null) value = Math.min(value, opt_max);
          return value;
      }

      function degreesToRadians(deg) {
          return deg * (Math.PI / 180);
      }

      function radiansToDegrees(rad) {
          return rad / (Math.PI / 180);
      }

      function MercatorProjection() {
          this.pixelOrigin_ = new google.maps.Point(TILE_SIZE / 2,
          TILE_SIZE / 2);
          this.pixelsPerLonDegree_ = TILE_SIZE / 360;
          this.pixelsPerLonRadian_ = TILE_SIZE / (2 * Math.PI);
      }

      MercatorProjection.prototype.fromLatLngToPoint = function (latLng,
      opt_point) {
          var me = this;
          var point = opt_point || new google.maps.Point(0, 0);
          var origin = me.pixelOrigin_;

          point.x = origin.x + latLng.lng() * me.pixelsPerLonDegree_;

          // NOTE(appleton): Truncating to 0.9999 effectively limits latitude to
          // 89.189.  This is about a third of a tile past the edge of the world
          // tile.
          var siny = bound(Math.sin(degreesToRadians(latLng.lat())), - 0.9999,
          0.9999);
          point.y = origin.y + 0.5 * Math.log((1 + siny) / (1 - siny)) * -me.pixelsPerLonRadian_;
          return point;
      };

      MercatorProjection.prototype.fromPointToLatLng = function (point) {
          var me = this;
          var origin = me.pixelOrigin_;
          var lng = (point.x - origin.x) / me.pixelsPerLonDegree_;
          var latRadians = (point.y - origin.y) / -me.pixelsPerLonRadian_;
          var lat = radiansToDegrees(2 * Math.atan(Math.exp(latRadians)) - Math.PI / 2);
          return new google.maps.LatLng(lat, lng);
      };

      //Mercator --END--

      function getNewRadius(radius) {
        var numTiles = 1 << map.getZoom();
        var center = map.getCenter();
        var moved = google.maps.geometry.spherical.computeOffset(center, 10000, 90); /*1000 meters to the right*/
        var projection = new MercatorProjection();
        var initCoord = projection.fromLatLngToPoint(center);
        var endCoord = projection.fromLatLngToPoint(moved);
        var initPoint = new google.maps.Point(initCoord.x * numTiles, initCoord.y * numTiles);
        var endPoint = new google.maps.Point(endCoord.x * numTiles, endCoord.y * numTiles);
        var pixelsPerMeter = (Math.abs(initPoint.x-endPoint.x))/10000.0;
        var totalPixelSize = Math.floor(radius*pixelsPerMeter);
        return totalPixelSize;
      }


      let map;

      function initMap() {
        map = new google.maps.Map(document.getElementById("map"), {
          center: new google.maps.LatLng("39.008623", "-104.889305"),
          zoom: 18,
          tilt: 0,
          mapTypeId: "roadmap",
        });
        const features = [

            <?php
                foreach ($points as $point) { 
                    if ($point["type"] == "ap") {
                        $iconPath = "/img/ap-icon.png";
                    } else if ($point["type"] == "switch") {
                        $iconPath = "/img/switch-icon.png";
                    } else if ($point["type"] == "modem") {
                        $iconPath = "/img/modem-icon.png";
                    } else {
                        $iconPath = "/img/dot-icon.png";
                    }
                    if ($point["type"] != "ignore") {
                        print("{ position: new google.maps.LatLng(" . $point["lat"] . ", " . $point["lon"] . "),
                                 label: '". addslashes($point["name"]) . "', equipment: '" . $point["id"] . "', tag: '" . $point["tag"] . "',
                                 location: new google.maps.LatLng(" . $point["lat"] . ", " . $point["lon"] . "),
                                 iconPath: '" . $iconPath . "',
                                 weight: 2,
                                 network: '" . $point["network"] . "',
                                 mac: '" . $point["mac"] . "',
                                 sn: '" . $point["sn"] . "',
                                 notes: '" . $point["notes"] . "'
                        }, ");
                    }
                }

            ?>
        ];

        const heatItems = [

            <?php
                foreach ($points as $point) { 
                    if ($point["type"] == "ap") {
                        $iconPath = "/img/ap-icon.png";
                        print("{ position: new google.maps.LatLng(" . $point["lat"] . ", " . $point["lon"] . "),
                                 label: '". addslashes($point["name"]) . "',
                                 equipment: '". $point["tag"] . "',
                                 location: new google.maps.LatLng(" . $point["lat"] . "," . $point["lon"] . "),
                                 iconPath: '" . $iconPath . "',
                                 weight: 2,
                                 network: '" . $point["network"] . "',
                                 mac: '" . $point["mac"] . "',
                                 sn: '" . $point["sn"] . "',
                                 notes: '" . $point["notes"] . "'
                        }, ");
                    }
                }

            ?>
        ];

        // Create markers
        for (let i = 0; i < features.length; i++) {
          const marker = new google.maps.Marker({
            position: features[i].position,
            label: features[i].label,
            map: map,
            draggable: true,
            icon: {
                url: features[i].iconPath,
                size: new google.maps.Size(24, 24),
                scaledSize: new google.maps.Size(24, 18),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(9,9),
                labelOrigin: new google.maps.Point(0,-10),
            },

            animation: google.maps.Animation.DROP,
            equipment: features[i].equipment,
          });
          const contentString =
            '<div id="content">' +
            '<div id="siteNotice">' +
            "</div>" +
            '<h1 id="firstHeading" class="firstHeading">' + features[i].label + '</h1>' +
            '<div id="bodyContent">' +
            '<p><b>Network:</b> ' + features[i].network + '<p/>' +
            '<p><b>MAC:</b> ' + features[i].mac + '<p/>' +
            '<p><b>Serial Number:</b> ' + features[i].sn + '<p/>' +
            '<p><b>Notes:</b> ' + features[i].notes + '<p/>' +
            '<p><a href="/equipment/update?a=' + features[i].tag + '" target="_blank"><b>Edit label</b></a></p>' +
            '</div>' +
            '</div>';
          const infowindow = new google.maps.InfoWindow({
            content: contentString,
          });
          google.maps.event.addListener(marker, 'dragend', function(e) {
            console.log(marker.equipment);
            document.getElementById('call').src = "/api/equipment/update?equipment=" + marker.equipment + "&lat=" + e.latLng.lat() + "&lon=" + e.latLng.lng();
          });
          google.maps.event.addListener(marker, 'click', function(e) {
            infowindow.open(map, marker);
          });
        }


        var heatmap = [];
        heatmap[20] = new google.maps.visualization.HeatmapLayer({
            data: heatItems,
            radius: getNewRadius(20),
            map: map,
            range: 20
          });




        /*
            foreach ($points as $index => $point) { 
                if ($point["type"] == "ap") {
                    $iconPath = "/img/ap-icon.png";
                    print("heatmap[" . $index . "] = new google.maps.visualization.HeatmapLayer({data: ");
                    print("[{ position: new google.maps.LatLng(" . $point["lat"] . ", " . $point["lon"] . "),
                              label: '". addslashes($point["name"]) . "',
                              equipment: '". $point["tag"] . "',
                              location: new google.maps.LatLng(" . $point["lat"] . "," . $point["lon"] . "),
                              iconPath: '" . $iconPath . "',
                              weight: 2,
                              network: '" . $point["network"] . "',
                              mac: '" . $point["mac"] . "',
                              sn: '" . $point["sn"] . "',
                              notes: '" . $point["notes"] . "'
                    }],");
                    print("radius: getNewRadius(" . $point["radius"] . "), map: map, range: " . $point["radius"] . "});");
                }
            }

        */

        /*var heatmap = new google.maps.visualization.HeatmapLayer({
            data: heatItems,
            radius: getNewRadius(),
            map: map,
        });*/

        const sijanBounds = {
            north: 39.007208,
            south: 39.006443,
            east: -104.888440,
            west:  -104.891846,
        };
        sijanBlueprints = new google.maps.GroundOverlay(
            "/img/maps/2348-<?php print($floor) ?>.png",
            sijanBounds
        );
        sijanBlueprints.setMap(map);


        const vandyBounds = {
            north: 39.01035470087656, 
            south: 39.009651258030445,
            east: -104.88542928049931,
            west:  -104.89011755962427
        };
        vandyBlueprints = new google.maps.GroundOverlay(
            "/img/maps/2360-<?php print($floor) ?>.png",
            vandyBounds
        );
        vandyBlueprints.setMap(map);

        google.maps.event.addListener(map, 'zoom_changed', function () {
              heatmap.forEach((h, i) => h.setOptions({radius:getNewRadius(h.range)}));
          });


      }
    </script>
  </head>
  <body>
    <div id="map"></div>
    <img id="call" style="display: none"/>

    <!-- Async script executes immediately and must be after any DOM elements used in callback. -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqP6lm5vtnLy6wQem6_KiQWim7GfrhP28&callback=initMap&libraries=visualization,geometry&v=weekly" async></script>

<?php } function equipmentCreateBlock($equipment, $admin) { ?>

    <div class="div-block-61">
      <div class="div-block-62"></div>
      <div>
        <h1 class="heading-18">CadetNet<br />Tag</h1>
      </div>
      <div class="div-block-65">
        <div class="text-block-18">Register Tag #<?php print($equipment["padded"]); ?></div>
        <div class="form-block w-form">
          <form class="validate-form" action="/api/equipment/create" method="post">
            <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Tag Name" required="" />
            <select name="model" class="cn-field _2 w-select">
              <option value="0" style="display:none">Select Model</option>
              <?php foreach (readObject("models") as $model) { ?>
                <option value="<?php print($model['id']); ?>"><?php print($model['name']); ?></option>
              <?php } ?>
            </select>

            <input type="text" class="cn-field w-input mac-format" maxlength="256" name="mac" placeholder="MAC"/>
            <input type="text" class="cn-field w-input" maxlength="256" name="sn" placeholder="Serial Number"/>

            <?php if ($admin["tier"] == config("admin-tier.global")) { ?>

              <select name="network" class="cn-field _2 w-select">
              <option value="0" style="display:none">Select Network</option>
                <?php foreach (readObject("networks") as $lnetwork) { ?>
                  <option value="<?php print($lnetwork['id']); ?>"><?php print($lnetwork['name']); ?></option>
                <?php } ?>
              </select>

            <?php } ?>
                
            <input type="text" name="lat" id="lat" value="0" hidden=""/> 
            <input type="text" name="lon" id="lon" value="0" hidden=""/> 
            <textarea placeholder="Notes" maxlength="4096" name="notes" class="cn-field w-input"></textarea>
            <input type="text" name="tag" value="<?php print($equipment["tag"]); ?>" hidden=""/> 
            <div class="div-block-66"><a class="cn-button _2 w-button">CANCEL</a><input type="submit" class="cn-button apply-new-device w-button" value="APPLY"></div></div>
        </div>
      </div>
    </div>
    <script src="/js/jquery.js"></script>
    <script>
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                function(position) {
                    var lat = position.coords.latitude;
                    var lon = position.coords.longitude;
                    document.getElementById('lat').value = lat;
                    document.getElementById('lon').value = lon;
                },
                function error(msg) {alert('Please enable your GPS position feature.');},
                {maximumAge:5000, timeout:10000, enableHighAccuracy: true});
        } else {
            console.log("Geolocation API is not supported in your browser.");
        }

        $(".validate-form").each(function(index) {
            var form = this;
            $(".mac-format", this).each(function(elem) {
                this.setAttribute("minLength", "17");
                this.setAttribute("maxLength", "17");
                this.addEventListener("input", function (e) {
                    regex = /[a-fA-F0-9]/g;
                    if (this.value.length > 17 || !regex.test(this.value.slice(-1))) {
                        this.value = this.value.slice(0, -1);
                    } 
                    if (this.value.length % 3 == 0 && this.value.length > 0) {
                        this.value = this.value.slice(0, -1) + ":" + this.value.slice(-1);
                    }
                    this.value = this.value.toLowerCase();
                    if (this.value.length == 17) {
                       form.elements[form.elements.length - 1].disabled = false;
                    } else {
                        form.elements[form.elements.length - 1].disabled = true;
                    }
                });
            });
        });
    </script>

<?php } function equipmentUpdateBlock($equipment, $admin) { ?>

    <div class="div-block-61">
      <div class="div-block-62"></div>
      <div>
        <h1 class="heading-18">CadetNet<br />Tag</h1>
      </div>
      <div class="div-block-65">
        <div class="text-block-18">Update Tag #<?php print($equipment["padded"]); ?></div>
        <div class="form-block w-form">
          <form class="validate-form" action="/api/equipment/update" method="post">
            <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Tag Name" required="" value="<?php print($equipment['name']); ?>"/>
            <select name="model" class="cn-field _2 w-select">
              <?php foreach (readObject("models") as $model) { ?>
                <option <?php if ($equipment['type'] == $model['id']) print('selected'); ?> value="<?php print($model['id']); ?>"><?php print($model['name']); ?></option>
              <?php } ?>
            </select>

            <input type="text" class="cn-field w-input mac-format" maxlength="256" name="mac" placeholder="MAC" value="<?php print($equipment['mac']); ?>"/>
            <input type="text" class="cn-field w-input" maxlength="256" name="sn" placeholder="Serial Number" value="<?php print($equipment['sn']); ?>"/>

            <?php if ($admin["tier"] == config("admin-tier.global")) { ?>

              <select name="network" class="cn-field _2 w-select">
                <?php foreach (readObject("networks") as $lnetwork) { ?>
                  <option <?php if ($equipment['network'] == $lnetwork['id']) print('selected'); ?> value="<?php print($lnetwork['id']); ?>"><?php print($lnetwork['name']); ?></option>
                <?php } ?>
              </select>

            <?php } ?>
                
            <input type="text" name="equipment" value="<?php print($equipment['id']); ?>" hidden=""/> 
            <textarea placeholder="Notes" maxlength="4096" name="notes" class="cn-field w-input"><?php print($model["notes"]) ?></textarea>
            <div class="div-block-66"><a class="cn-button _2 w-button">CANCEL</a><input type="submit" class="cn-button apply-new-device w-button" value="APPLY"></div></div>
            <form action="/api/equipment/update" method="post">
              <input type="text" name="equipment" value="<?php print($equipment['id']) ?>" hidden=""/>
              <input type="text" name="lat" id="lat" value="0" hidden=""/> 
              <input type="text" name="lon" id="lon" value="0" hidden=""/> 
              <div class="div-block-66"><input type="submit" class="cn-button apply-new-device w-button" value="UPDATE LOCATION"></div></div>
            </form>
        </div>
      </div>
    </div>
    <script src="/js/jquery.js"></script>
    <script>
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                function(position) {
                    var lat = position.coords.latitude;
                    var lon = position.coords.longitude;
                    document.getElementById('lat').value = lat;
                    document.getElementById('lon').value = lon;
                },
                function error(msg) {alert('Please enable your GPS position feature.');},
                {maximumAge:5000, timeout:10000, enableHighAccuracy: true});
        } else {
            console.log("Geolocation API is not supported in your browser.");
        }


        $(".validate-form").each(function(index) {
            var form = this;
            $(".mac-format", this).each(function(elem) {
                this.setAttribute("minLength", "17");
                this.setAttribute("maxLength", "17");
                this.addEventListener("input", function (e) {
                                        regex = /[a-fA-F0-9]/g;
                    if (this.value.length > 17 || !regex.test(this.value.slice(-1))) {
                        this.value = this.value.slice(0, -1);
                    } 
                    if (this.value.length % 3 == 0 && this.value.length > 0) {
                        this.value = this.value.slice(0, -1) + ":" + this.value.slice(-1);
                    }
                    this.value = this.value.toLowerCase();
                    if (this.value.length == 17) {
                       form.elements[form.elements.length - 1].disabled = false;
                    } else {
                        form.elements[form.elements.length - 1].disabled = true;
                    }
                });
            });
        });
    </script>

<?php } function equipmentViewBlock($equipment) { ?>

    <div class="div-block-61">
      <div class="div-block-62"></div>
      <div>
        <h1 class="heading-18">CadetNet<br />Tag</h1>
      </div>
      <div class="div-block-64">
        <div class="div-block-65">
            <div class="text-block-16 small">Name: <?php print($equipment["name"]) ?><br /></div>
            <div class="text-block-16 small">Model: <?php print($equipment["model-name"]) ?>‍<br /></div>
            <div class="text-block-16 small">Type: <?php print($equipment["model-type"]) ?>‍<br /></div>
            <div class="text-block-16 small">Network: <?php print($equipment["network-name"]) ?><br /></div>
            <div class="text-block-16 small">MAC: <?php print($equipment["mac"]) ?></div>
            <div class="text-block-16 small">Serial Number: <?php print($equipment["sn"]) ?><br /></div>
            <?php if ($equipment["contact"] != null) { ?><div class="text-block-16 small">Contact: <?php print($equipment["contact"]) ?><br /></div><?php } ?>
            <?php if ($equipment["notes"]) { ?><div class="text-block-16 small">Notes:<br /><?php print($equipment["notes"]) ?><br /></div><?php } ?>
        </div>
      </div>
      <div class="div-block-79">
       <a href="/equipment/update?a=<?php print($equipment['tag']); ?>" class="submit-button-2 _2 w-button">Update</a>
      </div>
    </div>

<?php }  function modelsBlock() { ?>

<div class="div-block-63"></div>
<div class="div-block-64">
  <div>
    <h1 class="heading-19">MODELS</h1>


    <?php foreach (readObject("models") as $model) {
        $equipment = readObject("equipment", ["model" => $model["id"]]);
        $model["cost-string"] = number_format($model["cost"]/100, 2, '.', ' ');
    ?>

      <div class="div-block-65">
        <div class="text-block-16 bold"><?php print($model["name"]); ?></div>
        <div class="div-block-66 seperate">
          <div class="cred-holder">
            <div class="text-block-16">Type: <?php print(config($model["type"], "equipment.type")); ?></div>
            <div class="text-block-16">Vendor: <?php print($model["vendor"]); ?></div>
            <div class="text-block-16">Cost: $<?php print($model["cost-string"]); ?></div>
            <div class="text-block-16">Range: <?php print($model["radius"]); ?> feet</div>
            <div class="text-block-16">Inventory: <?php print(count($equipment)); ?></div>
            <div class="text-block-16 small"><br /><?php print($model["notes"]) ?><br /></div>
          </div>
        </div>
        <div class="div-block-66 action-container">
          <a class="button-5 edit-device-button w-button"></a>
          <a class="button-5 _2 delete-button w-button"></a>
          <div class="delete-confirmation delete-confirm">
            <div class="div-block-65">
              <div class="text-block-16-copy">Delete model <?php print($model['name']); ?>?</div>
              <div class="div-block-66">
                <a class="cn-button _2 cancel-delete w-button">CANCEL</a>
                <form action="/api/equipment/model/delete" method="post" target="transFrame">
                  <input type="text" name="model" value="<?php echo $model['id'];?>" hidden="">
                  <input type="submit" class="cn-button apply-delete w-button" value="APPLY">
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="edit-device">
          <div class="form-block w-form">
          <form action="/api/equipment/model/update" target="transFrame" method="post">
              <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Name"required="" value="<?php print($model['name']); ?>"/>
              <select name="type" class="cn-field _2 w-select">
                <?php foreach (config("equipment.type") as $id => $type) { ?>
                  <option <?php if ($model['type'] == $id) print('selected'); ?> value="<?php print($id); ?>"><?php print($type); ?></option>
                <?php } ?>
              </select>
              <input type="text" class="cn-field w-input" maxlength="256" name="vendor" placeholder="Vendor"required="" value="<?php print($model['vendor']); ?>"/>
              <input type="number" min="0.01" step="0.01" max="100000" class="cn-field w-input" name="cost" placeholder="Cost" required="" value="<?php print($model['cost-string']); ?>" />
              <input type="number" min="0" step="0.1" max="1000" class="cn-field w-input" name="radius" placeholder="Range (feet)" required="" value="<?php print($model['radius']); ?>" />
              <textarea placeholder="Notes" maxlength="4096" name="notes" class="cn-field w-input"><?php print($model["notes"]) ?></textarea>
              <input type="text" name="model" value="<?php echo $model['id'];?>" hidden="">
              <div class="div-block-66">
                <a class="cn-button _2 cancel-edit-device w-button">CANCEL</a>
                <input type="submit" class="cn-button apply-edit-user w-button" value="APPLY">
              </div>
            </form>
          </div>
        </div>
      </div>


      <?php } ?>

      <div class="div-block-65 new-device">
        <div class="form-block w-form">
          <form action="/api/equipment/model/create" target="transFrame" method="post">
            <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Name"required=""/>
            <select name="type" class="cn-field _2 w-select">
              <option value="0" style="display:none">Select Type</option>
              <?php foreach (config("equipment.type") as $id => $type) { ?>
                <option value="<?php print($id); ?>"><?php print($type); ?></option>
              <?php } ?>
              </select>
            <input type="text" class="cn-field w-input" maxlength="256" name="vendor" placeholder="Vendor"required=""/>
            <input type="number" min="0.01" step="0.01" max="100000" class="cn-field w-input" name="cost" placeholder="Cost" required=""/>
            <input type="number" min="0" step="0.1" max="1000" class="cn-field w-input" name="radius" placeholder="Range (feet)" required=""/>
            <textarea placeholder="Notes" maxlength="4096" name="notes" class="cn-field w-input"></textarea>
            <div class="div-block-66">
              <a class="cn-button _2 cancel-new-device w-button">CANCEL</a>
              <input type="submit" class="cn-button apply-edit-user w-button" value="APPLY">
            </div>
          </form>
        </div>
      </div>
        <a class="div-block-65 hover new-device-button w-inline-block">
          <div class="div-block-66-copy">
            <div class="button-5 _3"></div>
          </div>
        </a>
      </div>
    </div>

  </div>

  <script src="/js/jquery.js"></script>
  <script>

  // Edit device
  $(".edit-device-button").each(function(index) {
      this.addEventListener("click", function() {
        this.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "auto";
      });
    });

    $(".cancel-edit-device").each(function(index) {
      this.addEventListener("click", function() {
        this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
      });
    });

    $(".apply-edit-device").each(function(index) {
      this.addEventListener("click", function() {
        this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
      });
    });

    // New device
    $(".new-device-button").each(function(index) {
      this.addEventListener("click", function() {
        this.style.height = "0px";
        this.parentNode.getElementsByClassName("new-device")[0].style.height = "auto";
      });
    });

    $(".cancel-new-device").each(function(index) {
      this.addEventListener("click", function() {
        this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
        this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

      });
    });

    $(".apply-new-device").each(function(index) {
      this.addEventListener("click", function() {
        this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
        this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

      });
    });


    $(".delete-button").each(function(index) {
      this.addEventListener("click", function() {
        this.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "flex";
      });
    });

    $(".cancel-delete").each(function(index) {
      this.addEventListener("click", function() {
        this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
      });
    });

    $(".apply-delete").each(function(index) {
      this.addEventListener("click", function() {
        //this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
      });
    });

    $('[type=submit]').each(function(index) {
            this.addEventListener("click", function() {
                setTimeout(function () {location.reload()}, 1000);
          });
        });


</script>


<?php }   function equipmentBlock($equipmentList, $admin) { ?>

<div class="div-block-63"></div>
<div class="div-block-64">
  <div>
    <h1 class="heading-19">CADETNET EQUIPMENT</h1>


    <?php foreach ($equipmentList as $equipment) { ?>

      <div class="div-block-65">
        <div class="text-block-16 bold"><?php print($equipment["name"]); ?></div>
        <div class="div-block-66 seperate">
          <div class="cred-holder">
            <div class="text-block-16 small">Model: <?php print($equipment["model-name"]) ?>‍<br /></div>
            <div class="text-block-16 small">Type: <?php print($equipment["type"]) ?>‍<br /></div>
            <div class="text-block-16 small">Network: <?php print($equipment["network-name"]) ?><br /></div>
            <div class="text-block-16 small">MAC: <?php print($equipment["mac"]) ?></div>
            <div class="text-block-16 small">Serial Number: <?php print($equipment["sn"]) ?><br /></div>
            <?php if ($equipment["contact"] != null) { ?><div class="text-block-16 small">Contact: <?php print($equipment["contact"]) ?><br /></div><?php } ?>
            <?php if ($equipment["notes"]) { ?><div class="text-block-16 small">Notes:<br /><?php print($equipment["notes"]) ?><br /></div><?php } ?>
          </div>
        </div>
        <div class="div-block-66 action-container">
          <a class="button-5 edit-device-button w-button"></a>
          <form action="/api/equipment/update" method="post" target="transFrame">
              <input type="text" name="equipment" value="<?php print($equipment['id']) ?>" hidden=""/>
              <input type="text" name="lat" id="lat" value="0" hidden=""/> 
              <input type="text" name="lon" id="lon" value="0" hidden=""/> 
              <input type="submit" class="button-5 login-icon-button w-button" value=""/>
            </form>
          <a class="button-5 _2 delete-button w-button"></a>
          <div class="delete-confirmation delete-confirm">
            <div class="div-block-65">
              <div class="text-block-16-copy">Delete equipment <?php print($equipment['name']); ?>?</div>
              <div class="div-block-66">
                <a class="cn-button _2 cancel-delete w-button">CANCEL</a>
                <form action="/api/equipment/delete" method="post">
                  <input type="text" name="equipment" value="<?php echo $equipment['id'];?>" hidden="">
                  <input type="text" name="return" value="/equipment/list" hidden=""/>
                  <input type="submit" class="cn-button apply-delete w-button" value="APPLY">
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="edit-device">
          <div class="form-block w-form">
          <form class="validate-form" action="/api/equipment/update" method="post">
            <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Tag Name" required="" value="<?php print($equipment['name']); ?>"/>
            <select name="model" class="cn-field _2 w-select">
              <?php foreach (readObject("models") as $model) { ?>
                <option <?php if ($equipment['type'] == $model['id']) print('selected'); ?> value="<?php print($model['id']); ?>"><?php print($model['name']); ?></option>
              <?php } ?>
              </select>

              <input type="text" class="cn-field w-input mac-format" maxlength="256" name="mac" placeholder="MAC" value="<?php print($equipment['mac']); ?>"/>
              <input type="text" class="cn-field w-input" maxlength="256" name="sn" placeholder="Serial Number" value="<?php print($equipment['sn']); ?>"/>

              <?php if ($admin["tier"] == config("admin-tier.global")) { ?>

                <select name="network" class="cn-field _2 w-select">
                  <?php foreach (readObject("networks") as $lnetwork) { ?>
                    <option <?php if ($equipment['network'] == $lnetwork['id']) print('selected'); ?> value="<?php print($lnetwork['id']); ?>"><?php print($lnetwork['name']); ?></option>
                  <?php } ?>
                </select>

              <?php } ?>
                  
              <input type="text" name="equipment" value="<?php print($equipment['id']); ?>" hidden=""/> 
              <textarea placeholder="Notes" maxlength="4096" name="notes" class="cn-field w-input"><?php print($model["notes"]) ?></textarea> 
              <input type="text" name="equipment" value="<?php echo $equipment['id'];?>" hidden="">
              <input type="text" name="return" value="/equipment/list" hidden=""/>
              <div class="div-block-66">
                <a class="cn-button _2 cancel-edit-device w-button">CANCEL</a>
                <input type="submit" class="cn-button apply-edit-user w-button" value="APPLY">
              </div>
            </form>
          </div>
        </div>
      </div>


      <?php } ?>

      <div class="div-block-65 new-device">
        <div class="form-block w-form">
          <form class="validate-form" action="/api/equipment/model/create" method="post">
            <input type="number" class="cn-field w-input mac-format" maxlength="10" name="tag" placeholder="Tag ID"/>
            <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Equipment Name" required="" />
            <select name="model" class="cn-field _2 w-select">
              <option value="0" style="display:none">Select Model</option>
              <?php foreach (readObject("models") as $model) { ?>
                <option value="<?php print($model['id']); ?>"><?php print($model['name']); ?></option>
              <?php } ?>
            </select>

            <input type="text" class="cn-field w-input mac-format" maxlength="256" name="mac" placeholder="MAC"/>
            <input type="text" class="cn-field w-input" maxlength="256" name="sn" placeholder="Serial Number"/>

            <?php if ($admin["tier"] == config("admin-tier.global")) { ?>

              <select name="network" class="cn-field _2 w-select">
              <option value="0" style="display:none">Select Network</option>
                <?php foreach (readObject("networks") as $lnetwork) { ?>
                  <option value="<?php print($lnetwork['id']); ?>"><?php print($lnetwork['name']); ?></option>
                <?php } ?>
              </select>

            <?php } ?>
                
            <input type="text" name="lat" id="lat2" value="0" hidden=""/> 
            <input type="text" name="lon" id="lon2" value="0" hidden=""/> 
            <textarea placeholder="Notes" maxlength="4096" name="notes" class="cn-field w-input"></textarea>
            <input type="text" name="tag" value="<?php print($equipment["tag"]); ?>" hidden=""/>
            <input type="text" name="return" value="/equipment/list" hidden=""/>
            <div class="div-block-66">
              <a class="cn-button _2 cancel-new-device w-button">CANCEL</a>
              <input type="submit" class="cn-button apply-edit-user w-button" value="APPLY">
            </div>
          </form>
        </div>
      </div>
        <a class="div-block-65 hover new-device-button w-inline-block">
          <div class="div-block-66-copy">
            <div class="button-5 _3"></div>
          </div>
        </a>
      </div>
    </div>

  </div>

  <script src="/js/jquery.js"></script>
  <script>

  // Edit device
  $(".edit-device-button").each(function(index) {
      this.addEventListener("click", function() {
        this.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "auto";
      });
    });

    $(".cancel-edit-device").each(function(index) {
      this.addEventListener("click", function() {
        this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
      });
    });

    $(".apply-edit-device").each(function(index) {
      this.addEventListener("click", function() {
        this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
      });
    });

    // New device
    $(".new-device-button").each(function(index) {
      this.addEventListener("click", function() {
        this.style.height = "0px";
        this.parentNode.getElementsByClassName("new-device")[0].style.height = "auto";
      });
    });

    $(".cancel-new-device").each(function(index) {
      this.addEventListener("click", function() {
        this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
        this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

      });
    });

    $(".apply-new-device").each(function(index) {
      this.addEventListener("click", function() {
        this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
        this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

      });
    });


    $(".delete-button").each(function(index) {
      this.addEventListener("click", function() {
        this.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "flex";
      });
    });

    $(".cancel-delete").each(function(index) {
      this.addEventListener("click", function() {
        this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
      });
    });

    $(".apply-delete").each(function(index) {
      this.addEventListener("click", function() {
        //this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
      });
    });


    /*$('[type=submit]').each(function(index) {
        this.addEventListener("click", function() {
            setTimeout(function () {location.reload()}, 2000);
      });
    });*/



    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            function(position) {
                var lat = position.coords.latitude;
                var lon = position.coords.longitude;
                document.getElementById('lat').value = lat;
                document.getElementById('lon').value = lon;
                document.getElementById('lat2').value = lat;
                document.getElementById('lon2').value = lon;
            },
            function error(msg) {alert('Please enable your GPS position feature.');},
            {maximumAge:5000, timeout:10000, enableHighAccuracy: true});
    } else {
        console.log("Geolocation API is not supported in your browser.");
    }


    $(".validate-form").each(function(index) {
        var form = this;
        $(".mac-format", this).each(function(elem) {
            this.setAttribute("minLength", "17");
            this.setAttribute("maxLength", "17");
            this.addEventListener("input", function (e) {
                                    regex = /[a-fA-F0-9]/g;
                if (this.value.length > 17 || !regex.test(this.value.slice(-1))) {
                    this.value = this.value.slice(0, -1);
                } 
                if (this.value.length % 3 == 0 && this.value.length > 0) {
                    this.value = this.value.slice(0, -1) + ":" + this.value.slice(-1);
                }
                this.value = this.value.toLowerCase();
                if (this.value.length == 17) {
                    form.elements[form.elements.length - 1].disabled = false;
                } else {
                    form.elements[form.elements.length - 1].disabled = true;
                }
            });
        });
    });


</script>

<?php }  function successBlock($message) { ?>
    <div class="div-block-61">
        <div class="div-block-62"></div>
        <div class="div-block-60">
            <h1 class="heading-18">SUCCESS</h1>
        </div>
        <div class="text-block-18"><?php print($message); ?></div>
    </div>
<?php } ?>