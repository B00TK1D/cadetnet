<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    function getPossibleConnections($equipment) {
        $connections = [];
        if ($equipment["type"] == equipmentType("Access Point")) {
            $connections = array_merge($connections, readObject("equipment", ["network" => $equipment["network"], "type" => equipmentType("Switch")]));
        } elseif ($equipment["type"] == equipmentType("Switch")) {
            $connections = array_merge($connections, readObject("equipment", ["network" => $equipment["network"], "type" => equipmentType("Switch")]));
            $connections = array_merge($connections, readObject("equipment", ["network" => $equipment["network"], "type" => equipmentType("Modem")]));
        }
        return $connections;
    }

    function equipmentType($string) {
        return array_search($string, config("equipment.type"));
    }

?>