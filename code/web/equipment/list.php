<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["elements"]);

    $admin = authenticate("admins")["admin"];

    $network = $admin["network"];
    if ($admin["tier"] == config("admin-tier.global")) $network = param(["network", $network]);

    $equipmentList = readObject("equipment", ["network" => $network]);

    foreach ($equipmentList as $key => $equipment) {
        $equipmentList[$key]["network-name"] = readObject("networks", ["id" => $equipment["network"]], 1)["name"];
        $equipmentList[$key]["contact"] = readObject("admins", ["network" => $equipment["network"]], 1);
        if ($equipmentList[$key]["contact"] != null) $equipmentList[$key]["contact"] = $equipmentList[$key]["contact"]["email"];
        $equipmentList[$key]["model-name"] = readObject("models", ["id" => $equipment["model"]], 1)["name"];
    }
    
    //print_r($equipmentList)

    head("CadetNet Equipment");
    equipmentBlock($equipmentList, $admin);
    foot();
?>