<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["elements", "functs"]);

    $admin = authenticate("admins")["admin"];
    $points = [];
    $menu = [];
    $selects = [];
    $selected = [];
    $floor = 6;

    if ($admin["tier"] == config("admin-tier.global")) {
        $floor = param(["floor", 6], ["sticky" => true]);
        $points = readObject("equipment", ["floor" => $floor]);
        $selects = ["floor" => [
            ["id" => 2, "name" => "2nd Floor"],
            ["id" => 3, "name" => "3rd Floor"],
            ["id" => 5, "name" => "5th Floor"],
            ["id" => 6, "name" => "6th Floor"],
        ]];
        $selected = ["floor" => $floor];
    } else {
        $points = readObject("equipment", ["network" => $admin["network"]]);
        $floor = readObject("networks", ["id" => $admin["network"]], 1)["floor"];
    }

    foreach($points as $key => $point) {
        if ($point["type"] == equipmentType("Access Point")) {
            $points[$key]["type"] = "ap";
        } elseif ($point["type"] == equipmentType("Switch")) {
            $points[$key]["type"] = "switch";
        } elseif ($point["type"] == equipmentType("Modem")) {
            $points[$key]["type"] = "modem";
        } elseif ($point["type"] == equipmentType("Other")) {
            $points[$key]["type"] = "other";
        } else {
            $points[$key]["type"] = "ignore";
        }
        $points[$key]["network"] = readObject("networks", ["id" => $point["network"]], 1)["name"];
        $points[$key]["radius"] = readObject("models", ["id" => $point["model"]], 1)["radius"];
    }

    
    if ($admin["tier"] == config("admin-tier.global")) {
        $menu["MASTER"] = "/admin/master/dashboard";
        $networks = readObject("networks");
        $network["id"] = param(["network", $admin["network"]], ["sticky" => true]);
    }

    $menu["DASHBOARD"] = "/admin/dashboard";
    $menu["LOGOUT"] = "/admin/logout";

    //print_r($points);

    $floors = [
        ["id" => 2, "name" => "2nd Floor"],
        ["id" => 3, "name" => "3rd Floor"],
        ["id" => 5, "name" => "5th Floor"],
        ["id" => 6, "name" => "6th Floor"],
    ];

    head("Equipment Map", $menu, $selects, $selected);
    equipmentMapBlock($points, $floor, $admin);
    foot();
?>