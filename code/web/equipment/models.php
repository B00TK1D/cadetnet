<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["functs", "elements"]);

    $admin = authenticate("global-admins")["admin"];
    
    head("CadetNet Credentials", ["DASHBOARD" => "/admin/dashboard", "LOGOUT" => "/admin/logout"]);
    modelsBlock();
    foot();
?>