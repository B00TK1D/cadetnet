<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["elements"]);

    $code = param(["code", 0]);
    $message = message($code);

    head("Success");
    successBlock($message);
    foot();
?>