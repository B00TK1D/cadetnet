<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["elements"]);

    $tag = param("a", ["showError" => true, "sticky" => true]);

    $equipment = readObject("equipment", ["tag" => $tag], 1);
    if (!$equipment) redirect("/equipment/create?a=" . $tag);
    $equipment["padded"] = str_pad($tag, 4, "0", STR_PAD_LEFT);

    $admin = authenticate("owners", ["network" => $equipment["network"]])["admin"];
    
    head("Update Label");
    equipmentBlock([$equipment], $admin);
    foot();
?>