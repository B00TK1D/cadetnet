<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["elements"]);

    $tag = param("a", ["displayError" => true, "sticky" => true]);

    $equipment = readObject("equipment", ["tag" => $tag], 1);

    if ($equipment == null) redirect("/equipment/create?a=" . $tag);

    $equipment["contact"] = readObject("admins", ["network" => $equipment["network"]], 1);
    if ($equipment["contact"] != null) $equipment["contact"] = $equipment["contact"]["email"];
    $model = readObject("models", ["id" => $equipment["model"]], 1);
    $network = readObject("networks", ["id" => $equipment["network"]], 1);
    $equipment["model-type"] = config("equipment.type." . $model["type"]);
    $equipment["model-name"] = $model["name"];
    $equipment["network-name"] = $network["name"];
    

    head("CadetNet Equipment");
    equipmentViewBlock($equipment);
    foot();
?>