<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil(["xml", "token"]);


    function aggregateRequest($targets) {

        $responses = [];

        $ch_sid = [];
        $ch_xml = [];
        $mh_sid = curl_multi_init();
        $mh_xml = curl_multi_init();

        foreach ($targets as $target) {

            $ch_tmp = curl_init($target["url"]);

            curl_setopt( $ch_tmp, CURLOPT_PORT, $target["port"]);
            curl_setopt($ch_tmp, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch_tmp, CURLOPT_SSL_VERIFYPEER, 0);
            if (isset($target["timeout"])) {
                curl_setopt($ch_tmp, CURLOPT_TIMEOUT, $target["timeout"]);
            } else {
                curl_setopt($ch_tmp, CURLOPT_TIMEOUT, 10);
            }
            curl_setopt($ch_tmp, CURLOPT_RETURNTRANSFER, true);
            if (isset($target["method"]) && $target["method"] == "POST") {
                curl_setopt($ch_tmp, CURLOPT_POST, 1);
            }
            if (isset($target["data"])) {
                curl_setopt($ch_tmp, CURLOPT_POSTFIELDS, $target["data"]);
            }

            $ch_sid[] = $ch_tmp;
            curl_multi_add_handle($mh_sid, $ch_tmp);
        }

        $running = null;
        do {
            curl_multi_exec($mh_sid, $running);
        } while ($running);


        foreach ($ch_sid as $ch) {
            curl_multi_remove_handle($mh_sid, $ch);
        }
        curl_multi_close($mh_sid);

        $i = 0;
        foreach ($ch_sid as $ch) {
            $response = [];
            foreach ($targets[$i] as $key => $value) {
                $response[$key] = $value;
            }
            $response["data"] = curl_multi_getcontent($ch);
            if (isset($targets[$i]["id"])) {
                $responses[$targets[$i]["id"]] = $response;
            } else {
                $responses[] = $response;
            }
        
            $i++;
        }

        return $responses;
    }


    function getSIDs($networks) {
        global $status;

        $sids = [];

        $tempUser = randomString(config("token.length")) . "@cadetnet.org";
        $tempPass = randomString(config("token.length"));

        $admin = [
            "name" => "System",
            "email" => $tempUser,
            "password" => getHash($tempPass),
            "status" => config("verification.verified"),
            "tier" => config("admin-tier.global"),
        ];

        $tempAdminID = createObject("admins", $admin);

        $targets = [];

        foreach ($networks as $network) {

            if ($network == null || $network["ip"] == "0.0.0.0" || $network["ip"] == "") {
                continue;
            }
            $target = [];

            $target["url"] = "https://" . $network["ip"] . ":4343/swarm.cgi";
            $target["port"] = 4343;
            $target["method"] = "POST";
            $target["data"] = "opcode=login&user=$tempUser&passwd=$tempPass";
            $target["timeout"] = 5;
            $target["id"] = $network["id"];
            $target["ip"] = $network["ip"];

            $targets[] = $target;
        }

        $responses = aggregateRequest($targets);

        foreach ($responses as $response) {
        if ($response["data"] != "") {
            $sid = parseBetween($response["data"], '<data name="sid" pn="true">', '</data>');
            $ip = $response["ip"];
            $network = $response["id"];
            $sids[$network] = ["sid" => $sid, "ip" => $ip];
        }
        }

        deleteObject("admins", ["id" => $tempAdminID]);

        return $sids;
    }





    function runCommand($sids, $cmd) {

        $output = [];

        $targets = [];

        foreach ($sids as $id => $sid) {

            $target = [];

            $target["url"] = "https://" . $sid["ip"] . ":4343/swarm.cgi?"; //opcode=show&ip=127.0.0.1&cmd=%27" . urlencode($cmd["cmd"]) . "%27&refresh=false&sid=" . $sid["sid"];
            
            foreach ($cmd as $key => $value) {
                $target["url"] .= urlencode($key) . "=" . urlencode($value) . "&";
            }

            $target["url"] .= "sid=" . $sid["sid"];
            $target["port"] = 4343;
            $target["id"] = $id;
            $target["ip"] = $sid["ip"];

            $targets[] = $target;
        }

        $responses = aggregateRequest($targets);
        $parsed = [];
        foreach ($responses as $id => $response) {
            $append["Network"] = $id;
            $array = parseXML($response["data"], $append);
            $parsed[] = $array;
        }
        $output = mergeParsed($parsed);

        return $output;
    }



    function listClients($networks) {
        $sids = getSIDs($networks);
        $cmd = [
            "opcode" => "show",
            "ip" => "127.0.0.1",
            "cmd" => "'show clients'",
            "refresh" => "false"
        ];
        $output = runCommand($sids, $cmd);
        return $output;
    }

    function showClient($networks, $client) {
        $sids = getSIDs($networks);
        $cmd = [
            "opcode" => "show",
            "ip" => "127.0.0.1",
            "cmd" => "'show stats client $client'",
            "refresh" => "false"
        ];
        $output = runCommand($sids, $cmd);
        return $output;
    }

    function showNetwork($networks) {
        $sids = getSIDs($networks);
        $cmd = [
            "opcode" => "show",
            "ip" => "127.0.0.1",
            "cmd" => "'show stats global'",
            "refresh" => "false"
        ];
        $output = runCommand($sids, $cmd);
        return $output;
    }



    function arubaSwarm($url, $port) {
        if (str_starts_with($url, "/swarm.cgi")) {
            $networks = list_networks();
            $sids = getSIDs($networks);
            
            $output = aggrigateRequest($url, $sids);
        }
    }

    

    function macLookup($mac) {    
        return exec("cat manuf.txt | grep $(echo " . $mac . " | sed -E 's/([0-9a-f][0-9a-f]).?([0-9a-f][0-9a-f]).?([0-9a-f][0-9a-f]).?.*/\1:\2:\3/g' | awk '{print toupper($0)}') | cut -d$'\t' -f2");
    }
?>