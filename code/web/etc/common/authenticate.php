<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil("message");

    function authenticate($level, $options = []) {
        $value = authenticateValue($options);
        $level = configValue($level, "authenticate");
        if (!checkTimeout()) {
            authenticateFail($level, $options, code("timeout"));
        }
        if ($level <= $value["level"]) {
            renewTimeout();
            return $value;
        } else {
            authenticateFail($level, $options, code("no-permissions"));
        }
        die();
    }


    function authenticateValue($options) {
       startSession();
        $return["level"] = 0;

        $return["user"] = [];
        $return["admin"] = [];

        $userOwner = false;
        $networkOwner = false;
        $adminOwner = false;
        $userAdmin = false;

        if (isset($_SESSION["user"])) {
            $return["user"] = readObject("users", ["id" => $_SESSION["user"]], 1);
        }

        if (isset($_SESSION["admin"])) {
            $return["admin"] = readObject("admins", ["id" => $_SESSION["admin"]], 1);
        }

        if (isset($options["user"])) {
            $optionUser = readObject("users", ["id" => $options["user"]], 1);
            if (isset($optionUser["id"])) {
                if (isset($return["user"]["id"])) $userOwner = ($optionUser["id"] == $return["user"]["id"]);
                if (isset($return["admin"]["network"])) $userAdmin = ($optionUser["network"] == $return["admin"]["network"]);
            }
        }

        if (isset($options["admin"])) {
            $optionAdmin = readObject("admins", ["id" => $options["admin"]], 1);
            if (isset($optionAdmin["id"])) {
                if (isset($return["admin"]["id"])) $adminOwner = ($optionAdmin["id"] == $return["admin"]["id"]);
            }
        }

        if (isset($options["network"])) {
            if (isset($return["admin"]["network"])) {
                if (isset($return["admin"]["network"])) $networkOwner = ($return["admin"]["network"] == $options["network"]);
            }
        }

        if (isset($return["admin"]["network"])) {
            if($return["admin"]["tier"] == config("admin-tier.global")) {
                $return["level"] = config("authenticate.global-admins");
                return $return;
            }
        }

        if ($userAdmin) {
            $return["level"] = config("authenticate.admin_owners");
            return $return;
        }

        if ($userOwner || $adminOwner || $networkOwner) {
            $return["level"] = config("authenticate.owners");
            return $return;
        }

        if ($return["admin"]) {
            $return["level"] = config("authenticate.admins");
            return $return;
        }

        if ($return["user"]) {
            $return["level"] = config("authenticate.users");
            return $return;
        }

        $return["level"] = config("authenticate.open");
        return $return;
    }

    function renewTimeout() {
        startSession();
        $_SESSION["lastActivity"] = time();
    }

    function checkTimeout() {
        if (isset($_SESSION["lastActivity"]) && (time() - $_SESSION["lastActivity"] < config("session.timeout"))) {
            return 1;
        }
        return 0;
    }

    function authenticateFail($level, $options, $code) {
        if (isset($options["fail"])) {
            redirect($options["fail"] . "?code=" . $code . "&redirect=" . urlencode($_SERVER['REQUEST_URI']));
        } elseif ($level >= config("authenticate.admins")) {
            redirect("/admin/login?code=" . $code . "&redirect=" . urlencode($_SERVER['REQUEST_URI']));
        } else {
            redirect("/user/login?code=" . $code . "&redirect=" . urlencode($_SERVER['REQUEST_URI']));
        }
    }
?>