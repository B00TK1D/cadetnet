<?php
    global $config;
    $config = [
        "mysql" => [
            "web" => [
                "host" => "webdb",
                "database" => "web",
                "user" => "web",
                "password" => $_ENV["MYSQL_PHP_PASSWORD"],
            ],
            "deviceradius" => [
                "host" => "radiusdb",
                "database" => "deviceradius",
                "user" => "radius",
                "password" => $_ENV["MYSQL_RADIUS_PASSWORD"],
            ],
            "adminradius" => [
                "host" => "radiusdb",
                "database" => "adminradius",
                "user" => "radius",
                "password" => $_ENV["MYSQL_RADIUS_PASSWORD"],
            ],
            "logs" => [
                "host" => "logdb",
                "database" => "logs",
                "user" => "php",
                "password" => $_ENV["MYSQL_RADIUS_PASSWORD"],
            ],
        ],

        "sendgrid" => [
            "api_key" => $_ENV["SENDGRID_API_KEY"],
            "defaults" => [
                "from_email" => "no-reply@cadetnet.org",
                "from_name" => "CadetNet",
                "name" => "",
                "domain" => $_ENV["DEPLOYMENT_DOMAIN"],
            ],
            "templates" => [
                "verification" => "d-acd9aa85281042afa3a64824f7052c6f",
                "password-reset" => "d-702d3e1d9006475c81c221509fae51f0",
                "invoice" => "d-cd7ba25b4cfa42ccaa571b28c255b3db",
                "user-deleted" => "d-ec193aa66b7e4ff89a27387cf80f86f5",
                "user-exists" => "d-4e7cf9fd1ad34d02b072fd4b908c449c",
                "admin-onboard" => "d-fb951fe256b140debc39ccf289b39c15",
                "payment-failed" => "d-7058d7ddeab04b74be4db3eb0eb62659",
                "account-terminated" => "d-eb9ff4e38bbc47f2875e8722129056f9",
            ],
        ],

        "stripe" => [
            "secret_key" => $_ENV["STRIPE_SECRET_KEY"],
            "public_key" => $_ENV["STRIPE_PUBLIC_KEY"],
            "endpoint_secret" => $_ENV["STRIPE_ENDPOINT_SECRET"],
            "plans" => [
                "basic_plan" => "prod_JDICDXUE2FFXnS",
                "basic_plan_dev" => "prod_JDOevQmNuaANyh",
            ],
            "prices" => [
                "basic_plan" => "price_1IarbfBRrh9j4EyTXKzoMIA3",
                "basic_plan_dev" => "price_1IaxreBRrh9j4EyTckphVZy8",
            ],
        ],

        "token" => [
            "length" => 16,
            "expiration" => [
                "verification" => "+6 hours",
                "password-reset" => "+1 hour",
            ],
            "purpose" => [
                "error" => 0,
                "verification" => 1,
                "password-reset" => 2,
                "deletion" => 3,
                "admin-verify" => 4,
                "password-reset-admin" => 5,
            ],
        ],

        "verification" => [
            "manually_created" => 0,
            "unverified" => 1,
            "verified" => 2,
            "error" => 3,
            "payment-error" => 4,
        ],

        "notification" => [
            "public_key" => $_ENV["PUSH_PUBLIC_KEY"],
            "private_key" => $_ENV["PUSH_PRIVATE_KEY"],
            "status" => [
                "unsent" => 0,
                "sent" => 1,
                "read" => 2,
            ],
        ],

        "authenticate" => [
            "open" => 0,
            "users" => 1,
            "admins" => 2,
            "owners" => 3,
            "admin_owners" => 4,
            "global-admins" => 5,
        ],

        "admin-tier" => [
            "network" => 0,
            "global" => 10,
        ],

        "equipment" => [
            "type" => [
                "Modem",
                "Switch",
                "Access Point",
                "Cable",
                "Conduit",
                "Other",
            ],
        ],

        "api" => [
            "key" => "test",
        ],

        "session" => [
            "timeout" => 1800,
        ],

        "deployment" => [
            "domain" => $_ENV["DEPLOYMENT_DOMAIN"],
        ],

        "redirects" => [
            "/verify" => "/user/verify",
            "/verify-admin" => "/admin/verify",
            "/login" => "/user/login",
            "/admin" => "/admin/dashboard",
            "/dashboard" => "/user/dashboard",
            "/user" => "/user/dashboard",
            "/mac-help" => "/help/mac",
            "/info" => "/public/info",
        ],

        "issue" => [
            "type" => [
                "account" => 1,
                "setup" => 2,
                "connection" => 3,
                "other" => 4,
                "latency" => 5,
                "speed" => 6,
                "dropping" => 7,
                "blocked" => 8,
            ],
            "activity" => [
                "browsing" => 1,
                "school" => 2,
                "streaming" => 3,
                "gaming" => 4,
                "other" => 5,
            ],
            "frequency" => [
                "constant" => 1,
                "often" => 2,
                "sometimes" => 3,
                "rarely" => 4,
                "once" => 5,
            ],
            "status" => [
                "unresolved" => 1,
                "pending" => 2,
                "resolved" => 3,
            ],
        ],

    ];

    function config($key) {
        global $config;
        $exploded_key = explode(".", $key);
        $current_obj = $config;
        foreach ($exploded_key as $subkey) {
            if (isset($current_obj[$subkey]) ) {
                $current_obj = $current_obj[$subkey];
            } else {
                return null;
            }
        }
        return $current_obj;
    }

    function configValue(&$val, $pre = "") {
        if (is_string($val)) {
            return config($pre . "." . $val);
        }
        return $val;
    }

    function isDevEnv() {
        if (str_starts_with($_ENV["DEPLOYMENT_DOMAIN"], "dev.")) {
            return true;
        }
        return false;
    }
?>