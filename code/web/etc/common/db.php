<?php

    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");

    ini_set("extension", "php_mysqli.dll");

    // Create DB connection
    global $mysqli;
    $mysqli = new mysqli(config("mysql.web.host"), config("mysql.web.user"), config("mysql.web.password"), config("mysql.web.database"));

    // Define DB Schema
    global $dbSchema;
    $dbSchema = [
        "users" => [
            "table" => "users",
            "fields" => [
                "id" => "i",
                "network" => "i",
                "name" => "s",
                "email" => "s",
                "status" => "i",
                "password" => "s",
                "subscription" => "s",
                "customer" => "s",
                "last4" => "s",
                "coupon" => "s",
            ],
            "sort" => [
                "name" => "ASC"
            ],
        ],
        "admins" => [
            "table" => "admins",
            "fields" => [
                "id" => "i",
                "network" => "i",
                "name" => "s",
                "email" => "s",
                "status" => "i",
                "tier" => "s",
                "password" => "s"
            ],
            "sort" => [
                "network" => "ASC"
            ],
        ],
        "devices" => [
            "table" => "devices",
            "fields" => [
                "id" => "i",
                "user" => "i",
                "name" => "s",
                "mac" => "s",
                "ip" => "s",
                "os" => "s",
            ],
        ],
        "tokens" => [
            "table" => "tokens",
            "fields" => [
                "id" => "i",
                "string" => "s",
                "user" => "i",
                "purpose" => "i",
                "expiration" => "s",
            ],
        ],
        "networks" => [
            "table" => "networks",
            "fields" => [
                "id" => "i",
                "name" => "s",
                "card" => "s",
                "floor" => "i",
                "ip" => "s",
                "balance" => "i",
                "creator" => "i",
                "updator" => "i",
            ],
            "sort" => [
                "name" => "ASC",
            ],
            "protected" => [
                "creator",
                "updator",
            ],
        ],
        "credentials" => [
            "table" => "credentials",
            "fields" => [
                "id" => "i",
                "network" => "i",
                "name" => "s",
                "link" => "s",
                "username" => "s",
                "cred" => "s",
                "creator" => "i",
                "updator" => "i",
            ],
            "protected" => [
                "creator",
                "updator",
            ],
            "sort" => [
                "network" => "ASC",
            ],
        ],
        "equipment" => [
            "table" => "equipment",
            "fields" => [
                "id" => "i",
                "tag" => "s",
                "name" => "s",
                "model" => "i",
                "notes" => "s",
                "network" => "i",
                "ip" => "s",
                "mac" => "s",
                "sn" => "s",
                "lat" => "d",
                "lon" => "d",
                "floor" => "i",
                "creds" => "s",
                "type" => "i",
                "creator" => "i",
                "updator" => "i",
            ],
            "protected" => [
                "creator",
                "updator",
            ],
        ],
        "connections" => [
            "table" => "connections",
            "fields" => [
                "id" => "i",
                "type" => "i",
                "network" => "i",
                "parent" => "i",
                "child" => "i",
                "length" => "i",
                "notes" => "s",
                "creator" => "i",
                "updator" => "i",
            ],
            "protected" => [
                "creator",
                "updator",
            ],
        ],
        "models" => [
            "table" => "models",
            "fields" => [
                "id" => "i",
                "type" => "i",
                "name" => "s",
                "vendor" => "s",
                "cost" => "i",
                "radius" => "i",
                "notes" => "s",
                "creator" => "i",
                "updator" => "i",
            ],
            "protected" => [
                "creator",
                "updator",
            ],
        ],
        "issues" => [
            "table" => "issues",
            "fields" => [
                "id" => "i",
                "user" => "i",
                "name" => "s",
                "email" => "s",
                "network" => "i",
                "type" => "i",
                "priority" => "i",
                "notes" => "s",
                "time" => "i",
                "duration" => "i",
                "frequency" => "i",
                "activity" => "i",
                "device" => "i",
                "ap" => "i",
                "modem" => "i",
                "ip" => "s",
                "admin" => "i",
                "status" => "i",
                "prediction" => "i",
                "cause" => "i",
            ],
            "sort" => [
                "priority" => "DESC",
            ],
        ],
        "causes" => [
            "table" => "causes",
            "fields" => [
                "id" => "i",
                "name" => "s",
                "type" => "i",
                "problem" => "s",
                "solution" => "s",
                "creator" => "i",
                "updator" => "i",
            ],
            "protected" => [
                "creator",
                "updator",
            ],
        ],
        "messages" => [
            "table" => "messages",
            "fields" => [
                "id" => "i",
                "user" => "i",
                "admin" => "i",
                "issue" => "i",
                "status" => "i",
                "message" => "s",
            ],
        ],
        "notification_endpoints" => [
            "table" => "notification_endpoints",
            "fields" => [
                "id" => "i",
                "user" => "i",
                "device" => "i",
                "endpoint" => "s",
                "auth" => "s",
                "p256dh" => "s",
            ],
        ],
        "notifications" => [
            "table" => "notifications",
            "fields" => [
                "id" => "i",
                "redirect" => "s",
                "endpoint" => "i",
                "status" => "i",
            ],
        ],
        "alerts" => [
            "table" => "alerts",
            "fields" => [
                "id" => "i",
                "admin" => "i",
                "message" => "s",
                "link" => "s",
                "device" => "i",
                "user" => "i",
                "ap" => "i",
                "modem" => "i",
                "network" => "i",
                "status" => "i",
            ],
        ],
        "alert_responses" => [
            "table" => "alert_responses",
            "fields" => [
                "id" => "i",
                "alert" => "i",
                "response" => "i",
            ],
        ],
        "events" => [
            "table" => "events",
            "fields" => [
                "id" => "i",
                "time" => "i",
                "code" => "i",
                "user" => "i",
                "admin" => "i",
                "device" => "i",
                "user" => "i",
                "ap" => "i",
                "modem" => "i",
                "network" => "i",
                "status" => "i",
            ],
        ],
        "modem_logs" => [
            "table" => "modem_logs",
            "fields" => [
                "id" => "i",
                "time" => "i",
                "modem" => "i",
                "status" => "i",
                "latency" => "i",
            ],
        ],
        "ap_logs" => [
            "table" => "events",
            "fields" => [
                "id" => "i",
                "time" => "i",
                "ap" => "i",
                "status" => "i",
                "neighboring_aps_valid" => "i",
                "neighboring_aps_interfering" => "i",
                "neighboring_aps_rogue" => "i",
                "neighboring_clients_valid" => "i",
                "neighboring_clients_interfering" => "i",
                "cpu" => "i",
                "memory" => "i",
                "throughput_out" => "i",
                "throughput_in" => "i",
                "24_power" => "i",
                "24_channel" => "i",
                "24_utilization" => "i",
                "24_noise" => "i",
                "24_frames_in" => "i",
                "24_frames_out" => "i",
                "24_management_in" => "i",
                "24_management_out" => "i",
                "24_drops" => "i",
                "24_errors" => "i",
                "50_channel" => "i",
                "50_power" => "i",
                "50_utilization" => "i",
                "50_noise" => "i",
                "50_frames_in" => "i",
                "50_frames_out" => "i",
                "50_management_in" => "i",
                "50_management_out" => "i",
                "50_drops" => "i",
                "50_errors" => "i",
            ],
        ],
        "device_logs" => [
            "table" => "device_logs",
            "fields" => [
                "id" => "i",
                "time" => "i",
                "device" => "i",
                "status" => "i",
                "network" => "i",
                "modem" => "i",
                "ap" => "i",
                "channel" => "i",
                "type" => "i",
                "cm" => "i",
                "frames_in" => "i",
                "frames_out" => "i",
                "errors_in" => "i",
                "errors_out" => "i",
                "throughput_in" => "i",
                "throughput_out" => "i",
                "signal" => "i",
                "speed" => "i",
                "connection_time" => "i",
            ],
        ],
    ];


    // MySQL interface functions

    function createObject($name, $fields) { // Create object in MySQL DB
        global $mysqli, $dbSchema;
        if (!isset($dbSchema[$name])) return; // Check table name exists
        $query = "INSERT INTO " . $name . " (";
        $types = "";
        $placeholders = "";
        $params = [];
        $first = true;
        foreach ($fields as $key => $field) { // Add fields
            if (!isset($dbSchema[$name]["fields"][$key])) continue; // Check field exists
            $first ? $query .= $key : $query .= "," . $key;
            $first ? $placeholders .= "?" : $placeholders .= ",?";
            $types .= $dbSchema[$name]["fields"][$key];
            $params[] = $field;
            $first = false;
        }
        if (!count($params)) return; // Check at least one valid field exists
        $query .= ") VALUES (" . $placeholders . ");";
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param($types, ...$params);
        $stmt->execute();
        return mysqli_insert_id($mysqli); // Return ID of newly created object
    }
    

    function readObject($tables, $filters = [], $limit = 0, $sorts = []) { // List object(s) from MySQL DB, with optional filters (also can be used as select by setting $filter["id"], etc.)
        global $mysqli, $dbSchema;
        $names = [];
        $default = "";
        if (is_array($tables)) {
            $names = $tables;
        } else {
            $names[] = $tables;
            $default = $tables . ".";
        }
        if (!is_array($filters) || !is_array($sorts)) {
            error("internal-error");
        }
        $first = true;
        $second = true;
        $query = "";
        $types = "";
        $params = [];
        foreach ($names as $name) {
            if (!isset($dbSchema[$name])) {; // Check table name exists
                error("internal-error");
            }
            $first ? $query = "SELECT * FROM " . $name : ($second ? $query .= " INNER JOIN " . $name : ", " . $name);
        }
        if (count($filters)) { // Add filters to query
            $first = true;
            foreach ($filters as $key => $filter) {
                if (!isset($dbSchema[$name]["fields"][$key])) continue; // Check field exists
                $first ? $query .= " WHERE " . $default . $key . "=?" : $query .= " AND " . $default . $key . "=?";
                $types .= $dbSchema[$name]["fields"][$key];
                $params[] = $filter;
                $first = false;
            }
        }
        if (count($sorts)) {  // Add parameter-defined sort values to query (overrides table-defined sort values)
            $first = true;
            foreach ($sorts as $key => $sort) {
                if (!isset($dbSchema[$name]["fields"][$key])) continue; // Check field exists
                $first ? $query .= " ORDER BY " . $default . $key . " " . $sort: $query .= ", " . $default . $key . " " . $sort;
                //$types .= $dbSchema[$name]["fields"][$key];
                $first = false;
            }
        } else {
            $first = true;
            foreach ($names as $name) {
                if (isset($dbSchema[$name]["sort"])) { // Add table-defined sort values to query
                    foreach ($dbSchema[$name]["sort"] as $key => $sort) {
                        if (!isset($dbSchema[$name]["fields"][$key])) continue; // Check field exists
                        $first ? $query .= " ORDER BY " . $name . "." . $key . " " . $sort: $query .= ", " . $name . "." . $key . " " . $sort;
                        //$types .= $dbSchema[$name]["fields"][$key];
                        $first = false;
                    }
                }
            }
        }
        if ($limit) {
            $query .= " LIMIT " . $limit;
        }
        $query .= ";";
        //debug($query);
        $stmt = $mysqli->prepare($query);
        if(count($params)) $stmt->bind_param($types, ...$params);
        $stmt->execute();
        $result = $stmt->get_result();
        $rows = [];
        while($row = mysqli_fetch_array($result)) $rows[] = $row;
        if ($limit == 1) {
            if (count($rows)) {
                $rows = reset($rows); // If only one item requested, don't return array
            } else {
                $rows = null;
            }
        }
        return $rows;
    }

    function searchObject($name, $searches = [], $filters = [], $limit = 0, $sorts = []) { // Search object(s) from MySQL DB, with provided filters
        global $mysqli, $dbSchema;
        if (!is_array($filters) || !is_array($sorts) || !isset($dbSchema[$name])) {
            error("internal-error");
        }
        $query = "SELECT * FROM " . $name;
        $types = "";
        $params = [];
        $first = true;
        if (count($filters) || count($searches)) {
            $query .= " WHERE";
        }
        if (count($filters)) { // Add filters to query
            $first = true;
            foreach ($filters as $key => $filter) {
                if (!isset($dbSchema[$name]["fields"][$key])) continue; // Check field exists
                $first ? $query .= " ( " . $name . "." . $key . "=?" : $query .= " AND " . $name . "." . $key . "=?";
                $types .= $dbSchema[$name]["fields"][$key];
                $params[] = $filter;
                $first = false;
            }
            $query .= " )";
            if (count($searches)) $query .= " AND";
        }
        $first = true;
        if (count($searches)) { // Add searches to query
            foreach ($searches as $key => $search) {
                if (!isset($dbSchema[$name]["fields"][$key])) continue; // Check field exists
                $first ? $query .= " ( LOWER(" . $name . "." . $key . ") LIKE ?" : $query .= " OR LOWER(" . $name . "." . $key . ") LIKE ?";
                $types .= $dbSchema[$name]["fields"][$key];
                $params[] = $search;
                $first = false;
            }
            $query .= " )";
        }
        if (count($sorts)) {  // Add parameter-defined sort values to query (overrides table-defined sort values)
            $first = true;
            foreach ($sorts as $key => $sort) {
                if (!isset($dbSchema[$name]["fields"][$key])) continue; // Check field exists
                $first ? $query .= " ORDER BY " . $name . "." . $key . " " . $sort: $query .= ", " . $name . "." . $key . " " . $sort;
                $first = false;
            }
        } elseif (isset($dbSchema[$name]["sort"])) { // Add table-defined sort values to query
            $first = true;
            foreach ($dbSchema[$name]["sort"] as $key => $sort) {
                if (!isset($dbSchema[$name]["fields"][$key])) continue; // Check field exists
                $first ? $query .= " ORDER BY " . $name . "." . $key . " " . $sort: $query .= ", " . $name . "." . $key . " " . $sort;
                $first = false;
            }
        }
        if ($limit) {
            $query .= " LIMIT " . $limit;
        }
        $query .= ";";
        //debug([$query, $params]);
        $stmt = $mysqli->prepare($query);
        foreach ($params as $key => $param) $params[$key] = "%" . $param . "%";
        if(count($params)) $stmt->bind_param($types, ...$params);
        $stmt->execute();
        $result = $stmt->get_result();
        $rows = [];
        while($row = mysqli_fetch_array($result)) $rows[] = $row;
        if ($limit == 1) {
            if (count($rows)) {
                $rows = reset($rows); // If only one item requested, don't return array
            } else {
                $rows = null;
            }
        }
        return $rows;
    }

    function updateObject($name, $fields, $filters = []) { // Update object(s) from MySQL DB, with filters to constain scope of update (update single object by setting $filter["id], etc.)
        global $mysqli, $dbSchema;
        if (!is_array($filters) || !isset($dbSchema[$name])) {
            error("internal-error");
        }
        $query = "UPDATE " . $name . " SET ";
        $types = "";
        $params = [];
        $first = true;
        foreach ($fields as $key => $field) {
            if (!isset($dbSchema[$name]["fields"][$key])) continue; // Check field exists
            $first ? $query .= $name . "." . $key . "=?" : $query .= "," . $name . "." . $key . "=?";
            $types .= $dbSchema[$name]["fields"][$key];
            $params[] = $field;
            $first = false;
        }
        if (!count($params)) return; // Check at least one valid field exists
        if (count($filters)) { // Add filters to query
            $first = true;
            foreach ($filters as $key => $filter) {
                if (!isset($dbSchema[$name]["fields"][$key])) continue; // Check field exists
                $first ? $query .= " WHERE " . $name . "." . $key . "=?" : $query .= " AND " . $name . "." . $key . "=?";
                $types .= $dbSchema[$name]["fields"][$key];
                $params[] = $filter;
                $first = false;
            }
        }
        $query .= ";";
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param($types, ...$params);
        $stmt->execute();
        return 1;
    }

    function deleteObject($name, $filters) { // Delete object(s) from MySQL DB specified by filters (at least one filter required to prevent accidentally table deletion)
        global $mysqli, $dbSchema;
        if (!is_array($filters) || !isset($dbSchema[$name])) {
            error("internal-error");
        }
        $query = "DELETE FROM " . $name;
        $types = "";
        $first = true;
        foreach ($filters as $key => $filter) { // Add filters to query
            if (!isset($dbSchema[$name]["fields"][$key])) continue; // Check field exists
            $first ? $query .= " WHERE " . $name . "." . $key . "=?" : $query .= " AND " . $name . "." . $key . "=?";
            $types .= $dbSchema[$name]["fields"][$key];
            $params[] = $filter;
            $first = false;
        }
        if (!count($params)) return; // Check at least one valid filter exists
        $query .= ";";
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param($types, ...$params);
        $stmt->execute();
        return 1;
    }

    function objectFields($name) {
        global $dbSchema;
        if (isset($dbSchema[$name])) {
            if (isset($dbSchema[$name]["protected"])) {
                $params = [];
                foreach ($dbSchema[$name]["fields"] as $field => $value) {
                    if (!in_array($field, $dbSchema[$name]["protected"])) {
                        $params[$field] = $value;
                    }
                }
                return $params;
            }
            return $dbSchema[$name]["fields"];
        }
        return [];
    }
?>