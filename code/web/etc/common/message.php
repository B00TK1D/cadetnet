<?php
    global $messageList;
    $messageList = [
        "good" => "Success",
        "error" => "Unknown error, please try again",
        "internal-error" => "Internal error, please try again",
        "invalid-credentials" => "Invalid credentials, please try again",
        "unverified" => "Account pending verification.  Please check email for verification link.",
        "logged-out" => "Successfully logged out",
        "setup-complete" => "Account setup complete",
        "password-reset" => "Password reset sent, check email for resest link",
        "user-deleted" => "User deleted",
        "timeout" => "Session timed out, please log in again",
        "no-permissions" => "Your account does not have permissions to access this page",
        "invalid-token" => "Invalid token",
        "missing-params" => "Missing parameters",
        "account-verified" => "Account verified successfully, please log in",
        "equipment-created" => "Tag registered",
        "equipment-updated" => "Tag updated",
        "equipment-deleted" => "Tag deleted",
        "password-set" => "Password updated, please log in",
        "nonexistent" => "Requested item does not exist",
        "payment-failed" => "Your card did not process, please try again",
    ];


    function message($code) {
        global $messageList;
        if ($code) {
            $index = array_keys($messageList)[$code - 1];
            if (isset($index)) {
                $return = $messageList[$index];
            }
            if (isset($return)) {
                return $return;
            }
        }
        return "";
    }

    function code($description) {
        global $messageList;
        $result = array_search($description,array_keys($messageList));
        if ($result !== false) {
            return $result + 1;
        }
        return $result;
    }

?>