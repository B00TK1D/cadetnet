<?php
    function sanitize($string) {
        return htmlspecialchars($string);
    }

    function getHash($string) {
        return password_hash($string, PASSWORD_DEFAULT);
    }

    function checkHash($password, $hash) {
        return password_verify($password, $hash);
    }
?>