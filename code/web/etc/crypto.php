<?php
    global $crypto;
    $crypto = [
        "cred_key" => $_ENV["CRED_KEY"]
    ];


    function encrypt($plaintext, $password = null) {
        global $crypto;
        if ($password == null) {
            $password = $crypto["cred_key"];
        }
        $method = "AES-256-CBC";
        $key = hash('sha256', $password, true);
        $iv = openssl_random_pseudo_bytes(16);

        $ciphertext = openssl_encrypt($plaintext, $method, $key, OPENSSL_RAW_DATA, $iv);
        $hash = hash_hmac('sha256', $ciphertext . $iv, $key, true);

        return base64_encode($iv . $hash . $ciphertext);
    }

    function decrypt($encoded, $password = null) {
        global $crypto;
        if ($password == null) {
            $password = $crypto["cred_key"];
        }
        $method = "AES-256-CBC";
        $ivHashCiphertext = base64_decode($encoded);
        $iv = substr($ivHashCiphertext, 0, 16);
        $hash = substr($ivHashCiphertext, 16, 32);
        $ciphertext = substr($ivHashCiphertext, 48);
        $key = hash('sha256', $password, true);

        if (!hash_equals(hash_hmac('sha256', $ciphertext . $iv, $key, true), $hash)) return null;

        return openssl_decrypt($ciphertext, $method, $key, OPENSSL_RAW_DATA, $iv);
    }
?>