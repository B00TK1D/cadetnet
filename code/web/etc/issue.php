<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil(["notification"]);


    global $predictions;
    $predictions = [
        "default" => [
            "message" => "Thank you for submitting the issue report.  The automated system couldn't determine how to fix the issue, but a cadet admin has been notified and will be in contact with you soon.",
            "link" => "/user/login",
        ],
        "mac-randomization" => [
            "message" => "Ensure that MAC randomization is disabled on your device.  Instructions availabe at the button below:",
            "link" => "/help/mac",
        ],
        "account-unverified" => [
            "message" => "It appears that your account has not been verified yet. Make sure to check your spam folder for the verification email.  Also, USAFA email addresses have a tendancy to drop these emails, so try using a personal account.  Try signing up again using the button below to re-send the verification email:",
            "link" => "/public/signup",
        ],
        /*"connection-instability" => [
            "message" => "You reported a conection issue which could be resolved by adjusting wireless settings or system tuning.  An admin has been notified and will assist in improving your connection soon.  Until then you are able to view your specific devices' connection statistics by clicking the button below (it may take several seconds to load).",
            "link" => "/utils/loader?redirect=/support/troubleshoot/device%3Fm=",
        ],*/
    ];



    function notifyReport($issue) {
        $admins = readObject("admins", ["network" => $issue["network"]]);
        $admins = array_merge($admins, readObject("admins", ["tier" => config("admin-tier.global")]));
        $endpoint = [];

        foreach ($admins as $admin) {
            $endpoints["users"][] = (-$admin["id"]);
        }

        $msg = "";

        if ($issue["type"] == config("issue.type.account")) $msg = "Account issue from ";
        if ($issue["type"] == config("issue.type.setup")) $msg = "Setup issue from ";
        if ($issue["type"] == config("issue.type.connection")) $msg = "Connection issue from ";
        if ($issue["type"] == config("issue.type.other")) $msg = "Issue from ";

        $msg .= $issue["name"];

        $url = "/support/issue/view?issue=" . $issue["id"];

        notificationSend("CadetNet Issue", $msg, $url, $endpoints);
    }

    function processReport($issue) {
        global $predictions;
        $prediction = "default";

        if ($issue["type"] == config("issue.type.setup")) {
            $prediction = "mac-randomization";
        } elseif ($issue["type"] == config("issue.type.account")) {
            $user = readObject("users", ["email" => $issue["email"]], 1);
            if ($user != null && $user["status"] == config("verification.unverified")) {
                $prediction = "account-unverified";
            }
        } elseif ($issue["type"] == config("issue.type.latency") || $issue["type"] == config("issue.type.speed") || $issue["type"] == config("issue.type.dropping")) {
            $prediction = "connection-instability";
        }

        return $prediction;
    }


    function getPriority($issue) {
        $priority = 0;
        if ($issue["type"] == config("issue.type.account")) $priority += 1;
        if ($issue["type"] == config("issue.type.setup")) $priority += 5;
        if ($issue["type"] == config("issue.type.connection")) $priority += 7;
        if ($issue["type"] == config("issue.type.latency")) $priority += 6;
        if ($issue["type"] == config("issue.type.speed")) $priority += 8;
        if ($issue["type"] == config("issue.type.dropping")) $priority += 10;
        if ($issue["type"] == config("issue.type.blocked")) $priority += 9;
        if ($issue["type"] == config("issue.type.other")) $priority += 2;

        if ($issue["activity"] == config("issue.activity.browsing")) $priority += 1;
        if ($issue["activity"] == config("issue.activity.school")) $priority += 10;
        if ($issue["activity"] == config("issue.activity.streaming")) $priority += 2;
        if ($issue["activity"] == config("issue.activity.gaming")) $priority += 5;

        if ($issue["frequency"] == config("issue.frequency.constant")) $priority += 10;
        if ($issue["frequency"] == config("issue.frequncy.often")) $priority += 5;
        if ($issue["frequency"] == config("issue.frequncy.sometimes")) $priority += 3;
        if ($issue["frequency"] == config("issue.frequncy.rarely")) $priority += 2;
        if ($issue["frequency"] == config("issue.frequncy.once")) $priority += 1;

        return $priority;

    }


    function getPrediction($key) {
        global $predictions;
        if (isset($predictions[$key])) return $predictions[$key];
        return $predictions["default"];
    }
?>