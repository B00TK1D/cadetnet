<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil(["composer"]);

    function mailTemplate($template, $data = []) {
        if (!configValue($template, "sendgrid.templates") || !isset($data["email"])) return;
        $data = array_merge(config("sendgrid.defaults"), $data);
        $data = assocArray($data);
        $mail = new \SendGrid\Mail\Mail();
        $mail->setTemplateId(configValue($template, "sendgrid.templates"));
        $mail->setFrom($data["from_email"], $data["from_name"]);
        $mail->addTo($data["email"], $data["name"]);
        $mail->addDynamicTemplateDatas($data);
        
        return send_mail($mail);
    }

    function send_mail($email) {
        global $config;
        $send = new \SendGrid(config("sendgrid.api_key"));
        try {
            $send->send($email);
            return 1;
        } catch (Exception $e) {
            return 0;
        }
    }

?>