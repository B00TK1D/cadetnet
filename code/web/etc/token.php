<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil(["db", "error"]);

    function generateToken($purpose) {
        $token = [];
        $token["string"] = randomString(config("token.length"));
        $time = new DateTime();
        $time->modify(config("token.expiration.verification"));
        $token["expiration"] = date("Y-m-d H:i:s", $time->getTimestamp());
        $token["purpose"] = configValue($purpose, "token.purpose");
        return $token;
    }

    function randomString($len) {
        return bin2hex(openssl_random_pseudo_bytes($len));
    }

    function checkToken($token, $purposes, $displayError = false, $expires = false) {
        arrayify($purposes);
        $check = readObject("tokens", ["string" => $token], 1);

        if ($check == null) {
            error("invalid-token", $displayError);
        }

        if ($expires) {
            $time = new DateTime($check["expiration"]);
            $now = new DateTime();
            if ($now > $time) {
                error("invalid-token", $displayError);
            }
        }

        foreach($purposes as $purpose) {
            if ($check["purpose"] == configValue($purpose, "token.purpose")) {
                return $check;
            }
        }
        error("invalid-token", $displayError);
    }
?>