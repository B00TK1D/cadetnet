<?php

    function parseXML($xml, $append = []) {
        $output = [];

        $tables = [];

        if(preg_match_all('/<t tn="(.*?)" nc=".*?">(.*?)<\/t>/', $xml, $matches)) {
            $i = 0;
            foreach ($matches[0] as $match) {
                $name = $matches[1][$i];
                $content = $matches[2][$i];
                $table = ["name" => $name, "content" => $content];
                $tables[$name] = $table;
                $i++;
            }
        }

        if(preg_match_all('/<data name="(.*?)\:?" pn=".*?">(.*?)<\/data>/', $xml, $matches)) {
            $i = 0;
            foreach ($matches[0] as $match) {
                $name = $matches[1][$i];
                $content = $matches[2][$i];
                $output["data"][$name] = $content;
                $i++;
            }
            foreach ($append as $id => $value) {
                $output["data"][$id] = $value;
            }
        }


        foreach ($tables as $table) {
            $key = [];

            if(preg_match_all("/<th>(.*?)<\/th>/", $table["content"], $matches)) {
                foreach ($matches[1] as $match) {
                    if(preg_match_all("/<h>(.*?)<\/h>/", $match, $sections)) {
                        foreach ($sections[1] as $section) {
                            $key[] = $section;
                        }
                    }
                }
            }



            if(preg_match_all("/<r>.*?<\/r>/", $table["content"], $matches)) {
                foreach ($matches[0] as $match) {
                    if(preg_match_all("/<c>(.*?)<\/c>/", $match, $sections)) {
                        $entry = [];
                        $i = 0;
                        foreach ($sections[1] as $section) {
                            $entry[$key[$i]] = $section;
                            $i++;
                        }
                        foreach ($append as $id => $value) {
                            $entry[$id] = $value;
                        }
                        $output[$table["name"]][] = $entry;
                    }
                }
            }
        }

        return $output;
    }


    function mergeParsed($sources, $append = []) {
        $merged = [];
        foreach ($sources as $source) {
            $merged = array_merge_recursive($merged, $source);
        }
        return $merged;
    }


    function listMergedString($data) {
        $output = "";
        if (is_array($data)) {
            foreach($data as $id => $value) {
                $output .= strval($value);
                if ($id != count($data) - 1) {
                    $output .= ", ";
                }
            }
        } else {
        $output = $data;
        }
        return $output;
    }


    function expandMerged($data) {
        $output = $data;
        if (is_array($data) && is_array(reset($data))) {
            $reset = reset($data);
            if (is_array(reset($reset))) {
                $output = [];
                foreach($data as $inner) {
                    foreach ($inner as $id => $value) {
                        $output[$id] = $value;
                    }
                }
            }
        }
        return $output;
    }


    function getClosest($data, $searches, $matches) {
        $closest = null;
        $best = 0;
        foreach ($data as $item) {
            $score = 0;
            $valid = true;
            foreach ($matches as $key => $match) {
                if ($item[$key] != $match) {
                    $valid = false;
                    break;
                }
            }
            if ($valid) {
                foreach ($searches as $key => $search) {
                    $score += abs(intval($item[$key]) - intval($search));
                }
                if ($closest === null || $score < $best) {
                    $closest = $item;
                    $best = $score;
                }
            }
        }
        return $closest;
    }

    
    function getUnique($data, $key) {
        $unique = [];
        foreach ($data as $item) {
            $valid = true;
            foreach ($unique as $u) {
                if ($item[$key] == $u) {
                    $valid = false;
                    break;
                }
            }
            if ($valid) {
                $unique[] = $item[$key];
            }
        }
        return $unique;
    }


    function sortBy($data, $key) {
        usort($data, function ($a, $b) use ($key) {
            return $a[$key] <=> $b[$key];
        });
        return $data;
    }
?>