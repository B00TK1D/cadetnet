<?php function macHelpBlock() { ?>

    <div class="info-container">
      <div>
        <h1 class="heading-18">MAC ADDRESS HELP</h1>
      </div>
      <div class="div-block-65">
        <div>
          <div class="text-block-18">The MAC address you are currently using on your device is a randomized MAC address, which will not work with CadetNet. Please follow instructions below to correct this:</div>
        </div>
        <div>
          <div id="ios-help" class="text-block-16"> <br /><strong>iPhone / iPad</strong><br /><br /><strong>1)</strong> Open the Settings app, then tap Wi-Fi<br /><br /><strong>2)</strong> Tap the information button next to the CadetNet network<br /><br /><strong>3)</strong> Tap to turn Private Address off<br /><br /><strong>4)</strong> Accept the privacy notice that may pop up<br /><br /><strong>5)</strong> Disconnect from CadetNet, reconnect, and add your new MAC address<br /><br />More info: <a href="https://support.apple.com/en-us/HT211227" target="_blank">Apple Support Article</a></div>
          <div id="watch-help" class="text-block-16"> <br /><strong>Apple Watch<br />‍</strong><br /><strong>1)</strong> Open the <strong>Settings</strong> app, then tap <strong>Wi-Fi</strong><br /><br /><strong>2)</strong> Tap <strong>CadetNet</strong><br />‍<br /><strong>3) </strong>Tap to turn <strong>Private Address</strong> off<br /><br /><strong>4)</strong> Disconnect from CadetNet, reconnect, and add your new MAC address<br /><br />More info: <a href="https://support.apple.com/en-us/HT211227" target="_blank">Apple Support Article</a></div>
          <div id="android-help" class="text-block-16"> <br /><strong>Android Phones<br />‍</strong><br /><strong>1)</strong> Open the Settings, then tap <strong>Wi-Fi</strong><br />‍<br /><strong>2)</strong> Tap the gear icon associated with CadetNet<br />‍<br /><strong>3)</strong> Tap <strong>Advanced</strong>, then <strong>Privacy</strong> (or Address Type on some phones)<br />‍<br /><strong>4)</strong> Tap <strong>Use phone MAC</strong> (or disable private address)<br />‍<br /><strong>5)</strong> Disconnect from CadetNet, reconnect, and add your new MAC address<br /><br />More info: <a href="https://support.plume.com/hc/en-us/articles/360052070714-How-do-I-disable-random-Wi-Fi-MAC-address-on-Android-10-https://support.plume.com/hc/en-us/articles/360052070714-How-do-I-disable-random-Wi-Fi-MAC-address-on-Android-10-" target="_blank">Android Support Article</a></div>
          <div id="windows-help" class="text-block-16"><strong><br />Windows 10<br />‍</strong> <br />‍<strong>1)</strong> Select the <strong>Start </strong> button, then select <strong>Settings </strong> &gt; <strong>Network &amp; Internet</strong> &gt; <strong>Wi-Fi </strong> &gt; <strong>Manage known networks</strong><br />‍<br /><strong>2)</strong> Choose <strong>CadetNet</strong>, then select <strong>Properties</strong> and choose <strong>Disable</strong> under <strong>Use random hardware addresses for this network</strong>‍<br />‍<br /><strong>3)</strong> Disconnect from CadetNet, reconnect, and add your new MAC address<br /><br />More info: <a href="https://support.microsoft.com/en-us/windows/how-to-use-random-hardware-addresses-ac58de34-35fc-31ff-c650-823fc48eb1bc" target="_blank">Windows Support Article</a></div>
        </div>
      </div>
      <div class="div-block-68">
        <div>
          <div class="div-block-66"><a onclick="window.history.back();" class="cn-button apply-edit-user w-button">BACK</a></div>
        </div>
      </div>
    </div>

<?php } ?>