<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil("proxy");

  
    if(isset($_SERVER["HTTP_CADETNET_TARGET_NETWORK"])) {
        arubaEntry($_SERVER["HTTP_CADETNET_TARGET_NETWORK"]);
    } else {
        frontendEntry();
    }

    function arubaEntry($subdomain) {
        $network = readObject("networks", ["id" => $subdomain], 1);
        if ($network == null) error();
        proxyPrint("https://" . $network["ip"] . $_SERVER["REQUEST_URI"], 4343);
    }


    function frontendEntry() {
        $mac = param(["mac", false]);
        $used = param(["used", false], ["sticky" => true]);
        if ($mac) {
            setParam("used", true);
            setParam("browserMac", $mac);
            setParam("browserAdded", false);
            redirect("/user/login");
        } else {
            if ($used) {
                redirect("/user/login");
            } else {
                if (config($_SERVER['REQUEST_URI'], "redirects")) {
                    redirect(config($_SERVER['REQUEST_URI'], "redirects"));
                }
                redirect("/public/info");
            }
        }
    }
 ?>