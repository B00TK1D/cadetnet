<?php function infoBlock() { ?>

<div class="info-container">
  <div>
    <h1 class="heading-18">INFO</h1>
  </div>
  <div class="div-block-65">
    <div>
      <div class="text-block-18">What is CadetNet?</div>
      <div class="text-block-16"> <br />- CadetNet is a wing-wide network that combines all squadron WiFi networks, allowing you to pay using one account and get access everywhere that has squadron WiFi.<br />‍</div>
    </div>
    <div>
      <div class="text-block-18">Who runs CadetNet?</div>
      <div class="text-block-16"> <br />- CadetNet is run entirely by cadets, for cadets. It is not an official government network, and never will be. It is entirely non-profit; all funds go to cover expenses and upgrade equipment.<br />‍</div>
    </div>
    <div>
      <div class="text-block-18">Is it secure?</div>
      <div class="text-block-16"> <br />- Yes. It uses industry-standard encryption techniques, and all payment details are routed through Stripe, a commercial payment processor.<br />‍</div>
    </div>
    <div>
      <div class="text-block-18">What are the rules?</div>
      <div class="text-block-16"> <br />- Don&#x27;t share your account with anybody. Please. If you do, you&#x27;re robbing other cadets. Other than that, do whatever you want; we will never block sites (except ads) or track you.</div>
    </div>
    <div class="div-block-66"><a href="/public/more/info" class="cn-button _2 w-button">MORE INFO</a></div>
  </div>
  <div class="div-block-68">
    <div>
      <div class="div-block-66"><a href="/public/signup" class="cn-button apply-edit-user w-button">SIGNUP</a></div>
    </div>
  </div>
</div>

<?php

}


function smallInfoBlock() {
  
?>

<div class="info-container">
<div>
<h1 class="heading-18">INFO</h1>
</div>
<div class="div-block-65">
<div>
  <div class="text-block-18">What is CadetNet?</div>
  <div class="text-block-16"> <br />- CadetNet provides a central payment and authentication mechanism for WiFi networks.<br />‍</div>
</div>
<div>
  <div class="text-block-18">Is it secure?</div>
  <div class="text-block-16"> <br />- Yes. It uses industry-standard encryption techniques.<br />‍</div>
</div>
<div>
  <div class="text-block-18">What are the rules?</div>
  <div class="text-block-16"> <br />- Don&#x27;t share your account with anybody. Please. If you do, you&#x27;re robbing other cadets. Other than that, do whatever you want; we will never block sites (except ads) or track you.</div>
</div>
</div>
<div class="div-block-68">
<div>
  <div class="div-block-66"><a href="/public/signup" class="cn-button apply-edit-user w-button">SIGNUP</a></div>
</div>
</div>
</div>

<?php

}


function termsBlock() {

?>

<div class="div-block-61 _2">
  <div class="div-block-62 _2"></div>
  <div class="div-block-60">
    <h1 class="heading-18 _2">TERMS OF SERVICE</h1>
  </div>
  <div class="w-container">
    <div class="tos w-richtext">
      <h2>Summary</h2>
      <ul role="list">
        <li>We won&#x27;t track you.</li>
        <li>We won&#x27;t throttle you.</li>
        <li>We won&#x27;t limit how many devices you can connect.</li>
        <li>We don&#x27;t offer any performance guarantees.</li>
        <li>Don&#x27;t share your account with anyone else.</li>
        <li>You get access anywhere CadetNet is installed.</li>
        <li>You can do anything (legal) that you want on CadetNet.</li>
      </ul>
      <p>‍</p>
      <p>‍</p>
      <h2>Legal Terms of Service</h2>
      <p>‍</p>
      <p>We (Operators of CadetNet) are offering this WiFi wireless Internet service (the “Service”) according to this CadetNet Terms of Service (the “Policy”) as a non-public service to residents of the U.S. Air Force Academy Sijan and Vandenburg dormitories and other authorized users, for the duration of their stays and visits.  All users of this Service agree to the terms of this Policy when connecting to the network. We have no control over information obtained through the Internet and cannot be held responsible for its content or accuracy. Use of the service is subject to the user’s own risk.We reserve the right to remove, block, filter, or restrict by any other means any materials that, in our sole discretion, may be illegal, may subject us to liability, or may violate this Policy. We may cooperate with legal authorities and/or third parties in the investigation of any suspected or alleged crime or civil wrongs. Violations of this Policy may result in the suspension or termination of access to the Service or other resources, or other actions as detailed below.<br /> <br />Responsibilities of Service Users:<br />Users are responsible for ensuring they are running up-to-date anti-virus software on their wireless devices. Users must be aware that, as they connect their devices to the Internet through the Service, they expose their devices to: worms, viruses, Trojan horses, denial-of-service attacks, intrusions, packet-sniffing, and other abuses by third-parties.  Users must respect all copyrights. Downloading or sharing copyrighted materials is strictly prohibited.  The running of programs, services, systems, processes, or servers by a single user or group of users that may substantially degrade network performance or accessibility will not be allowed. Electronic chain letters and mail bombs are prohibited. Accessing another person&#x27;s computer, computer account, files, or data without permission is prohibited. Attempting to circumvent or subvert system or network security measures is prohibited. Creating or running programs that are designed to identify security loopholes, to decrypt intentionally secured data, or to gain unauthorized access to any system is prohibited. Using any means to decode or otherwise obtain restricted passwords or access control information is prohibited. Forging the identity of a user or machine in an electronic communication is prohibited. Saturating network or computer resources to the exclusion of another&#x27;s use, for example, by overloading the network with traffic such as emails or legitimate (file backup or archive) or malicious (denial of service attack) activity, is prohibited.  Users should adopt appropriate security measures when using the Service. Users are responsible for the security of their own devices.<br /> <br />Limitations of Wireless Network Access: <br />We are not liable for any damage, undesired resource usage, or detrimental effects that may occur to a user&#x27;s device and/or software while the user’s device is attached to the Service. The user is responsible for any actions taken from his or her device, whether intentional or unintentional, that damage or otherwise affect other devices or users of the Service. The user hereby releases the Service and its operators from liability for any loss, damage, security infringement, or injury which the user may sustain as a result of being allowed access to the Service. The user agrees to be solely responsible for any such loss, infringement, damage, or injury. <br /> <br />Terms of Service: <br />By connecting to the Service, the user agrees to comply with and to be legally bound by the terms of this Policy. The user also agrees to complete proper payment, defined as payment per owner of device(s), prior to the usage of the service. If this Policy or any terms of the Service are unacceptable or become unacceptable to the user, the user&#x27;s only right shall be to terminate his or her use of the Service. <br /> <br />Lawful Use: <br />The Service may only be used for lawful purposes and in a manner which we believe to be consistent with the rights of other users. The Service shall not be used in a manner which would violate any law or infringe any copyright, trademark, trade secret, right of publicity, privacy right, or any other right of any person or entity. The Service shall not be used for the purpose of accessing, transmitting, or storing material which is considered obscene, libelous or defamatory. Illegal acts may subject users to prosecution by local, state, federal, or international authorities.<br /> <br />The user specifically agrees to the following conditions: <br />The user agrees to be legally considered a member of CadetNet LLC for the duration of the user's use of the Service, and accepts all legal obligations and responsibilities associated with this membership.<br />The user agrees to the use of automated and operated monitoring systems for the purose of improving quality of service and offering additional services which may be of benifit to the user.  Parts of these monitoring systems may be controlled by third parties, and data such as connection statistics may be exported to external platforms for analysis.<br />The user will use the Service only as permitted by applicable local, state, federal, and International laws. The user will refrain from any actions that we consider to be negligent or malicious. The user will not send email containing viruses or other malicious or damaging software. The user will run appropriate anti-virus software to remove such damaging software from his or her computer.  The user will not access web sites which contain material that is grossly offensive to us, including clear expressions of bigotry, racism, or hatred. The user will not access web sites which contain material that defames, abuses, or threatens others. <br /> <br />Changes to Service: <br />We reserve the right to change the Service offered, the features of the Service offered, the terms of this Policy, or its system without notice to the user. The terms of this Policy will be listed at all times in this document and available for view at any time.<br /> <br />By completing a connection to this service, you acknowledge that you understand and agree to this Policy.</p>
      <p>‍</p>
      <p>‍</p>
    </div>
    <div class="div-block-78"><a class="cn-button w-button" onclick="window.history.back();">BACK</a></div>
  </div>
</div>

<?php

}


function signupBlock($coupon, $networks) {
  
?>

<div class="div-block-61">
  <div class="div-block-62"></div>
  <div class="div-block-60">
    <h1 class="heading-18">CADETNET SIGNUP</h1>
    <div class="w-form">
      <form action="/user/onboard" method="post">
        <label for="name" class="cn-label">Name</label>
        <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="" required="" />
        <label for="squadron" class="cn-label">Squadron</label>
        <select name="network" class="cn-field _2 w-select">
            <option value="0" style="display:none">Select Squadron</option>
            <?php foreach ($networks as $network) { ?>
              <option value="<?php print($network['id']); ?>"><?php print($network['name']); ?></option>
            <?php } ?>
          </select>
        <label for="email" class="cn-label">Email Address</label>
        <input type="text" class="cn-field w-input" maxlength="256" name="email" placeholder="" required="" />
        <label for="password" class="cn-label">Password</label>
        <input type="password" class="cn-field w-input" maxlength="256" name="password" placeholder="" id="password" required="" />
        <label for="confirm-password" class="cn-label">Confirm Password</label>
        <input type="password" class="cn-field w-input" maxlength="256" name="confirm-password" placeholder="" id="confirm-password" required="" />
        <label class="w-checkbox"><input type="checkbox" id="checkbox" name="checkbox" data-name="Checkbox" required="" class="w-checkbox-input" />
        <span class="checkbox-label-3 w-form-label">I agree to the CadetNet <a href="/public/terms" class="link-4">Terms of Service</a></span></label>
        <input type="text" name="coupon" value="<?php print($coupon); ?>" hidden=""/>
        <input type="submit" value="Signup" data-wait="Please wait..." class="submit-button-2 w-button" />
      </form>
    </div>
    <div class="text-block-18" id="error"></div>
  </div>
  <div class="footer">
    <div class="div-block-79">
      <div class="text-block-19">Already have an account?</div><a href="/user/login" class="submit-button-2 _2 w-button">Login</a>
    </div>
  </div>
</div>
<script src="/js/jquery.js"></script>
<script>
    $("[type=submit]").each(function(index) {
      this.addEventListener("click", function(event) {
        if (document.getElementById('password').value != document.getElementById('confirm-password').value) {
            document.getElementById('error').innerHTML = "Error: Password entries do not match";
            event.preventDefault();
        }
      });
    });
</script>

<?php } ?>