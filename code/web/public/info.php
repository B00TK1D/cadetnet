<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll("elements");

    head("CadetNet Info", ["SIGNUP" => "/public/signup", "LOGIN" => "/user/login", "ADMIN" => "/admin/login"]);
    smallInfoBlock();
    foot();
?>