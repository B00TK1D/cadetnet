<?php function moreInfoBlock() { ?>

    <div class="info-container">
      <div class="div-block-63"></div>
      <div>
        <h1 class="heading-18">MORE INFO</h1>
      </div>
      <div class="div-block-65">
        <div>
          <div class="text-block-18">How much of my data is visible to who?</div>
          <div class="text-block-16"> <br />- CadetNet is committed to privacy. Government access to data will be strictly limited to legal investigations, and will only be provided when legally required (i.e. if a cadet is being investigated for hacking foreign governments, CadetNet may cooperate with authorities. If an AOC wants to know where a cadet was one night, CadetNet will not cooperate - even if it could answer that question, which it can&#x27;t.)<br /><br /> - Unitron-level administrators are able to see the names, email addresses, device names, and device MAC addresses for all users within their squadron. This information is purely for troubleshooting purposes, and no other information is visible.<br /><br /> - Wing-level administrators (which are highly restricted) are able to see the same information as squadron-level administrators, but for every user on the system. Once again, this is purely for troublehsooting.<br />‍</div>
        </div>
        <div>
          <div class="text-block-18">Where does the money go?</div>
          <div class="text-block-16"> <br />- Payments are automatically handled by Stripe, and all income goes to a central Stripe account. Out of each payment, Stripe charges a $0.45 fee, CadetNet receives $0.10 to cover operating costs (domain name, servers, etc.), and the remaining $4.45 is added to the squadron&#x27;s balance. Each squadron is issued a credit card which they can use to spend their balance. This card is used to pay the monthly fee of $110 to Comcast, and to pay for system upgrades. At no time does anybody make a profit off of CadetNet.<br /><br /> - Even if you are signing up with a squadron-supplied link that gives you free credit, you still need to input your billing information. This is required so that once the credit period expires, CadetNet can begin billing you. No charges will be created on this card until the free period expires, and you are free to cancel your account at any time before then.<br />‍</div>
        </div>
        <div>
          <div class="text-block-18">What are the speeds?</div>
          <div class="text-block-16"> <br /> - CadetNet does not throttle bandwidth at all, and every squadron has a 1Gbps connection from Comcast, so your speeds are entirely dependant on the wireless connection between you and the nearest access point. Normally, rooms with an access point directly outside will receive 80Mbps - 200Mbps, and rooms further away will receive 30Mbps - 100Mbps. Older wireless adapters may also affect the maximum speed received. Ping varies primarily by time of day (total network usage tends to spike around 1800 - 2100), but is usually 10ms - 20ms.<br />‍</div>
        </div>
        <div>
          <div class="text-block-18">How many devices can I add?</div>
          <div class="text-block-16"> <br /> - You can add as many devices as you want.  Just make sure that you only add your own devices; sharing your account with others is not allowed.  You can add devices through the CadetNet dashboard after logging in.  (Hint: if you connect to CadetNet on a new device and sign into your account, it will automatically detect your MAC address and auto-fill that section.)<br />‍</div>
        </div>
        <div>
          <div class="text-block-18">What if I don&#x27;t want to pay anymore?</div>
          <div class="text-block-16"> <br /> - You are free to delete your account at any time, which will remove your billing information and end payments. Your devices will immediately lose access to CadetNet; we do not offer refunds on the remainder of the month.<br />‍</div>
        </div>
        <div>
          <div class="text-block-18">Why won&#x27;t my gaming console connect?</div>
          <div class="text-block-16"> <br /> - Gaming console plus WiFi equals pain. The internal wireless adapters on most major gaming consoles are notoriously terrible, particularly when you put concrete and steel walls between them and the access points. CadetNet is continuously upgrading its infrastructure, and is also attempting to install hardline connections to every room. Until this happens, the best option is to make sure your gaming console is as close to the hallway as possible, and ensure it has a direct line-of-sight to the door.<br />‍</div>
        </div>
        <div>
          <div class="text-block-18">Why isn&#x27;t CadetNet perfect?</div>
          <div class="text-block-16"> <br /> - WiFi is an incredible feat of spewing electrons into the air and then catching them as they come back. USAFA created an engineering miracle by designing walls that allow sound to carry through walls perfectly at night, but block almost every single electron with a literal Faraday cage around each room. That being said, CadetNet usually works pretty well. It at least works significantly better than other solutions, at a fraction of the cost, and it is committed to constantly becoming better. The goal is not perfection, but rather constant improvement.<br />‍</div>
        </div>
        <div class="div-block-66"><a href="/public/info" class="cn-button _2 w-button">BACK</a></div>
      </div>
      <div class="div-block-68">
        <div>
          <div class="div-block-66"><a href="/public/signup" class="cn-button apply-edit-user w-button">SIGNUP</a></div>
        </div>
      </div>
    </div>

<?php } ?>