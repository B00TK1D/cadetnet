<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll("elements");

    head("CadetNet Signup", ["INFO" => "/public/info", "LOGIN" => "/user/login", "ADMIN" => "/admin/login"]);
    signupBlock(param(["c", ""]), readObject("networks"));
    foot();
?>