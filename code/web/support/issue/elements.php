<?php function listIssueBlock($issues) { ?>

    <div class="div-block-63"></div>
    <div class="div-block-64">
      <div>
        <h1 class="heading-19">ISSUES</h1>


        <?php foreach ($issues as $issue) { ?>
          <div class="div-block-65">
            <div class="text-block-16"><?php print($issue['name']); ?></div>
            <div class="div-block-66 seperate">
              <div class="cred-holder">
                <div class="text-block-16 small"><?php print($issue['time']); ?><br /></div>
                <div class="text-block-16">Type: <?php print($issue['type-name']); ?></div>
                <div class="text-block-16">Network: <?php print($issue['network-name']); ?></div>
              </div>
            </div>
            <div class="div-block-66 action-container">
              <a href="/support/issue/view?issue=<?php print($issue['id']); ?>" class="button-5 _2 reveal-button w-button"></a>
              <a class="button-5 _2 delete-button w-button"></a>
              <div class="delete-confirmation delete-confirm">
                <div class="div-block-65">
                  <div class="text-block-16-copy">Resolve issue with <?php print($issue['name']); ?>?</div>
                  <div class="div-block-66">
                    <a class="cn-button _2 cancel-delete w-button">CANCEL</a>
                    <form action="/api/issue/resolve" method="post" target="transFrame">
                      <input type="text" name="issue" value="<?php echo $issue['id'];?>" hidden="">
                      <input type="submit" class="cn-button apply-delete w-button" value="APPLY">
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>


        <?php } ?>

      </div>

      <script src="/js/jquery.js"></script>
      <script>

      // Edit device
      $(".edit-device-button").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "auto";
          });
        });

        $(".cancel-edit-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
          });
        });

        $(".apply-edit-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
          });
        });

        // New device
        $(".new-device-button").each(function(index) {
          this.addEventListener("click", function() {
            this.style.height = "0px";
            this.parentNode.getElementsByClassName("new-device")[0].style.height = "auto";
          });
        });

        $(".cancel-new-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

          });
        });

        $(".apply-new-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

          });
        });


        $(".delete-button").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "flex";
          });
        });

        $(".cancel-delete").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
          });
        });

        $(".apply-delete").each(function(index) {
          this.addEventListener("click", function() {
            //this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
          });
        });


        $('[type=submit]').each(function(index) {
            this.addEventListener("click", function() {
                setTimeout(function () {location.reload()}, 1000);
          });
        });


    </script>

<?php }  function viewIssueBlock($issue) { ?>

    <div class="div-block-63"></div>
    <div class="div-block-64">
      <div>
        <h1 class="heading-19">ISSUE</h1>
        <div class="div-block-65">
          <div class="text-block-16"><?php print($issue['name']); ?></div>
          <div class="div-block-66 seperate">
            <div class="cred-holder">
            <div class="text-block-16">Submitted <?php print($issue['time']); ?></div>
            <div class="text-block-16">Email: <?php print($issue['email']); ?></div>
            <div class="text-block-16">Network: <?php print($issue['network-name']); ?></div>
              <div class="text-block-16">Issue type: <?php print($issue['type-name']); ?></div>
              <?php if ($issue["device"]) { ?>
                <div class="text-block-16">Device: <?php print($issue['device-name']); ?></div>
              <?php } ?>
              <?php if ($issue["frequency"] != -1) { ?>
                <div class="text-block-16">Frequency: <?php print($issue['frequency-name']); ?></div>
              <?php } ?>
              <?php if ($issue["activity"] != -1) { ?>
                <div class="text-block-16">Activity: <?php print($issue['activity-name']); ?></div>
              <?php } ?>
              <?php if ($issue["notes"]) { ?><div class="text-block-16"><br />Notes:<br /><?php print($issue["notes"]) ?><br /></div><?php } ?>
            </div>
          </div>
          <div class="div-block-66 action-container">
              <?php if ($issue["device-db"] != null) { ?><a href="/utils/loader?redirect=/support/troubleshoot/device%3Fm=<?php print($issue['device-db']['mac']); ?>" class="button-5 _4 troubleshoot-button w-button"></a> <?php } ?>
              <a class="button-5 _2 delete-button w-button"></a>
              <div class="delete-confirmation delete-confirm">
                <div class="div-block-65">
                  <div class="text-block-16-copy">Resolve issue with <?php print($issue['name']); ?>?</div>
                  <div class="div-block-66">
                    <a class="cn-button _2 cancel-delete w-button">CANCEL</a>
                    <form action="/api/issue/resolve" method="post">
                      <input type="text" name="issue" value="<?php echo $issue['id'];?>" hidden="">
                      <input type="submit" class="cn-button apply-delete w-button" value="APPLY">
                    </form>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
    <div class="div-block-68">
      <div>
        <div class="div-block-66"><a href="/support/issue/list" class="cn-button apply-edit-user w-button">BACK</a></div>
      </div>
    </div>

    <script src="/js/jquery.js"></script>
      <script>

      // Edit device
      $(".edit-device-button").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "auto";
          });
        });

        $(".cancel-edit-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
          });
        });

        $(".apply-edit-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
          });
        });

        // New device
        $(".new-device-button").each(function(index) {
          this.addEventListener("click", function() {
            this.style.height = "0px";
            this.parentNode.getElementsByClassName("new-device")[0].style.height = "auto";
          });
        });

        $(".cancel-new-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

          });
        });

        $(".apply-new-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

          });
        });


        $(".delete-button").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "flex";
          });
        });

        $(".cancel-delete").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
          });
        });

        $(".apply-delete").each(function(index) {
          this.addEventListener("click", function() {
            //this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
          });
        });


        $('[type=submit]').each(function(index) {
            this.addEventListener("click", function() {
                setTimeout(function () {location.reload()}, 1000);
          });
        });


    </script>

<?php } ?>