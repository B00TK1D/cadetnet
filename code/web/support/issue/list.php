<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["elements","functs"]);    

    $admin = authenticate("admins")["admin"];

    $issues = [];

    if ($admin["tier"] == config("admin-tier.global")) {
        $issues = readObject("issues");
    } else {
        $issues = readObject("issues", ["network" => $admin["network"]]);
    }

    foreach ($issues as $key => $issue) {
        if ($issue["status"] == config("issue.status.resolved")) {
            unset($issues[$key]);
            continue;
        }

        if ($issue["type"] == config("issue.type.account")) $issues[$key]["type-name"] = "Account";
        elseif ($issue["type"] == config("issue.type.setup")) $issues[$key]["type-name"] = "Setup";
        elseif ($issue["type"] == config("issue.type.connection")) $issues[$key]["type-name"] = "Connection";
        elseif ($issue["type"] == config("issue.type.latency")) $issues[$key]["type-name"] = "Latency";
        elseif ($issue["type"] == config("issue.type.speed")) $issues[$key]["type-name"] = "Speed";
        elseif ($issue["type"] == config("issue.type.dropping")) $issues[$key]["type-name"] = "Connection dropping";
        elseif ($issue["type"] == config("issue.type.blocked")) $issues[$key]["type-name"] = "Site blocked";
        elseif ($issue["type"] == config("issue.type.other")) $issues[$key]["type-name"] = "Other";
        else $issues[$key]["type-name"] = "N/A";

        if ($issue["activity"] == config("issue.activity.browsing")) $issues[$key]["activity-name"] = "Internet Browsing";
        elseif ($issue["activity"] == config("issue.activity.school")) $issues[$key]["activity-name"] = "School";
        elseif ($issue["activity"] == config("issue.activity.streaming")) $issues[$key]["activity-name"] = "Streaming";
        elseif ($issue["activity"] == config("issue.activity.gaming")) $issues[$key]["activity-name"] = "Gaming";
        elseif ($issue["activity"] == config("issue.activity.other")) $issues[$key]["activity-name"] = "Other";
        else $issues[$key]["activity-name"] = "N/A";

        if ($issue["frequency"] == config("issue.frequency.constant")) $issues[$key]["frequency-name"] = "Constant (Every 5-10 minutes)";
        elseif ($issue["frequency"] == config("issue.frequency.often")) $issues[$key]["frequency-name"] = "Often (every 10-30 minutes)";
        elseif ($issue["frequency"] == config("issue.frequency.sometimes")) $issues[$key]["frequency-name"] = "Sometimes (every 1-3 hours)";
        elseif ($issue["frequency"] == config("issue.frequency.rarely")) $issues[$key]["frequency-name"] = "Rarely (every few days)";
        elseif ($issue["frequency"] == config("issue.frequency.once")) $issues[$key]["frequency-name"] = "Once";
        else $issues[$key]["frequency-name"] = "N/A";

        $issues[$key]["device-name"] = "";
        $issues[$key]["device-db"] = readObject("devices", ["id" => $issue["device"]], 1);
        if ($issues[$key]["device-db"] != null) {
            $issues[$key]["device-name"] = $issues[$key]["device-db"]["name"];
        }

        $issues[$key]["network-name"] = "";
        $issues[$key]["network-db"] = readObject("networks", ["id" => $issue["network"]], 1);
        if ($issues[$key]["network-db"] != null) {
            $issues[$key]["network-name"] = $issues[$key]["network-db"]["name"];
        }

        $issues[$key]["time"] = date('Hi dMy', strtotime($issue["created"]) - 21600);
    }

    head("CadetNet Issue");
    listIssueBlock($issues);
    foot();
?>