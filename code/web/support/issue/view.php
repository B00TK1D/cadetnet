<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["elements","functs"]);    

    $id = param("issue", ["sticky" => true]);
    $issue = readObject("issues", ["id" => $id], 1);
    if ($issue == null) error();

    if ($issue["user"] != -1) {
        authenticate("owners", ["user" => $issue["user"]]);
    } else {
        authenticate("owners", ["network" => $issue["network"]]);
    }

    if ($issue["type"] == config("issue.type.account")) $issue["type-name"] = "Account";
    elseif ($issue["type"] == config("issue.type.setup")) $issue["type-name"] = "Setup";
    elseif ($issue["type"] == config("issue.type.connection")) $issue["type-name"] = "Connection";
    elseif ($issue["type"] == config("issue.type.latency")) $issue["type-name"] = "Latency";
    elseif ($issue["type"] == config("issue.type.speed")) $issue["type-name"] = "Speed";
    elseif ($issue["type"] == config("issue.type.dropping")) $issue["type-name"] = "Connection dropping";
    elseif ($issue["type"] == config("issue.type.blocked")) $issue["type-name"] = "Site blocked";
    elseif ($issue["type"] == config("issue.type.other")) $issue["type-name"] = "Other";
    else $issue["type-name"] = "N/A";

    if ($issue["activity"] == config("issue.activity.browsing")) $issue["activity-name"] = "Internet Browsing";
    elseif ($issue["activity"] == config("issue.activity.school")) $issue["activity-name"] = "School";
    elseif ($issue["activity"] == config("issue.activity.streaming")) $issue["activity-name"] = "Streaming";
    elseif ($issue["activity"] == config("issue.activity.gaming")) $issue["activity-name"] = "Gaming";
    elseif ($issue["activity"] == config("issue.activity.other")) $issue["activity-name"] = "Other";
    else $issue["activity-name"] = "N/A";

    if ($issue["frequency"] == config("issue.frequency.constant")) $issue["frequency-name"] = "Constant (Every 5-10 minutes)";
    elseif ($issue["frequency"] == config("issue.frequency.often")) $issue["frequency-name"] = "Often (every 10-30 minutes)";
    elseif ($issue["frequency"] == config("issue.frequency.sometimes")) $issue["frequency-name"] = "Sometimes (every 1-3 hours)";
    elseif ($issue["frequency"] == config("issue.frequency.rarely")) $issue["frequency-name"] = "Rarely (every few days)";
    elseif ($issue["frequency"] == config("issue.frequency.once")) $issue["frequency-name"] = "Once";
    else $issue["frequency-name"] = "N/A";

    $issue["device-name"] = "";
    $issue["device-db"] = readObject("devices", ["id" => $issue["device"]], 1);
    if ($issue["device-db"] != null) {
        $issue["device-name"] = $issue["device-db"]["name"];
    }

    $issue["network-name"] = "";
    $issue["network-db"] = readObject("networks", ["id" => $issue["network"]], 1);
    if ($issue["network-db"] != null) {
        $issue["network-name"] = $issue["network-db"]["name"];
    }

    $issues[$key]["time"] = date('Hi dMy', strtotime($issue["created"]) - 21600);

    head("CadetNet Issue");
    viewIssueBlock($issue);
    foot();

?>