<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["elements","functs"]);

    $auth = authenticate("users");
    $user = $auth["user"];
    if ($user == null) redirect("/user/login?redirect=/support/report/issue");

    $user["devices"] = readObject("devices", ["user" => $user["id"]]);

    head("Report Issue");
    connectionIssueBlock($user);
    foot();
?>