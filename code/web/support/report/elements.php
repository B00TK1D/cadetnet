<?php function issueTypeBlock() { ?>

    <div class="div-block-61">
      <div class="div-block-62"></div>
      <div class="div-block-60">
        <h1 class="heading-18">CADETNET SUPPORT</h1>
        <div class="w-form">
          <form action="/support/report/issue" method="post">
            <label for="squadron" class="cn-label">Issue Type</label>
            <select name="type" class="cn-field _2 w-select" required="">
                <option value="0" style="display:none">Select</option>
                <option value="<?php print(config("issue.type.account")); ?>">Account Issues (verifiation email, etc.)</option>
                <option value="<?php print(config("issue.type.setup")); ?>">Setup Issues (device won't connect after adding, etc.)</option>
                <option value="<?php print(config("issue.type.connection")); ?>">Connectivity Issues (connection dropping, latency, etc)</option>
                <option value="<?php print(config("issue.type.other")); ?>">Other</option>
              </select>
            <input type="submit" value="Next" data-wait="Please wait..." class="submit-button-2 w-button" />
          </form>
        </div>
      </div>
    </div>


<?php }  function accountIssueBlock($networks) { ?>

<div class="div-block-61">
  <div class="div-block-62"></div>
  <div class="div-block-60">
    <h1 class="heading-18">CADETNET SUPPORT</h1>
    <div class="w-form">
      <form action="/api/issue/create" method="post">
        <label for="name" class="cn-label">Name</label>
        <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="" required="" />
        <label for="squadron" class="cn-label">Squadron</label>
        <select name="network" class="cn-field _2 w-select">
            <option value="0" style="display:none">Select Squadron</option>
            <?php foreach ($networks as $network) { ?>
              <option value="<?php print($network['id']); ?>"><?php print($network['name']); ?></option>
            <?php } ?>
          </select>
        <label for="email" class="cn-label">Email Address</label>
        <input type="text" class="cn-field w-input" maxlength="256" name="email" placeholder="" required="" />
        <textarea placeholder="Issue Description" maxlength="4096" name="notes" class="cn-field w-input"></textarea>
        <input name="type" value="<?php print(config("issue.type.account")); ?>" hidden=""/>
        <input name="ip" value="<?php print($_SERVER["HTTP_X_FORWARDED_HOST"]); ?>" hidden=""/>
        <input type="submit" value="Submit" data-wait="Please wait..." class="submit-button-2 w-button" />
      </form>
    </div>
  </div>
</div>

<?php }  function otherIssueBlock($networks) { ?>

<div class="div-block-61">
  <div class="div-block-62"></div>
  <div class="div-block-60">
    <h1 class="heading-18">CADETNET SUPPORT</h1>
    <div class="w-form">
      <form action="/api/issue/create" method="post">
        <label for="name" class="cn-label">Name</label>
        <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="" required="" />
        <label for="network" class="cn-label">Squadron</label>
        <select name="network" class="cn-field _2 w-select">
            <option value="0" style="display:none">Select Squadron</option>
            <?php foreach ($networks as $network) { ?>
              <option value="<?php print($network['id']); ?>"><?php print($network['name']); ?></option>
            <?php } ?>
          </select>
        <label for="email" class="cn-label">Email Address</label>
        <input type="text" class="cn-field w-input" maxlength="256" name="email" placeholder="" required="" />
        <textarea placeholder="Issue Description" maxlength="4096" name="notes" class="cn-field w-input"></textarea>
        <input name="type" value="<?php print(config("issue.type.other")); ?>" hidden=""/>
        <input name="ip" value="<?php print($_SERVER["HTTP_X_FORWARDED_HOST"]); ?>" hidden=""/>
        <input type="submit" value="Submit" data-wait="Please wait..." class="submit-button-2 w-button" />
      </form>
    </div>
  </div>
</div>

<?php }  function setupIssueBlock($user) { ?>

<div class="div-block-61">
  <div class="div-block-62"></div>
  <div class="div-block-60">
    <h1 class="heading-18">CADETNET SUPPORT</h1>
    <div class="w-form">
      <form action="/api/issue/create" method="post">
        <label for="device" class="cn-label">Device</label>
        <select name="device" class="cn-field _2 w-select">
            <option value="0" style="display:none">Select device with issue</option>
            <?php foreach ($user["devices"] as $device) { ?>
              <option value="<?php print($device['id']); ?>"><?php print($device['name']); ?></option>
            <?php } ?>
          </select>
        <textarea placeholder="Issue Description" maxlength="4096" name="notes" class="cn-field w-input"></textarea>
        <input name="user" value="<?php print($user['id']); ?>" hidden=""/>
        <input name="network" value="<?php print($user['network']); ?>" hidden=""/>
        <input name="email" value="<?php print($user['email']); ?>" hidden=""/>
        <input name="name" value="<?php print($user['name']); ?>" hidden=""/>
        <input name="type" value="<?php print(config("issue.type.setup")); ?>" hidden=""/>
        <input name="ip" value="<?php print($_SERVER["HTTP_X_FORWARDED_HOST"]); ?>" hidden=""/>
        <input type="submit" value="Submit" data-wait="Please wait..." class="submit-button-2 w-button" />
      </form>
    </div>
  </div>
</div>

<?php }  function connectionIssueBlock($user) { ?>

<div class="div-block-61">
  <div class="div-block-62"></div>
  <div class="div-block-60">
    <h1 class="heading-18">CADETNET SUPPORT</h1>
    <div class="w-form">
      <form action="/api/issue/create" method="post">
        <label for="device" class="cn-label">Device</label>
        <select name="device" class="cn-field _2 w-select">
        <option value="0" style="display:none">Select device with issue</option>
          <?php foreach ($user["devices"] as $device) { ?>
            <option value="<?php print($device['id']); ?>"><?php print($device['name']); ?></option>
          <?php } ?>
        </select>
        <label for="type" class="cn-label">Description</label>
        <select name="type" class="cn-field _2 w-select">
          <option value="0" style="display:none">Select description</option>
          <option value="<?php print(config("issue.type.latency")); ?>">Latency</option>
          <option value="<?php print(config("issue.type.speed")); ?>">Speed</option>
          <option value="<?php print(config("issue.type.dropping")); ?>">Connection dropping</option>
          <option value="<?php print(config("issue.type.blocking")); ?>">Resource blocked (app not loading, etc.)</option>
          <option value="<?php print(config("issue.type.connection")); ?>">Other</option>
        </select>
        <label for="frequency" class="cn-label">Issue Frequency</label>
        <select name="frequency" class="cn-field _2 w-select">
          <option value="0" style="display:none">Select frequency</option>
          <option value="<?php print(config("issue.frequency.constant")); ?>">Constant (Every 5-10 minutes)</option>
          <option value="<?php print(config("issue.frequency.often")); ?>">Often (every 10-30 minutes)</option>
          <option value="<?php print(config("issue.frequency.sometimes")); ?>">Sometimes (every 1-3 hours)</option>
          <option value="<?php print(config("issue.frequency.rarely")); ?>">Rarely (every few days)</option>
          <option value="<?php print(config("issue.frequency.once")); ?>">Once</option>
        </select>
        <label for="activity" class="cn-label">Activity</label>
        <select name="activity" class="cn-field _2 w-select">
          <option value="0" style="display:none">Activity during issue</option>
          <option value="<?php print(config("issue.activity.browsing")); ?>">Internet Browsing</option>
          <option value="<?php print(config("issue.activity.school")); ?>">School</option>
          <option value="<?php print(config("issue.activity.streaming")); ?>">Streaming (shows, movies, etc.)</option>
          <option value="<?php print(config("issue.activity.gaming")); ?>">Gaming</option>
          <option value="<?php print(config("issue.activity.other")); ?>">Other</option>
        </select>
        <textarea placeholder="More details (optional)" maxlength="4096" name="notes" class="cn-field w-input"></textarea>
        <input name="user" value="<?php print($user['id']); ?>" hidden=""/>
        <input name="network" value="<?php print($user['network']); ?>" hidden=""/>
        <input name="email" value="<?php print($user['email']); ?>" hidden=""/>
        <input name="name" value="<?php print($user['name']); ?>" hidden=""/>
        <input name="ip" value="<?php print($_SERVER["HTTP_X_FORWARDED_HOST"]); ?>" hidden=""/>
        <input type="submit" onclick="this." value="Submit" data-wait="Please wait..." class="submit-button-2 w-button" />
      </form>
    </div>
  </div>
</div>


<?php } ?>