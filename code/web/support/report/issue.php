<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["elements","functs"]);

    //authenticate("open");

    $type = param(["type", false]);

    if ($type) {
        if ($type == config("issue.type.account")) redirect("/support/report/account");
        elseif ($type == config("issue.type.setup")) redirect("/support/report/setup");
        elseif ($type == config("issue.type.connection")) redirect("/support/report/connection");
        elseif ($type == config("issue.type.other")) redirect("/support/report/other");
        die($type);
        //else error();
    }

    head("Report Issue");
    issueTypeBlock();
    foot();

?>