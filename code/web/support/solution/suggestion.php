<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["elements","functs"]);
    includeUtil(["issue"]);

    //$user = authenticate("open");

    $prediction = param("prediction", ["showError" => true, "sticky" => true]);
    $id = param(["issue", 0], ["sticky" => true]);

    $message = getPrediction($prediction)["message"];
    $link = getPrediction($prediction)["link"];

    /*if ($prediction == "connection-instability") {
        $issue = readObject("issues", ["id" => $id], 1);
        if ($issue != null) {
            $device = readObject("devices", ["id" => $issue["device"]], 1);
            if ($device != null) {
                $link .= $device["mac"];
            }
        }
    }*/

    head("CadetNet Suggestion");
    solutionBlock($message, $link);
    foot();
?>