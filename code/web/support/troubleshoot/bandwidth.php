<?php
    include_once('db.php');
    include_once('functs.php');
    include_once('config.php');
    include_once('crypto.php');
    include_once('aruba.php');

    $init = initAdmin();
    if($init != $config["codes"]["good"]) {
      header("Location: /login-admin.php?code=" . $init . "&redirect=" . urlencode($_SERVER['REQUEST_URI']));
      die();
    }

    $admin = select_admin($_SESSION["admin"]);

    if (!isset($_GET["m"])) {
        header("Location: /admin");
        die();
    }
    $mac = sanitize($_GET["m"]);


    $device["db"] = select_device_mac($mac);
    if (count($device["db"]) > 0) {
        $device["db"] = $device["db"][0];
        $device["owner"] = select_user($device["db"]["user"]);
        $device["name"] = $device["db"]["name"];
        $device["user"] = $device["owner"]["name"];
    } else {
        $device["name"] = "Unidentified";
        $device["user"] = "No device found";
    }

    //if ((!isset($device["owner"])) || !($admin["network"] == $device["owner"]["network"])) {
        if (!($admin["network"] == 50)) {
            header("Location: /login-admin.php?code=" . $init . "&redirect=" . urlencode($_SERVER['REQUEST_URI']));
            die();
        }
    ///}


    if (!isset($_GET["l"])) {
        header("Location: /loader.php?redirect=" . urlencode($_SERVER['REQUEST_URI'] . "&l=1"));
        die();
    }


    $client = showClient(list_networks(), $mac);
    $network = 0;

    if (isset($client["data"])) {
        if (is_array($client["data"]["Network"])) {
            $network = $client["data"]["Network"][0];
        } else {
            $network = $client["data"]["Network"];
        }
    }
    

?>




<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>CadetNet Troubleshooting</title>
    <meta content="CadetNet Troubleshooting" property="og:title" />
    <meta content="CadetNet Troubleshooting" property="twitter:title" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <link href="/style.css?hash=<?php print(md5_file('style.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="/highcharts.css?hash=<?php print(md5_file('highcharts.css')); ?>" rel="stylesheet" type="text/css" />
    <script src="/js/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">
      WebFont.load({
        google: {
          families: ["Vollkorn:400,400italic,700,700italic", "Exo:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic", "Roboto Condensed:300,regular,700", "Roboto:300,regular,500"]
        }
      });
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-02Q4GJRYZR"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-02Q4GJRYZR');
    </script>
    <script type="text/javascript">
      ! function(o, c) {
        var n = c.documentElement,
          t = " w-mod-";
        n.className += t + "js", ("ontouchstart" in o || o.DocumentTouch && c instanceof DocumentTouch) && (n.className += t + "touch")
      }(window, document);
    </script>
    <link href="/img/icon%2032.png" rel="shortcut icon" type="image/x-icon" />
    <link href="/img/icon%20256.png" rel="apple-touch-icon" />
  </head>

  <body class="body-2">
    <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar-2 w-nav">
      <div class="w-container"><a href="#" class="brand-2 w-nav-brand"></a>
        <nav role="navigation" class="w-nav-menu"><a href="/admin" class="nav-link-2 w-nav-link">DASHBOARD</a><a href="/logout-admin" class="nav-link-2 w-nav-link">LOGOUT</a></nav>
        <div class="w-nav-button">
          <div class="w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
    <div class="div-block-63"></div>
    <div class="div-block-64">
      <div>
        <h1 class="heading-19">BANDWIDTH TROUBLESHOOTING</h1>
        <div class="div-block-65">
          <div class="text-block-16"><?php print($device["name"] . ' (' . $device["user"] . ')'); ?></div>
          <div class="div-block-66 seperate">
            <div class="cred-holder">
              <figure class="figure">
                <div id="device-bandwidth-container"></div>
                <div id="network-bandwidth-container"></div>
                <div id="global-bandwidth-container"></div>
              </figure>
            </div>
    </div>
        </div>
      </div>
    </div>
    <div class="div-block-68">
      <div>
        <a onclick="window.history.back();" class="cn-button apply-edit-user w-button">BACK</a></div>
      </div>
    </div>



    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script>


    function bps(bits) {
        if (bits == 0) return '';
        var s = ['bps', 'Kbps', 'Mbps', 'Gbps', 'Tbps', 'Pbps'];
        var e = Math.floor(Math.log(bits)/Math.log(1000));
        var value = ((bits/Math.pow(1000, Math.floor(e))).toFixed(2));
        e = (e<0) ? (-e) : e;
        value += ' ' + s[e];
        return value;
    }

    Highcharts.ajax({
        url: 'https://cadetnet.org/api/stats/device/bandwidth/chart?m=<?php print($mac); ?>',
        dataType: 'text',
        success: function (activity) {

            activity = JSON.parse(activity);
            activity.datasets.forEach(function (dataset, i) {


                series = [];
                dataset.series.forEach(function (s, i) {
                    s.data = Highcharts.map(s.data, function (val, j) {
                        return [activity.xData[j], val];
                    });
                    series[i] = {
                        data: s.data,
                        name: s.name,
                        type: s.type,
                        color: "#fa3379",
                        visible: true,
                        tooltip: {
                            valueSuffix: ' ' + s.network,
                            style: {
                                color: "#fa3379"
                            },
                            valueDecimals: s.valueDecimals
                        }
                    };
                });



                var chartDiv = document.createElement('div');
                chartDiv.className = 'chart';
                document.getElementById('device-bandwidth-container').appendChild(chartDiv);


                yAxisFormatter = null;
                tooltipFormatter = null;
                if (dataset.type == "bandwidth") {
                    tooltipFormatter = function() { return bps(this.y) + " " + this.series.name;};
                    yAxisFormatter = function() { return bps(this.value);};
                }

                Highcharts.chart(chartDiv, {
                    chart: {
                        marginLeft: 40, // Keep all charts left aligned
                        spacingTop: 20,
                        spacingBottom: 20,
                        backgroundColor: 'transparent',
                        style:{
                            color: "#fa3379"
                        }
                    },
                    title: {
                        text: dataset.name,
                        align: 'left',
                        margin: 0,
                        x: 30,
                        style:{
                            color: "#fa3379"
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled: true,
                        itemStyle: {
                            color: '#fa3379'
                        },
                            itemHoverStyle: {
                            color: '#fa3379'
                        },
                    },
                    xAxis: {
                        crosshair: {
                            color: "#fa3379"
                        },
                        labels: {
                            format: '{value:%H:%M:%S}',
                            style:{
                                color: "#fa3379"
                            }
                        },
                        style:{
                            color: "#fa3379"
                        }
                    },
                    yAxis: {
                        type: dataset.type,
                        min: dataset.min,
                        max: dataset.max,
                        title: {
                            text: null
                        },
                        labels: {
                            format: '{value}',
                            style:{
                                color: "#fa3379"
                            },
                            width: "40px",
                            formatter: yAxisFormatter
                        }
                    },
                    colors: ['#fa3379'],
                    tooltip: {
                        shared: false,
                        borderWidth: 0,
                        backgroundColor: 'none',
                        pointFormat: '{point.y}',
                        headerFormat: '',
                        shadow: false,
                        style: {
                            fontSize: '18px',
                            color: "#fa3379"
                        },
                        formatter: tooltipFormatter
                    },
                    series: series
                });
            });
        }
    });

    Highcharts.ajax({
        url: 'https://cadetnet.org/api/stats/network/bandwidth/chart?s=<?php print($network); ?>',
        dataType: 'text',
        success: function (activity) {

            activity = JSON.parse(activity);
            activity.datasets.forEach(function (dataset, i) {


                series = [];
                dataset.series.forEach(function (s, i) {
                    s.data = Highcharts.map(s.data, function (val, j) {
                        return [activity.xData[j], val];
                    });
                    series[i] = {
                        data: s.data,
                        name: s.name,
                        type: s.type,
                        color: "#fa3379",
                        visible: true,
                        tooltip: {
                            valueSuffix: ' ' + s.network,
                            style: {
                                color: "#fa3379"
                            },
                            valueDecimals: s.valueDecimals
                        }
                    };
                });



                var chartDiv = document.createElement('div');
                chartDiv.className = 'chart';
                document.getElementById('network-bandwidth-container').appendChild(chartDiv);


                yAxisFormatter = null;
                tooltipFormatter = null;
                if (dataset.type == "bandwidth") {
                    tooltipFormatter = function() { return bps(this.y) + " " + this.series.name;};
                    yAxisFormatter = function() { return bps(this.value);};
                }

                Highcharts.chart(chartDiv, {
                    chart: {
                        marginLeft: 40, // Keep all charts left aligned
                        spacingTop: 20,
                        spacingBottom: 20,
                        backgroundColor: 'transparent',
                        style:{
                            color: "#fa3379"
                        }
                    },
                    title: {
                        text: dataset.name,
                        align: 'left',
                        margin: 0,
                        x: 30,
                        style:{
                            color: "#fa3379"
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled: true,
                        itemStyle: {
                            color: '#fa3379'
                        },
                            itemHoverStyle: {
                            color: '#fa3379'
                        },
                    },
                    xAxis: {
                        crosshair: {
                            color: "#fa3379"
                        },
                        labels: {
                            format: '{value:%H:%M:%S}',
                            style:{
                                color: "#fa3379"
                            }
                        },
                        style:{
                            color: "#fa3379"
                        }
                    },
                    yAxis: {
                        type: dataset.type,
                        min: dataset.min,
                        max: dataset.max,
                        title: {
                            text: null
                        },
                        labels: {
                            format: '{value}',
                            style:{
                                color: "#fa3379"
                            },
                            width: "40px",
                            formatter: yAxisFormatter
                        }
                    },
                    colors: ['#fa3379'],
                    tooltip: {
                        shared: false,
                        borderWidth: 0,
                        backgroundColor: 'none',
                        pointFormat: '{point.y}',
                        headerFormat: '',
                        shadow: false,
                        style: {
                            fontSize: '18px',
                            color: "#fa3379"
                        },
                        formatter: tooltipFormatter
                    },
                    series: series
                });
            });
        }
    });

    Highcharts.ajax({
        url: 'https://cadetnet.org/api/stats/global/bandwidth/chart',
        dataType: 'text',
        success: function (activity) {

            activity = JSON.parse(activity);
            activity.datasets.forEach(function (dataset, i) {


                series = [];
                dataset.series.forEach(function (s, i) {
                    s.data = Highcharts.map(s.data, function (val, j) {
                        return [activity.xData[j], val];
                    });
                    series[i] = {
                        data: s.data,
                        name: s.name,
                        type: s.type,
                        color: "#fa3379",
                        visible: true,
                        tooltip: {
                            valueSuffix: ' ' + s.network,
                            style: {
                                color: "#fa3379"
                            },
                            valueDecimals: s.valueDecimals
                        }
                    };
                });



                var chartDiv = document.createElement('div');
                chartDiv.className = 'chart';
                document.getElementById('global-bandwidth-container').appendChild(chartDiv);


                yAxisFormatter = null;
                tooltipFormatter = null;
                if (dataset.type == "bandwidth") {
                    tooltipFormatter = function() { return bps(this.y) + " " + this.series.name;};
                    yAxisFormatter = function() { return bps(this.value);};
                }

                Highcharts.chart(chartDiv, {
                    chart: {
                        marginLeft: 40, // Keep all charts left aligned
                        spacingTop: 20,
                        spacingBottom: 20,
                        backgroundColor: 'transparent',
                        style:{
                            color: "#fa3379"
                        }
                    },
                    title: {
                        text: dataset.name,
                        align: 'left',
                        margin: 0,
                        x: 30,
                        style:{
                            color: "#fa3379"
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled: true,
                        itemStyle: {
                            color: '#fa3379'
                        },
                            itemHoverStyle: {
                            color: '#fa3379'
                        },
                    },
                    xAxis: {
                        crosshair: {
                            color: "#fa3379"
                        },
                        labels: {
                            format: '{value:%H:%M:%S}',
                            style:{
                                color: "#fa3379"
                            }
                        },
                        style:{
                            color: "#fa3379"
                        }
                    },
                    yAxis: {
                        type: dataset.type,
                        min: dataset.min,
                        max: dataset.max,
                        title: {
                            text: null
                        },
                        labels: {
                            format: '{value}',
                            style:{
                                color: "#fa3379"
                            },
                            width: "40px",
                            formatter: yAxisFormatter
                        }
                    },
                    colors: ['#fa3379'],
                    tooltip: {
                        shared: false,
                        borderWidth: 0,
                        backgroundColor: 'none',
                        pointFormat: '{point.y}',
                        headerFormat: '',
                        shadow: false,
                        style: {
                            fontSize: '18px',
                            color: "#fa3379"
                        },
                        formatter: tooltipFormatter
                    },
                    series: series
                });
            });
        }
    });


    </script>


  </body>
</html>
