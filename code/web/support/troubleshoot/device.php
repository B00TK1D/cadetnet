<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["elements"]);
    includeUtil(["crypto", "aruba", "xml"]);


    $mac = param("m", ["sticky" => true, "showError" => true]);

    

    $device["db"] = readObject("devices", ["mac" => $mac], 1);
    if ($device["db"] == null) error("nonexistent");

    $device["owner"] = readObject("users", ["id" => $device["db"]["user"]], 1);
    if ($device["owner"] == null) error("nonexistent");
    $device["name"] = $device["db"]["name"];
    $device["user"] = $device["owner"]["name"];

    authenticate("owners", ["user" => $device["owner"]["id"]]);

    /*if (!isset($_GET["l"])) {
        header("Location: /utils/loader?redirect=" . urlencode($_SERVER['REQUEST_URI'] . "&l=1"));
        die();
    }*/

    $device["client"] = showClient(readObject("networks"), $mac);
    $device["mac"] = $mac;


    if (isset($device["client"]["data"])) {
        $device["network-name"] = listMergedString($device["client"]["data"]["Network"]);
        $device["ip"] = listMergedString($device["client"]["data"]["IP Address"]);
        $device["ap"] = listMergedString($device["client"]["data"]["Access Point"]);
        $device["ch"] = listMergedString($device["client"]["data"]["Channel"]);
        $device["os"] = listMergedString($device["client"]["data"]["OS"]);
        $device["connectionTime"] = listMergedString($device["client"]["data"]["Connection Time"]);
        $device["stats"] = expandMerged($device["client"]["Swarm Client Stats"]);
    } else {
        $device["network-name"] = "";
        $device["ip"] = "";
        $device["ap"] = "Disconnected";
        $device["ch"] = "Not currently connected";
        $device["os"] = "";
        $device["connectionTime"] = "Disconnected";
        $device["stats"] = [];
    }

    head("Device Troubleshooting");
    troubleshootDeviceBlock($device);
    foot();
?>