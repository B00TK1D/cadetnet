<?php function troubleshootDeviceBlock($device) { ?>


    <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar-2 w-nav">
      <div class="w-container"><a href="#" class="brand-2 w-nav-brand"></a>
        <nav role="navigation" class="w-nav-menu"><a href="/admin" class="nav-link-2 w-nav-link">DASHBOARD</a><a href="/logout-admin" class="nav-link-2 w-nav-link">LOGOUT</a></nav>
        <div class="w-nav-button">
          <div class="w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
    <div class="div-block-63"></div>
    <div class="div-block-64">
      <div>
        <h1 class="heading-19">DEVICE TROUBLESHOOTING</h1>
        <div class="div-block-65">
          <div class="text-block-16"><?php print($device["name"] . ' (' . $device["user"] . ')'); ?></div>
          <div class="div-block-66 seperate">
            <div class="cred-holder">
              <figure class="highcharts-figure">
                <div id="container"></div>
              </figure>
            </div>
          </div>
          <div class="div-block-66 seperate">
            <div class="cred-holder">
              <div class="text-block-16">MAC: <?php print($device["mac"]); ?></div>
              <div class="text-block-16">Network: <?php print($device["network-name"]); ?></div>
              <div class="text-block-16">AP: <?php print($device["ap"]); ?> (CH <?php print($device["ch"]); ?>)</div>
              <div class="text-block-16">IP: <?php print($device["ip"]); ?></div>
              <div class="text-block-16">OS: <?php print($device["os"]); ?></div>
              <div class="text-block-16">Connection Time: <?php print($device["connectionTime"]); ?></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="div-block-68">
      <div>
        <div class="div-block-66"><a href="/dashboard" class="cn-button apply-edit-user w-button">BACK</a></div>
      </div>
    </div>



    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <link href="/highcharts.css?hash=<?php print(md5_file($_SERVER['DOCUMENT_ROOT'] . '/highcharts.css')); ?>" rel="stylesheet" type="text/css" />
    <script>

    ['mousemove', 'touchmove', 'touchstart'].forEach(function (eventType) {
        document.getElementById('container').addEventListener(
            eventType,
            function (e) {
                var chart,
                    point,
                    i,
                    event;

                for (i = 0; i < Highcharts.charts.length; i = i + 1) {
                    chart = Highcharts.charts[i];
                    // Find coordinates within the chart
                    event = chart.pointer.normalize(e);
                    // Get the hovered point
                    chart.series.forEach(function (series) {
                        point = series.searchPoint(event, true);
                        if (point) {
                            point.highlight(e);
                            if (chart.series.length == 1) {
                                point.onMouseOver();
                            }
                        }
                    });
                }
            }
        );
    });


    Highcharts.Pointer.prototype.reset = function () {
        return undefined;
    };


    Highcharts.Point.prototype.highlight = function (event) {
        event = this.series.chart.pointer.normalize(event);
        //this.series.chart.tooltip.refresh(this); // Show the tooltip
        this.series.chart.xAxis[0].drawCrosshair(event, this); // Show the crosshair
    };

    function syncExtremes(e) {
        var thisChart = this.chart;

        if (e.trigger !== 'syncExtremes') { // Prevent feedback loop
            Highcharts.each(Highcharts.charts, function (chart) {
                if (chart !== thisChart) {
                    if (chart.xAxis[0].setExtremes) { // It is null while updating
                        chart.xAxis[0].setExtremes(
                            e.min,
                            e.max,
                            undefined,
                            false,
                            { trigger: 'syncExtremes' }
                        );
                    }
                }
            });
        }
    }

    function bps(bits) {
        if (bits == 0) return '';
        var s = ['bps', 'Kbps', 'Mbps', 'Gbps', 'Tbps', 'Pbps'];
        var e = Math.floor(Math.log(bits)/Math.log(1000));
        var value = ((bits/Math.pow(1000, Math.floor(e))).toFixed(2));
        e = (e<0) ? (-e) : e;
        value += ' ' + s[e];
        return value;
    }

    Highcharts.ajax({
        url: '/api/logs/device/connection/chart?m=<?php print($device["mac"]); ?>',
        dataType: 'text',
        success: function (activity) {

            activity = JSON.parse(activity);
            activity.datasets.forEach(function (dataset, i) {


                series = [];
                dataset.series.forEach(function (s, i) {
                    s.data = Highcharts.map(s.data, function (val, j) {
                        return [activity.xData[j], val];
                    });
                    series[i] = {
                        data: s.data,
                        name: s.name,
                        type: s.type,
                        color: "#fa3379",
                        visible: true,
                        tooltip: {
                            valueSuffix: ' ' + s.network,
                            style: {
                                color: "#fa3379"
                            },
                            valueDecimals: s.valueDecimals
                        }
                    };
                });



                var chartDiv = document.createElement('div');
                chartDiv.className = 'chart';
                document.getElementById('container').appendChild(chartDiv);


                yAxisFormatter = null;
                tooltipFormatter = null;
                if (dataset.type == "bandwidth") {
                    tooltipFormatter = function() { return bps(this.y) + " " + this.series.name;};
                    yAxisFormatter = function() { return bps(this.value);};
                }

                Highcharts.chart(chartDiv, {
                    chart: {
                        marginLeft: 40, // Keep all charts left aligned
                        spacingTop: 20,
                        spacingBottom: 20,
                        backgroundColor: 'transparent',
                        style:{
                            color: "#fa3379"
                        }
                    },
                    title: {
                        text: dataset.name,
                        align: 'left',
                        margin: 0,
                        x: 30,
                        style:{
                            color: "#fa3379"
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled: true,
                        itemStyle: {
                            color: '#fa3379'
                        },
                            itemHoverStyle: {
                            color: '#fa3379'
                        },
                    },
                    xAxis: {
                        visible: false,
                        minorGridLineWidth: 0,
                        gridLineWidt: 0,
                        crosshair: {
                            color: "#fa3379"
                        },
                        events: {
                            setExtremes: syncExtremes
                        },
                        labels: {
                            format: '{value:%H:%M:%S}',
                            style:{
                                color: "#fa3379"
                            }
                        },
                        style:{
                            color: "#fa3379"
                        }
                    },
                    yAxis: {
                        type: dataset.type,
                        min: dataset.min,
                        max: dataset.max,
                        title: {
                            text: null
                        },
                        labels: {
                            format: '{value}',
                            style:{
                                color: "#fa3379"
                            },
                            width: "40px",
                            formatter: yAxisFormatter
                        }
                    },
                    colors: ['#fa3379'],
                    tooltip: {
                        shared: false,
                        borderWidth: 0,
                        backgroundColor: 'none',
                        pointFormat: '{point.y}',
                        headerFormat: '',
                        shadow: false,
                        style: {
                            fontSize: '18px',
                            color: "#fa3379"
                        },
                        formatter: tooltipFormatter
                    },
                    series: series
                });
            });
        }
    });


    </script>


<?php } ?>