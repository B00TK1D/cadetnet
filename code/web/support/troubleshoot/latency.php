<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>CadetNet Troubleshooting</title>
    <meta content="CadetNet Troubleshooting" property="og:title" />
    <meta content="CadetNet Troubleshooting" property="twitter:title" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <link href="/style.css?hash=<?php print(md5_file('style.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="/highcharts.css?hash=<?php print(md5_file('highcharts.css')); ?>" rel="stylesheet" type="text/css" />
    <script src="/js/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">
      WebFont.load({
        google: {
          families: ["Vollkorn:400,400italic,700,700italic", "Exo:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic", "Roboto Condensed:300,regular,700", "Roboto:300,regular,500"]
        }
      });
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-02Q4GJRYZR"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-02Q4GJRYZR');
    </script>
    <script type="text/javascript">
      ! function(o, c) {
        var n = c.documentElement,
          t = " w-mod-";
        n.className += t + "js", ("ontouchstart" in o || o.DocumentTouch && c instanceof DocumentTouch) && (n.className += t + "touch")
      }(window, document);
    </script>
    <link href="/img/icon%2032.png" rel="shortcut icon" type="image/x-icon" />
    <link href="/img/icon%20256.png" rel="apple-touch-icon" />
  </head>

  <body class="body-2">
    <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar-2 w-nav">
      <div class="w-container"><a href="#" class="brand-2 w-nav-brand"></a>
        <nav role="navigation" class="w-nav-menu"><a href="/admin" class="nav-link-2 w-nav-link">DASHBOARD</a><a href="/logout-admin" class="nav-link-2 w-nav-link">LOGOUT</a></nav>
        <div class="w-nav-button">
          <div class="w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
    <div class="div-block-63"></div>
    <div class="div-block-64">
      <div>
        <h1 class="heading-19">LATENCY TROUBLESHOOTING</h1>
        <figure class="highcharts-figure">
            <div id="container"></div>
        </figure>
      </div>
    </div>
    <div class="div-block-68">
      <div>
        <a onclick="window.history.back();" class="cn-button apply-edit-user w-button">BACK</a></div>
      </div>
    </div>

    <script src="https://code.highcharts.com/highcharts.js"></script>

    <script src="/js/jquery.js"></script>


    <script>
        var count = 0;
        Highcharts.chart('container', {
        chart: {
            title: {
                text: 'Latency',
            },
            type: 'spline',
            backgroundColor: 'transparent',
            events: {
                load: function() {
                    chart = this;
                    let outbound = [];
                    let inbound = [];
                    let isp = [];
                    let i = 0;
                    let j = 0;
                    setInterval(function() {
                        $.ajax({
                            url: 'https://cadetnet.org/timestamp?t=' + (new Date().getTime()),
                            success: function(response){
                                str = response.split(",");
                                var now = (new Date().getTime());
                                outbound[i] = (Math.round(((str[1] - str[0])) * 100) / 100); // TCP overhead accounts for 5 ms
                                inbound[i++] = (Math.round(((now - str[1])) * 100) / 100); // TCP overhead accounts for 5 ms
                            }
                        });
                        $.ajax({
                            url: 'https://cadetnet.org/ping?t=' + (new Date().getTime()),
                            success: function(response){
                                var now = (new Date().getTime());
                                isp[j++] = (Math.round(response * 100) / 100);
                            }
                        });
                    }, 100);
                    setInterval( function(){
                        inboundVal = Math.min(...inbound);
                        outboundVal = Math.min(...outbound);
                        totalVal = inboundVal + outboundVal;
                        ispVal = Math.min(...isp);
                        localVal = totalVal - ispVal;
                        console.log("inbound: " + inboundVal + ", outbound: " + outboundVal + ", total: " + totalVal + ", isp: " + ispVal + ", local: " + localVal);
                        let now = Date.now();
                        if (count++ < 90) {
                            chart.series[0].addPoint([now, localVal]);
                            chart.series[1].addPoint([now, ispVal]);
                            chart.series[2].addPoint([now, inboundVal]);
                            chart.series[3].addPoint([now, outboundVal]);
                            chart.series[4].addPoint([now, totalVal]);
                        } else {
                            chart.series[0].addPoint([now, localVal], true, true);
                            chart.series[1].addPoint([now, ispVal], true, true);
                            chart.series[2].addPoint([now, inboundVal], true, true);
                            chart.series[3].addPoint([now, outboundVal], true, true);
                            chart.series[4].addPoint([now, outboundVal + inboundVal], true, true);
                        }
                        outbound = [];
                        inbound = [];
                        local = [];
                        i = 0;
                        j = 0;
                    }, 1000);
                }
            }
        },
        xAxis: {
            type: 'datetime',
            title: {
            text: 'Time',
            },
        },
        yAxis: {
            title: {
                text: "Ping (ms)"
            },
            min: 0
        },
        legend: {
            itemStyle: {
                color: '#fa3379'
            },
            itemStyleHover: {
                color: '#fa3379'
            },
        },
        plotOptions: {
            series: {
                animation: {
                    duration: 200
                }
            }
        },
        colors: ['#9e1d46', '#d6255a', '#fa3379'],
        series: [{
                tooltip: {
                    headerFormat: '<b>{point.y} ms Local</b><br>',
                    pointFormat: '{point.x:%H:%M:%S}'
                },
                name: "Local Latency",
                data: []
            },{
                tooltip: {
                    headerFormat: '<b>{point.y} ms ISP</b><br>',
                    pointFormat: '{point.x:%H:%M:%S}'
                },
                name: "ISP Latency",
                data: []
            },{
                tooltip: {
                    headerFormat: '<b>{point.y} ms Inbound</b><br>',
                    pointFormat: '{point.x:%H:%M:%S}'
                },
                name: "Inbound Latency",
                data: []
            },{
                tooltip: {
                    headerFormat: '<b>{point.y} ms Outbound</b><br>',
                    pointFormat: '{point.x:%H:%M:%S}'
                },
                name: "Outbound Latency",
                data: []
            },{
                tooltip: {
                    headerFormat: '<b>{point.y} ms Total</b><br>',
                    pointFormat: '{point.x:%H:%M:%S}'
                },
                name: "Total Latency",
                data: []
            }]
        });


    </script>

</body>
</html>