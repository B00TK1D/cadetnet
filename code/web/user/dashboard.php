<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["functs", "elements"]);

    $user = authenticate("users")["user"];
    if ($user == null) redirect("/admin/dashboard");
    $user = getUser($user["id"]);

    $mac = "";
    if (!param(["browserAdded", true], ["sticky" => true])) {
        $mac = param("browserMac", ["sticky" => true]);
        setParam("browserAdded", true);
    }

    setParam("used", true);

    head("CadetNet Dashboard", ["SUPPORT" => "/support/report/issue", "LOGOUT" => "/user/logout"]);
    dashboardBlock($user, $mac);
    foot();
?>