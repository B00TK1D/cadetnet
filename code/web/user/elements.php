<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php"); 



    function verifyBlock($token, $message = "") {
?>

    <div class="div-block-61">
      <div class="div-block-62"></div>
      <div class="div-block-60">
        <h1 class="heading-18">CADETNET SIGNUP</h1>
        <div class="cn-label _2">$5 per month. Unlimited devices.</div>
        <div class="w-form">
          <form class="validate-form" id="payment-form" action="/api/user/payment/create" method="post">
            <input type="text" name="token" value="<?php print($token)?>" hidden=""/>
            <div class="w-embed">
              <style>
                .StripeElement {
                  box-sizing: border-box;

                  height: 52px;

                  padding: 13px 13px;

                  border: 0px solid transparent;
                  border-radius: 0px;
                  background-color: white;

                  color: #323232;
                  font-size: 18px;

                  transition: box-shadow 150ms ease;
                }

                .StripeElement--focus {
                  box-shadow: 0 1px 3px 0 #cfd7df;
                }

                .StripeElement--invalid {
                  border-color: #fa755a;
                }

                .StripeElement--webkit-autofill {
                  background-color: #fefde5 !important;
                }
              </style>

              <div class="form-row">
                <div id="card-element">
                  <!-- A Stripe Element will be inserted here. -->
                </div>

                <!-- Used to display form errors. -->
                <div class="cn-label" id="card-errors" role="alert"></div>
              </div>
              <input id="stripe-token" name="stripe-token" hidden>
            </div>
            <div class="pay-coupon-container"><input type="submit" value="PAY" data-wait="Please wait..." id="submit-payment" class="cn-button w-button" /></div>
            <div class="html-embed-5 w-embed w-script">
              <script src="https://js.stripe.com/v3/"></script>


              <script>
                // Create a Stripe client.
                var stripe = Stripe('<?php print($_ENV["STRIPE_PUBLIC_KEY"])?>');

                // Create an instance of Elements.
                var elements = stripe.elements();

                // Custom styling can be passed to options when creating an Element.
                // (Note that this demo uses a wider set of styles than the guide below.)
                var style = {
                  base: {
                    color: '#32325d',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                    '::placeholder': {
                      color: '#aab7c4'
                    }
                  },
                  invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                  }
                };

                // Create an instance of the card Element.
                var card = elements.create('card', {
                  style: style
                });

                // Add an instance of the card Element into the `card-element` <div>.
                card.mount('#card-element');

                // Handle real-time validation errors from the card Element.
                card.addEventListener('change', function(event) {
                  var displayError = document.getElementById('card-errors');
                  if (event.error) {
                    displayError.textContent = event.error.message;
                  } else {
                    displayError.textContent = '';
                  }
                });

                // Handle form submission.
                var button = document.getElementById('submit-payment');
                button.addEventListener('click', function(event) {
                  event.preventDefault();

                  stripe.createToken(card).then(function(result) {
                    if (result.error) {
                      // Inform the user if there was an error.
                      var errorElement = document.getElementById('card-errors');
                      errorElement.textContent = result.error.message;
                    } else {
                      // Send the token to your server.
                      stripeTokenHandler(result.token);
                    }
                  });
                }, {
                  once: false
                });

                // Submit the form with the token ID.
                function stripeTokenHandler(token) {
                  // Insert the token ID into the form so it gets submitted to the server
                  var hiddenInput = document.getElementById('stripe-token');
                  hiddenInput.setAttribute('value', token.id);

                  // Submit the form
                  document.getElementById('payment-form').submit();
                }
              </script>
            </div>
          </form>
        </div>
        <div class="coupon-description"><?php print($message); ?></div>
      </div>
    </div>
    <script>

    $(".validate-form").each(function(index) {  
            var form = this;
            $(".coupon", this).each(function(elem) {
                coupon = this;
                this.addEventListener("input", function (e) {
                    var xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            document.getElementById("coupon-description").innerHTML = this.responseText;
                        }
                    };
                    xhttp.open("GET", "/api/user/payment/coupon/apply?coupon=" + coupon.value + "&token=<?php print($token) ?>", true);
                    xhttp.send();
                });
            });
        });               

        $(".validate-form").keypress(function(e) {
            //Enter key
            if (e.which == 13) {
                return false;
            }
        });
    </script>

<?php

    }

    function notificationBlock() {
      
?>


    <script type="text/javascript" src="/js/notification.js"></script>
    <div class="info-container">
      <div>
        <h1 class="heading-18">NOTIFICATIONS</h1>
      </div>
      <div class="div-block-65">
        <div>
          <div class="text-block-18">Allow Notifications</div>
          <div class="text-block-16"> <br />CadetNet needs to be able to send you notifications in order to resolve issues and provide the best experience possible.<br />‍</div>
          <div class="text-block-16"> <br />Please allow notifications in order to ensure we can communicate with you.<br />‍</div>
        </div>
      </div>
      <div class="div-block-68">
        <div>
          <div class="div-block-66"><a class="cn-button apply-edit-user w-button" id="push-subscription-button">ALLOW NOTIFICATIONS</a></div>
        </div>
      </div>
    </div>
    <div class="footer">
    <div class="div-block-79">
      <div class="text-block-19"></div><a id="next" href="/user/dashboard" class="submit-button-2 _2 w-button">Skip</a>
    </div>
  </div>


<?php

    }

    function loginBlock($code, $redirect) {
      
?>

<div class="div-block-61">
  <div class="div-block-62"></div>
  <div class="div-block-60">
    <h1 class="heading-18">     LOGIN      </h1>
      <form id="login" name="login" action="/user/login" method="post">
        <label for="email" class="cn-label">Email Address</label><input type="email" class="cn-field w-input" maxlength="256" name="email" placeholder="" id="email" required="" />
        <label for="password" class="cn-label">Password</label><input type="password" class="cn-field w-input" maxlength="256" name="password" placeholder="" id="password" required="" />
        <?php if($code == code("invalid-credentials")) { ?>
            <a href="/user/password/request" class="submit-button-2 _3 w-button">Reset Password</a>
        <?php } ?>
        <?php if($redirect) { ?>
            <input type="text" name="redirect" hidden="" value="<?php print($redirect); ?>"/>
        <?php } ?>
        <input type="submit" value="Login" data-wait="Please wait..." class="submit-button-2 w-button" />
      </form>
      <div class="text-block-18">
        <?php print(message($code)); ?>
      </div>
    <div class="footer">
    <div class="div-block-79">
      <div class="text-block-19">"It sucks but at least it's cheap"</div>
    </div>
    <div class="div-block-79">
      <div class="text-block-19">Looking for CadetNet Admin?</div><a href="/admin/login" class="submit-button-2 _2 w-button">Switch</a>
    </div>
  </div>
</div>

<?php

    }


    function dashboardBlock($user, $mac) {
      
?>

  <div class="div-block-63"></div>
    <div class="div-block-64">
      <div>
        <h1 class="heading-19">DEVICES</h1>


        <?php foreach ($user["devices"] as $device) { ?>


        <div class="div-block-65">
          <div class="text-block-16"><?php echo $device["name"]?></div>
          <div class="text-block-16"><?php echo $device["mac"]?></div>
          <div class="div-block-66 action-container"><a class="button-5 edit-device-button w-button"></a><a class="button-5 _2 delete-button w-button"></a>
            <div class="delete-confirmation delete-confirm">
              <div class="div-block-65">
                <div class="text-block-16-copy">Delete device <?php echo $device["name"]?>?</div>
                <div class="div-block-66"><a class="cn-button _2 cancel-delete w-button">CANCEL</a><form action="/api/device/delete" method="post" target="transFrame"><input type="text" name="user" value="<?php print($user['id']); ?>" hidden=""><input type="text" name="device" value="<?php print($device['id']); ?>" hidden=""><input type="submit"  class="cn-button apply-delete w-button" value="DELETE"></form></div>
              </div>
            </div>
          </div>
          <div class="edit-device">
            <div class="form-block w-form">
              <form class="validate-form" name="edit-device" action="/api/device/update" method="post" target="transFrame">
                <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Device Name" required="" value="<?php echo $device['name']?>"/>
                <input type="text" class="cn-field w-input mac-format" name ="mac" maxlength="256" placeholder="MAC" required="" value="<?php echo $device['mac']?>"/>
                <input hidden="" type="text" name="user" value="<?php print($user['id'])?>"/>
                <input hidden="" type="text" name="device" value="<?php print($device['id'])?>"/>
                <div class="div-block-66"><a class="cn-button _2 cancel-edit-device w-button">CANCEL</a><input type="submit" class="cn-button apply-edit-device w-button" value="APPLY"></div>
              </form>
            </div>
          </div>
        </div>

        
        <?php } ?>


        <div class="div-block-65 new-device">
          <div class="form-block w-form">
            <form class="validate-form" name="new-device" action="/api/device/create" method="post" target="transFrame"><input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Device Name" required="" /><input type="text" class="cn-field w-input mac-format" maxlength="256" name="mac" placeholder="MAC" required="" /><input hidden="" type="text" name="user" value="<?php print($user['id'])?>"/>
              <div class="div-block-66"><a class="cn-button _2 cancel-new-device w-button">CANCEL</a><input type="submit" disabled="true" class="cn-button apply-new-device w-button" value="APPLY"></div>
            </form>
          </div>
        </div><a class="div-block-65 hover new-device-button w-inline-block">
          <div class="div-block-66-copy">
            <div class="button-5 _3"></div>
          </div>
        </a>
      </div>

      <?php if($mac != "") {  ?>
        
        <div class="add-current-device current-device">
            <div class="div-block-65 _2">
            <div class="text-block-16-copy">Add current device</div>
            <div class="form-block w-form">
                <form name="add-current-device" action="/api/device/create" method="post" target="transFrame"><input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Device Name" required="" /><input type="text" class="cn-field w-input mac-format" maxlength="256" name="mac" placeholder="MAC" required="" readonly="readonly" value="<?php print($mac); ?>"/><input hidden="" type="text" name="user" value="<?php print($user['id'])?>"/>
                    <div class="div-block-66"><a class="cn-button _2 cancel-current-device w-button">CANCEL</a><input type="submit" class="cn-button apply-current-device w-button"  value="APPLY"></div>
                </form>
            </div>
            </div>
        </div>
      <?php } ?>


      <div class="add-current-device mac-error" id="mac-error">
        <div class="div-block-65 _2">
          <div class="text-block-16-copy">Error - Randomized MAC Address<br />Please disable MAC randomization</div>
          <div class="form-block w-form">
            <div>
              <div class="div-block-66"><a href="#" class="cn-button _2 cancel-mac-error w-button">CANCEL</a><a href="/help/mac" class="cn-button apply-current-device w-button">HELP</a></div>
            </div>
          </div>
        </div>
      </div>


      <div class="div-block-69"></div>
      <div class="div-block-67">
        <h1 class="heading-19">ACCOUNT</h1>
        <div class="div-block-65-copy">
          <div class="text-block-16"><?php print($user["name"]); ?></div>
          <div class="text-block-16"><?php print($user["email"]); ?></div>
          <div class="text-block-16"><?php print($user["network"]["name"]); ?></div>
          <div class="div-block-66 action-container"><a class="button-5 edit-account-button w-button"></a>
          </div>
          <div class="edit-account">
            <form name="edit-user" action="/api/user/update" method="post" target="transFrame">
              <div class="form-block w-form">
                <input type="text" class="cn-field w-input" maxlength="256" name="name" placeholder="Name" required="" value="<?php print($user['name']); ?>"/>
                <select name="network" class="cn-field _2 w-select">
                  <?php foreach ($user["networks"] as $network) { ?>
                    <option <?php if ($user['network']['id'] == $network['id']) print('selected'); ?> value="<?php print($network['id']); ?>"><?php print($network['name']); ?></option>
                  <?php } ?>
                </select>
                <input type="text" name="user" value="<?php print($user['id']); ?>" hidden=""/>
              </div>
              <div class="div-block-66"><a class="cn-button _2 cancel-edit-account w-button">CANCEL</a><input type="submit" class="cn-button apply-edit-account w-button" value="APPLY"/></div>
            </form>
          </div>
        </div>
        <div class="div-block-65-copy">
          <div class="text-block-16">**** **** **** <?php print($user["last4"]); ?></div>
          <div class="text-block-16"><?php print("$" . $user["amountDue"] . " due " . $user["paidThrough"]); ?></div>
          <div class="div-block-66 action-container"><a class="button-5 edit-card-button w-button"></a>
          </div>
          <div class="edit-card">
            <div class="form-block w-form">
              <form id="email-form" name="email-form" action="/api/user/payment/update" method="post" target="transFrame">
                <div class="w-embed">
                  <style>
                    .StripeElement {
                      box-sizing: border-box;

                      height: 52px;

                      padding: 13px 13px;

                      border: 0px solid transparent;
                      border-radius: 0px;
                      background-color: white;

                      color: #323232;
                      font-size: 18px;

                      transition: box-shadow 150ms ease;
                    }

                    .StripeElement--focus {
                      box-shadow: 0 1px 3px 0 #cfd7df;
                    }

                    .StripeElement--invalid {
                      border-color: #fa755a;
                    }

                    .StripeElement--webkit-autofill {
                      background-color: #fefde5 !important;
                    }
                  </style>

                  <div class="form-row">
                    <div id="card-element">
                      <!-- A Stripe Element will be inserted here. -->
                    </div>

                    <!-- Used to display form errors. -->
                    <div id="card-errors" role="alert"></div>
                  </div>
                  <input name="stripe-token" id="stripe-token" hidden="">
                  <input name="user" value="<?php print($user['id']); ?>" hidden="">
                </div>
                <div class="div-block-66"><a class="cn-button _2 cancel-edit-card w-button">CANCEL</a><input type="submit" id="submit-payment" class="cn-button apply-edit-card w-button" value="APPLY"></div>
                <div class="html-embed-5 w-embed w-script">
                  <script src="https://js.stripe.com/v3/"></script>


                  <script>
                    // Create a Stripe client.
                    var stripe = Stripe('<?php print($_ENV["STRIPE_PUBLIC_KEY"])?>');

                    // Create an instance of Elements.
                    var elements = stripe.elements();

                    // Custom styling can be passed to options when creating an Element.
                    // (Note that this demo uses a wider set of styles than the guide below.)
                    var style = {
                      base: {
                        color: '#32325d',
                        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                        fontSmoothing: 'antialiased',
                        fontSize: '16px',
                        '::placeholder': {
                          color: '#aab7c4'
                        }
                      },
                      invalid: {
                        color: '#fa755a',
                        iconColor: '#fa755a'
                      }
                    };

                    // Create an instance of the card Element.
                    var card = elements.create('card', {
                      style: style
                    });

                    // Add an instance of the card Element into the `card-element` <div>.
                    card.mount('#card-element');

                    // Handle real-time validation errors from the card Element.
                    card.addEventListener('change', function(event) {
                      var displayError = document.getElementById('card-errors');
                      if (event.error) {
                        displayError.textContent = event.error.message;
                      } else {
                        displayError.textContent = '';
                      }
                    });

                    // Handle form submission.
                    var button = document.getElementById('submit-payment');
                    button.addEventListener('click', function(event) {
                      event.preventDefault();

                      stripe.createToken(card).then(function(result) {
                        if (result.error) {
                          // Inform the user if there was an error.
                          var errorElement = document.getElementById('card-errors');
                          errorElement.textContent = result.error.message;
                        } else {
                          // Send the token to your server.
                          stripeTokenHandler(result.token);
                        }
                      });
                    }, {
                      once: true
                    });

                    // Submit the form with the token ID.
                    function stripeTokenHandler(token) {
                      // Insert the token ID into the form so it gets submitted to the server
                      var hiddenInput = document.getElementById('stripe-token');
                      hiddenInput.setAttribute('value', token.id);

                      // Submit the form
                      var button = document.getElementById('submit-payment');
                      button.click();
                      setTimeout(function() {
                        location.reload()
                      }, 2000);
                    }
                  </script>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="div-block-65-copy">
          <div class="div-block-75"><a id="submit-payment"  class="cn-button import-users-button w-button">DELETE ACCOUNT</a>
            <div class="import-users">
              <div class="div-block-65">
                <div class="text-block-16-copy">THIS WILL REMOVE ACCESS FOR ALL YOUR DEVICES.</div>
                <div class="text-block-16-copy">CONTINUE?</div>
                <div class="div-block-66"><a class="cn-button _2 cancel-import w-button">CANCEL</a><form action="/api/user/delete" method="post"><input type="text" name="user" value="<?php print($user['id']); ?>" hidden=""><input type="submit" id="delete-user" class="cn-button apply-import w-button" value="DELETE"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="w-embed w-script">
      <script src="/js/jquery.js"></script>

      <script>
        // Edit device
        $(".edit-device-button").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "auto";
            this.parentNode.parentNode.getElementsByClassName("action-container")[0].style.height = "0px";
          });
        });

        $(".cancel-edit-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("action-container")[0].style.height = "auto";
          });
        });

        $(".apply-edit-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-device")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("action-container")[0].style.height = "auto";
          });
        });

        // New device
        $(".new-device-button").each(function(index) {
          this.addEventListener("click", function() {
            this.style.height = "0px";
            this.parentNode.getElementsByClassName("new-device")[0].style.height = "auto";
          });
        });

        $(".cancel-new-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

          });
        });

        $(".apply-new-device").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("new-device-button")[0].style.height = "auto";

          });
        });

        $(".delete-button").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "flex";
          });
        });

        $(".cancel-delete").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
          });
        });

        $(".apply-delete").each(function(index) {
          this.addEventListener("click", function() {
            //this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("delete-confirm")[0].style.display = "none";
          });
        });


        // Edit account
        $(".edit-account-button").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.getElementsByClassName("edit-account")[0].style.height = "auto";
            this.parentNode.parentNode.getElementsByClassName("action-container")[0].style.height = "0px";
          });
        });

        $(".cancel-edit-account").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-account")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("action-container")[0].style.height = "auto";
          });
        });

        $(".apply-edit-account").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-account")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("action-container")[0].style.height = "auto";
          });
        });

        // Edit card
        $(".edit-card-button").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-card")[0].style.height = "auto";
            this.parentNode.parentNode.getElementsByClassName("action-container")[0].style.height = "0px";
          });
        });

        $(".cancel-edit-card").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-card")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("action-container")[0].style.height = "auto";
          });
        });

        $(".apply-edit-card").each(function(index) {
          this.addEventListener("click", function() {
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("edit-card")[0].style.height = "0px";
            this.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("action-container")[0].style.height = "auto";
          });
        });

        $(".import-users-button").each(function(index) {
          this.addEventListener("click", function() {
               this.parentNode.getElementsByClassName("import-users")[0].style.display = "flex";
            });
        });

        $(".cancel-import").each(function(index) {
          this.addEventListener("click", function() {
               this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("import-users")[0].style.display = "none";
            });
        });

        $(".apply-import").each(function(index) {
          this.addEventListener("click", function() {
               this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("import-users")[0].style.display = "none";
            });
        });

        $(".cancel-current-device").each(function(index) {
          this.addEventListener("click", function() {
               this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("current-device")[0].style.display = "none";
            });
        });

        $(".apply-current-device").each(function(index) {
          this.addEventListener("click", function() {
               this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("current-device")[0].style.display = "none";
            });
        });

        $(".cancel-mac-error").each(function(index) {
          this.addEventListener("click", function() {
               this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("mac-error")[0].style.display = "none";
            });
        });

        $("[type=submit]").each(function(index) {
          this.addEventListener("click", function(event) {
            if (this.id != "submit-payment" && this.id != "delete-user" && !this.disabled) {
                setTimeout(function() {
                    location.reload()
                }, 200);
            }
          });
        });


        $(".validate-form").each(function(index) {
            var form = this;
            $(".mac-format", this).each(function(elem) {
                this.setAttribute("minLength", "17");
                this.setAttribute("maxLength", "17");
                this.addEventListener("input", function (e) {
                                        regex = /[a-fA-F0-9]/g;
                    if (this.value.length > 17 || !regex.test(this.value.slice(-1))) {
                        this.value = this.value.slice(0, -1);
                    } 
                    if (this.value.length % 3 == 0 && this.value.length > 0) {
                        this.value = this.value.slice(0, -1) + ":" + this.value.slice(-1);
                    }
                    this.value = this.value.toLowerCase();
                    if (this.value.length == 17) {
                      if (this.value.charAt(1) == '2' || this.value.charAt(1) == '6' ||  this.value.charAt(1) == 'a' ||  this.value.charAt(1) == 'e') {
                        form.elements[form.elements.length - 1].disabled = false;
                        document.getElementById('mac-error').style.display = "flex";
                        this.value = "";
                      } else {
                       form.elements[form.elements.length - 1].disabled = false;
                      }
                    } else {
                        form.elements[form.elements.length - 1].disabled = true;
                    }
                });
            });
        });

      </script>
    </div>
    <div class="div-block-68"></div>
    <script>
        document.cookie = "l=1; expires=Thu, 18 Dec 2025 12:00:00 UTC; path=/";
    </script>





<?php

    }


    function updatePaymentBlock($user, $message) {
      
?>

<div class="div-block-61">
      <div class="div-block-62"></div>
      <div class="div-block-60">
        <h1 class="heading-18">UPDATE PAYMENT</h1>
        <div class="cn-label _2">Your payment information is out of date, please update it to continue</div>
        <div class="w-form">
          <form class="validate-form" id="payment-form" action="/api/user/payment/update" method="post">
            <input type="text" name="user" value="<?php print($user["id"])?>" hidden=""/>
            <div class="w-embed">
              <style>
                .StripeElement {
                  box-sizing: border-box;

                  height: 52px;

                  padding: 13px 13px;

                  border: 0px solid transparent;
                  border-radius: 0px;
                  background-color: white;

                  color: #323232;
                  font-size: 18px;

                  transition: box-shadow 150ms ease;
                }

                .StripeElement--focus {
                  box-shadow: 0 1px 3px 0 #cfd7df;
                }

                .StripeElement--invalid {
                  border-color: #fa755a;
                }

                .StripeElement--webkit-autofill {
                  background-color: #fefde5 !important;
                }
              </style>

              <div class="form-row">
                <div id="card-element">
                  <!-- A Stripe Element will be inserted here. -->
                </div>

                <!-- Used to display form errors. -->
                <div class="cn-label" id="card-errors" role="alert"></div>
              </div>
              <input id="stripe-token" name="stripe-token" hidden>
            </div>
            <div class="pay-coupon-container"><input type="submit" value="PAY" data-wait="Please wait..." id="submit-payment" class="cn-button w-button" /></div>
            <div class="html-embed-5 w-embed w-script">
              <script src="https://js.stripe.com/v3/"></script>


              <script>
                // Create a Stripe client.
                var stripe = Stripe('<?php print($_ENV["STRIPE_PUBLIC_KEY"])?>');

                // Create an instance of Elements.
                var elements = stripe.elements();

                // Custom styling can be passed to options when creating an Element.
                // (Note that this demo uses a wider set of styles than the guide below.)
                var style = {
                  base: {
                    color: '#32325d',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                    '::placeholder': {
                      color: '#aab7c4'
                    }
                  },
                  invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                  }
                };

                // Create an instance of the card Element.
                var card = elements.create('card', {
                  style: style
                });

                // Add an instance of the card Element into the `card-element` <div>.
                card.mount('#card-element');

                // Handle real-time validation errors from the card Element.
                card.addEventListener('change', function(event) {
                  var displayError = document.getElementById('card-errors');
                  if (event.error) {
                    displayError.textContent = event.error.message;
                  } else {
                    displayError.textContent = '';
                  }
                });

                // Handle form submission.
                var button = document.getElementById('submit-payment');
                button.addEventListener('click', function(event) {
                  event.preventDefault();

                  stripe.createToken(card).then(function(result) {
                    if (result.error) {
                      // Inform the user if there was an error.
                      var errorElement = document.getElementById('card-errors');
                      errorElement.textContent = result.error.message;
                    } else {
                      // Send the token to your server.
                      stripeTokenHandler(result.token);
                    }
                  });
                }, {
                  once: false
                });

                // Submit the form with the token ID.
                function stripeTokenHandler(token) {
                  // Insert the token ID into the form so it gets submitted to the server
                  var hiddenInput = document.getElementById('stripe-token');
                  hiddenInput.setAttribute('value', token.id);

                  // Submit the form
                  document.getElementById('payment-form').submit();
                }
              </script>
            </div>
          </form>
        </div>
        <div class="coupon-description"><?php print($message); ?></div>
      </div>
    </div>
    <script>              
        $(".validate-form").keypress(function(e) {
            //Enter key
            if (e.which == 13) {
                return false;
            }
        });
    </script>

<?php

    }
    
?>