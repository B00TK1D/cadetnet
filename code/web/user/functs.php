<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil(["composer", "mail", "token"]);

    function login($email, $password) {
        $user = readObject("users", ["email" => $email], 1);
        if ($user == null) {
            redirect("/user/login?code=" . code("invalid-credentials"));
        }
        if ($user["status"] < config("verification.verified")) {
            redirect("/user/login?code=" . code("unverified"));
        }
        if (checkHash($password, readObject("users", ["id" => $user["id"]], 1)["password"])) {
            $_SESSION["user"] = $user["id"];
            renewTimeout();
            if ($user["status"] == config("verification.payment-error")) {        
                redirect("/user/payment/update");
            }
            return code("good");
        } else {
            redirect("/user/login?code=" . code("invalid-credentials"));
        }
        redirect("/user/login?code=" . code("error"));
    }

    function logout() {
        $_SESSION["user"] = -1;;
        redirect("/user/login?code=" . code("logged-out"));
    }

    function getUser($id) {
        $user = readObject("users", ["id" => $id], 1);
        $user["devices"] = readObject("devices", ["user" => $id]);
        $user["network"] = readObject("networks", ["id" => $user["network"]], 1);

        $stripe = new \Stripe\StripeClient(config("stripe.secret_key"));

        try {
            $invoice = $stripe->invoices->upcoming(["customer" => $user["customer"]]);
            if ($invoice->amount_due % 100 == 0) {
                $user["amountDue"] = $invoice->amount_due / 100;
            } else {
                $user["amountDue"] = number_format(($invoice->amount_due /100), 0, '.', ' ');
            }
    
            $user["paidThrough"] = new DateTime("@$invoice->period_end");
            $user["paidThrough"] = $user["paidThrough"]->format('d M Y');
        } catch (Exception $e) {
            $user["amountDue"] = "0.00";
            $user["paidThrough"] = "(no subscription)";
        }

        $user["networks"] = readObject("networks");
        
        

        return $user;
    }

    function onboardUser($user) {
        if ($user["network"] == 0) {
            die();
        }

        $existingUser = readObject("users", ["email" => $user["email"]], 1);
        if ($existingUser == null) {
            $user["status"] = config("verification.unverified");
            $user["id"] = createObject("users", $user);
            $token = generateToken("verification");
            $token["user"] = $user["id"];
            createObject("tokens", $token);
            $user["token"] = $token["string"];
            $user["link"] = "https://" . config("deployment.domain") . "/user/verify?token=" . $token["string"];
            mailTemplate("verification", $user);
        } else {
            if ($existingUser["status"] == config("verification.verified")) {
                mailTemplate("user-exists", $user);
            } else {
                deleteObject("tokens", ["user" => $existingUser["id"], "purpose" => config("token.verification")]);
                updateObject("users", $user, ["email" => $user["email"]]);
                $token = generateToken("verification");
                $token["user"] = $existingUser["id"];
                createObject("tokens", $token);
                $existingUser["token"] = $token["string"];
                $user["link"] = "https://" . config("deployment.domain") . "/user/verify?token=" . $token["string"];
                mailTemplate("verification", $user);
            }
        }
    }
?>