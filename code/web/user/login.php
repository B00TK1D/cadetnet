<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil(["message", "param"]);
    includeAll(["elements", "functs"]);

    $email = param(["email", false]);
    $password = param(["password", false], ["sanitize" => false]);

    $code = param(["code", false]);
    $redirect = param(["redirect", false]);

    if ($email && $password) {
        login($email, $password);
        if (param("notification-subscribed", ["sticky" => true])) {
            redirect("/user/dashboard", $redirect);
        } else {
            setParam("notification-subscribed", true, true);
            redirect("/user/register", $redirect);
        }
        
    }

    head("CadetNet Login");
    loginBlock($code, $redirect);
    foot();
?>