<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil(["message", "param"]);
    includeAll(["functs"]);

    $user["name"] = param("name");
    $user["email"] = strtolower(param("email"));
    $user["network"] = param("network");
    $user["password"] = getHash(param("password", ["sanitize" => false]));
    $user["coupon"] = param(["coupon", ""], ["sanitize" => false]);

    onboardUser($user);
    redirect("/user/verification/pending");
?>