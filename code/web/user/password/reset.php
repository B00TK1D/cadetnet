<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["functs", "elements"]);
    
    $token = param("token", ["displayError" => true]);

    checkToken($token, "password-reset", true);

    head("Reset Password");
    passwordResetBlock($token);
    foot();
?>