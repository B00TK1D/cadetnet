<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll(["elements"]);
    includeUtil(["message"]);

    $user = authenticate("users")["user"];
    $code = param(["code", ""]);

    head("Update Payment");
    updatePaymentBlock($user, message($code));
    foot();
?>