<?php function verificationPendingBlock() { ?>

    <div class="div-block-61">
      <div class="div-block-62"></div>
      <div class="div-block-60">
        <h1 class="heading-18">     VERIFY EMAIL     </h1>
      </div>
      <div class="text-block-18 centered">Please check your email to activate your account.</br></div>
      <div class="text-block-18 centered">(This may take up to an hour to process, please be patient and also check spam)</div>
    </div>

<?php } ?>