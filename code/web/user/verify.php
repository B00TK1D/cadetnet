<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil(["token", "message", "param"]);
    includeAll(["elements"]);

    $token = param("token", ["displayError" => true]);
    $code = param(["code", ""]);

    checkToken($token, "verification", true);

    head("CadetNet Payment");
    verifyBlock($token, message($code));
    foot();
?>