<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeAll("elements");

    $lines = explode("\n", file_get_contents("loaders.txt"));
    $line = $lines[array_rand($lines)] . "...";

    $redirect = param("redirect");

    head();
    loaderBlock($line);
    redirectBlock($redirect);
    foot();
?>