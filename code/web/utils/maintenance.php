<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8" />
    <title>CadetNet</title>
    <meta content="CadetNet" property="og:title" />
    <meta content="CadetNet" property="twitter:title" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <link href="/style.css" rel="stylesheet" type="text/css" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-02Q4GJRYZR"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-02Q4GJRYZR');
    </script>
    <link href="/img/icon%2032.png" rel="shortcut icon" type="image/x-icon" />
    <link href="/img/icon%20256.png" rel="apple-touch-icon" />
    </head>
    <body class="body-2">
    <div class="div-block-61">
    <div class="div-block-62"></div>
    <div class="div-block-60">
        <h1 class="heading-18">DOWN FOR MAINTAINENCE</h1>
    </div>
    <div class="text-block-18">This site is currently down for maintaince. Check back soon!</div>
    </body>
</html>