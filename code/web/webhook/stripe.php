<?php
    include_once($_SERVER["DOCUMENT_ROOT"] . "/functs.php");
    includeUtil(["composer", "mail"]);

    \Stripe\Stripe::setApiKey($config["stripe"]["secret_key"]);

    $payload = @file_get_contents('php://input');
    $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
    $event = null;

    try {
        $event = \Stripe\Webhook::constructEvent(
            $payload, $sig_header, config("stripe.endpoint_secret")
        );
    } catch(\UnexpectedValueException $e) {
        // Invalid payload
        http_response_code(400);
        exit();
    } catch(\Stripe\Exception\SignatureVerificationException $e) {
        // Invalid signature
        http_response_code(400);
        exit();
    }


    // Handle the event
    switch ($event->type) {
        case "invoice.created":
            $invoice = $event->data->object;
            $user = readObject("users", ["customer" => $invoice->customer], 1);
            $user["amount"] = number_format(($invoice->total /100), 2, '.', ' ');
            $user["link"] = $invoice->invoice_pdf;

            //mailTemplate("invoice", $user);

            break;

        case "issuing_authorization.created":
            $authorization = $event->data->object;
            $amount = $authorization->amount;
            $card = $authorization->card->id;

            $network = readObject("networks", ["card" => $card], 1);

            $network["balance"] = $network["balance"] - $amount;

            updateObject("networks", $network, ["id" => $network["id"]]);

            break;

        case "payment_intent.succeeded":
            $paymentIntent = $event->data->object;
            
            $customer = $paymentIntent->customer;
            $amount = $paymentIntent->amount;
            $user = readObject("users", ["customer" => $customer] , 1);
            $network = readObject("networks", ["id" => $user["network"]], 1);
        
            $network["balance"] = $network["balance"] + ($amount - 55);

            updateObject("networks", $network, ["id" => $network["id"]]);

            break;

        case "charge.failed":
            $charge = $event->data->object;
            $customer = $charge->customer;

            $user = readObject("users", ["customer" => $customer], 1);
            if ($user == null) break;
            $user["status"] = config("verification.payment-error");
            updateObject("users", $user, ["id" => $user["id"]]);

            mailTemplate("payment-failed", $user);
    
            break;


        case "charge.expired":
            $charge = $event->data->object;
            $customer = $charge->customer;

            $user = readObject("users", ["customer" => $customer], 1);

            $stripe = new \Stripe\StripeClient(config("stripe.secret_key"));

            if($user["subscription"] != "") {
                try {
                    $stripe->subscriptions->cancel(
                        $user["subscription"]
                    );
                } catch (\Stripe\Exception\ApiErrorException $e) {

                }
            }

            if($user["customer"] != "") {
                try {
                    $stripe->customers->delete(
                        $user["customer"]
                    );
                } catch (\Stripe\Exception\ApiErrorException $e) {

                }
            }

            mailTemplate("account-terminated", $user);

            deleteObject("users", ["id" => $id]);
            deleteObject("devices", ["user" => $id]);
            deleteObject("notification_endpoints", ["user" => $id]);


        default:
            echo 'Received unknown event type ' . $event->type;
    }

    http_response_code(200);
?>