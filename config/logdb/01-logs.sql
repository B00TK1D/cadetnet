--CREATE TABLE IF NOT EXISTS `pings` (
 --`id` int(11) NOT NULL AUTO_INCREMENT,
 --`ip` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
 --`avg` int(6) NOT NULL DEFAULT '0',
 --`max` int(6) NOT NULL DEFAULT '0',
 --`timestamp` timestamp DEFAULT now() NOT NULL,
 --`updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
 --PRIMARY KEY (`id`)
--) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `generic` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `type` int(6) NOT NULL DEFAULT '0',
 `target` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
 `source` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
 `data` int(11) NOT NULL DEFAULT '0',
 `string` varchar(256) NOT NULL DEFAULT '0',
 `span` int(11) NOT NULL DEFAULT '0',
 `starttime` timestamp DEFAULT now() NOT NULL,
 `endtime` timestamp DEFAULT now() NOT NULL,
 `timestamp` timestamp DEFAULT now() NOT NULL,
 `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `clients` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `mac` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' UNIQUE,
 `type` int(6) NOT NULL DEFAULT '',
 `network` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
 `ip` varchar(16) NOT NULL DEFAULT '0',
 `os` varchar(32) NOT NULL DEFAULT '0',
 `essid` varchar(64) NOT NULL DEFAULT '0',
 `ap` varchar(256) NOT NULL DEFAULT '0',
 `ch` varchar(5) NOT NULL DEFAULT '0',
 `type` varchar(256) NOT NULL DEFAULT '0',
 `role` varchar(256) NOT NULL DEFAULT '0',
 `ipv6` varchar(64) NOT NULL DEFAULT '0',
 `signal` int(4) NOT NULL DEFAULT '0',
 `speed` int(4) NOT NULL DEFAULT '0',
 `created` timestamp DEFAULT now() NOT NULL,
 `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;