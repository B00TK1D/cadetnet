-- -----------  User data --------------
CREATE TABLE IF NOT EXISTS `users` (
  -- Holds all users that go through signup process
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `network` int(16) NOT NULL DEFAULT '-1',
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` int(16) NOT NULL DEFAULT '-1',
  `password` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `subscription` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `customer` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last4` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `coupon` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS `admins` (
  -- Holds all admins (which are seperate from users - can be duplicate emails, but are seperate accounts)  Also used for RADIUS authentication (admin RADIUS)
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `network` int(3) NOT NULL DEFAULT '-1',
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(16) NOT NULL DEFAULT '-1',
  `tier` int(16) NOT NULL DEFAULT '-1',
  `password` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS `devices` (
  -- Holds all device info, linked to users.  Also used for RADIUS authentcation (device RADIUS)
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `user` int(16) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `mac` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `os` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS `tokens` (
  -- Holds all tokens, such as signup, password reset, etc.
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `string` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `user` int(16) NOT NULL DEFAULT '-1',
  `purpose` int(16) NOT NULL DEFAULT '-1',
  `expiration` datetime NOT NULL,
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS `networks` (
  -- Correlates to squads generally, each network has a dedicated card from Stripe issuing, and usually one modem assigned to it
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `card` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ip` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `floor` int(4) NOT NULL DEFAULT '0',
  `balance` int(16) NOT NULL DEFAULT '0',
  `creator` int(16) NOT NULL DEFAULT '-1',
  `updator` int(16) NOT NULL DEFAULT '-1',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS `credentials` (
  -- Holds encrypted credentials for central management
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `network` int(16) NOT NULL DEFAULT '-1',
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cred` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `creator` int(16) NOT NULL DEFAULT '-1',
  `updator` int(16) NOT NULL DEFAULT '-1',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- ------------  Equipment data --------------
CREATE TABLE IF NOT EXISTS `equipment` (
  -- Holds details about all equpiment tagged with a QR code sticker, including location data for map placement
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `tag` int(16) NOT NULL DEFAULT '-1',
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `model` int(16) NOT NULL DEFAULT '-1',
  `notes` varchar(4096) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `network` int(16) NOT NULL DEFAULT '-1',
  `ip` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mac` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sn` varchar(128) NOT NULL DEFAULT '-1',
  `lat` float(10, 6) NOT NULL DEFAULT '-1',
  `lon` float(10, 6) NOT NULL DEFAULT '-1',
  `floor` int(4) NOT NULL DEFAULT '-1',
  `creds` int(16) NOT NULL DEFAULT '-1',
  `type` int(16) NOT NULL DEFAULT '-1',
  `creator` int(16) NOT NULL DEFAULT '-1',
  `updator` int(16) NOT NULL DEFAULT '-1',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS `connections` (
  -- Holds connections between equipment (i.e. cables)
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `type` int(16) NOT NULL DEFAULT '-1',
  `network` int(16) NOT NULL DEFAULT '-1',
  `parent` int(16) NOT NULL DEFAULT '-1',
  `child` int(16) NOT NULL DEFAULT '-1',
  `length` int(16) NOT NULL DEFAULT '-1',
  `notes` varchar(4096) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `creator` int(16) NOT NULL DEFAULT '-1',
  `updator` int(16) NOT NULL DEFAULT '-1',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS `models` (
  -- Holds equipment models used in equipment table
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `type` int(16) NOT NULL DEFAULT '-1',
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `vendor` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cost` int(16) NOT NULL DEFAULT '0',
  `radius` int(16) NOT NULL DEFAULT '0',
  `notes` varchar(4096) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `creator` int(16) NOT NULL DEFAULT '-1',
  `updator` int(16) NOT NULL DEFAULT '-1',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- ------------  Troubleshooting data --------------
CREATE TABLE IF NOT EXISTS `issues` (
  -- Holds user-submitted issues, including predicted and admin-identified causes
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `user` int(16) NOT NULL DEFAULT '-1',
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `network` int(16) NOT NULL DEFAULT '-1',
  `priority` int(16) NOT NULL DEFAULT '-1',
  `type` int(16) NOT NULL DEFAULT '-1',
  `notes` varchar(4096) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `time` timestamp DEFAULT now() NOT NULL,
  `duration` int(16) NOT NULL DEFAULT '-1',
  `frequency` int(16) NOT NULL DEFAULT '-1',
  `activity` int(16) NOT NULL DEFAULT '-1',
  `device` int(16) NOT NULL DEFAULT '-1',
  `ap` int(16) NOT NULL DEFAULT '-1',
  `modem` int(16) NOT NULL DEFAULT '-1',
  `ip` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `admin` int(16) NOT NULL DEFAULT '-1',
  `status` int(16) NOT NULL DEFAULT '-1',
  `prediction` int(16) NOT NULL DEFAULT '-1',
  `cause` int(16) NOT NULL DEFAULT '-1',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS `causes` (
  -- Holds all known issue causes, including symptoms and solutions.  Eventually will allow auto-correction
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` int(16) NOT NULL DEFAULT '-1',
  `problem` varchar(4096) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `solution` varchar(4096) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `creator` int(16) NOT NULL DEFAULT '-1',
  `updator` int(16) NOT NULL DEFAULT '-1',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS `messages` (
  -- Holds all admin-user troubleshooting chat messages
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `user` int(16) NOT NULL DEFAULT '-1',
  `admin` int(16) NOT NULL DEFAULT '-1',
  `issue` int(16) NOT NULL DEFAULT '-1',
  `status` int(16) NOT NULL DEFAULT '-1',
  `message` varchar(4096) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS `notification_endpoints` (
  -- Holds all push notification endpoints
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `user` int(16) NOT NULL DEFAULT '-1',
  `device` int(16) NOT NULL DEFAULT '-1',
  `endpoint` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `auth` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `p256dh` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS `notifications` (
  -- Holds all push notification endpoints
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `redirect` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` int(16) NOT NULL DEFAULT '-1',
  `endpoint` int(16) NOT NULL DEFAULT '-1',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS `alerts` (
  -- Holds all alerts (such as maintainence alerts) which are pushed out from admins to users of varius scopes.  Scopes left at 0 will be ignored.
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `admin` int(16) NOT NULL DEFAULT '-1',
  `message` varchar(4096) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `device` int(16) NOT NULL DEFAULT '-1',
  `user` int(16) NOT NULL DEFAULT '-1',
  `ap` int(16) NOT NULL DEFAULT '-1',
  `modem` int(16) NOT NULL DEFAULT '-1',
  `network` int(16) NOT NULL DEFAULT '-1',
  `status` int(16) NOT NULL DEFAULT '-1',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS `alert_responses` (
  -- Holds responses to alerts, if solicited (such as if an alert is pushed out asking a question like "Are you having latency issues?", this tracks responses)
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `alert` int(16) NOT NULL DEFAULT '-1',
  `response` int(16) NOT NULL DEFAULT '-1',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- ------------  Log data --------------
CREATE TABLE IF NOT EXISTS `events` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `time` timestamp DEFAULT now() NOT NULL,
  `code` int(16) NOT NULL DEFAULT '-1',
  `user` int(16) NOT NULL DEFAULT '-1',
  `admin` int(16) NOT NULL DEFAULT '-1',
  `device` int(16) NOT NULL DEFAULT '-1',
  `ap` int(16) NOT NULL DEFAULT '-1',
  `modem` int(16) NOT NULL DEFAULT '-1',
  `network` int(16) NOT NULL DEFAULT '-1',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS `modem_logs` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `time` timestamp DEFAULT now() NOT NULL,
  `modem` int(16) NOT NULL DEFAULT '-1',
  `status` int(16) NOT NULL DEFAULT '-1',
  `latency` int(16) NOT NULL DEFAULT '-1',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS `ap_logs` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `time` timestamp DEFAULT now() NOT NULL,
  `ap` int(16) NOT NULL DEFAULT '-1',
  `status` int(16) NOT NULL DEFAULT '-1',
  `neighboring_aps_valid` int(16) NOT NULL DEFAULT '-1',
  `neighboring_aps_interfering` int(16) NOT NULL DEFAULT '-1',
  `neighboring_aps_rogue` int(16) NOT NULL DEFAULT '-1',
  `neighboring_clients_valid` int(16) NOT NULL DEFAULT '-1',
  `neighboring_clients_interfering` int(16) NOT NULL DEFAULT '-1',
  `cpu` int(16) NOT NULL DEFAULT '-1',
  `memory` int(16) NOT NULL DEFAULT '-1',
  `clients` int(16) NOT NULL DEFAULT '-1',
  `throughput_out` int(16) NOT NULL DEFAULT '-1',
  `throughput_in` int(16) NOT NULL DEFAULT '-1',
  `24_power` int(16) NOT NULL DEFAULT '-1',
  `24_channel` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `24_utilization` int(16) NOT NULL DEFAULT '-1',
  `24_noise` int(16) NOT NULL DEFAULT '-1',
  `24_frames_in` int(16) NOT NULL DEFAULT '-1',
  `24_frames_out` int(16) NOT NULL DEFAULT '-1',
  `24_management_in` int(16) NOT NULL DEFAULT '-1',
  `24_management_out` int(16) NOT NULL DEFAULT '-1',
  `24_drops` int(16) NOT NULL DEFAULT '-1',
  `24_errors` int(16) NOT NULL DEFAULT '-1',
  `50_channel` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `50_power` int(16) NOT NULL DEFAULT '-1',
  `50_utilization` int(16) NOT NULL DEFAULT '-1',
  `50_noise` int(16) NOT NULL DEFAULT '-1',
  `50_frames_in` int(16) NOT NULL DEFAULT '-1',
  `50_frames_out` int(16) NOT NULL DEFAULT '-1',
  `50_management_in` int(16) NOT NULL DEFAULT '-1',
  `50_management_out` int(16) NOT NULL DEFAULT '-1',
  `50_drops` int(16) NOT NULL DEFAULT '-1',
  `50_errors` int(16) NOT NULL DEFAULT '-1',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS `device_logs` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `time` timestamp DEFAULT now() NOT NULL,
  `device` int(16) NOT NULL DEFAULT '-1',
  `status` int(16) NOT NULL DEFAULT '-1',
  `network` int(16) NOT NULL DEFAULT '-1',
  `modem` int(16) NOT NULL DEFAULT '-1',
  `ap` int(16) NOT NULL DEFAULT '-1',
  `channel` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cm` int(16) NOT NULL DEFAULT '-1',
  `frames_in` int(16) NOT NULL DEFAULT '-1',
  `frames_out` int(16) NOT NULL DEFAULT '-1',
  `errors_in` int(16) NOT NULL DEFAULT '-1',
  `errors_out` int(16) NOT NULL DEFAULT '-1',
  `throughput_in` int(16) NOT NULL DEFAULT '-1',
  `throughput_out` int(16) NOT NULL DEFAULT '-1',
  `signal` int(16) NOT NULL DEFAULT '-1',
  `speed` int(16) NOT NULL DEFAULT '-1',
  `connection_time` int(16) NOT NULL DEFAULT '-1',
  `created` timestamp DEFAULT now() NOT NULL,
  `updated` timestamp DEFAULT now() ON UPDATE now() NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;