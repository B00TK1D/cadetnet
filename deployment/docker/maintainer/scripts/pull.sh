#!/bin/bash

export $(egrep -v '^#' /etc/build/.env | xargs)

git clone -b $DEPLOYMENT_BRANCH --single-branch https://gitlab.com/B00TK1D/cadetnet.git/ /etc/build
git fetch origin/$DEPLOYMENT_BRANCH
git reset --hard origin/$DEPLOYMENT_BRANCH
git -C /etc/build pull --ff-only origin $DEPLOYMENT_BRANCH

chmod +x /var/opt/webhookd/scripts/pull.sh