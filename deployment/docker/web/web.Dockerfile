FROM php:apache


RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli

RUN apt update && apt install -y git zip

RUN php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer

RUN apt-get install -y libgmp-dev re2c libmhash-dev libmcrypt-dev file
RUN ln -s /usr/include/x86_64-linux-gnu/gmp.h /usr/local/include/
RUN docker-php-ext-configure gmp 
RUN docker-php-ext-install gmp

RUN composer require stripe/stripe-php
RUN composer require sendgrid/sendgrid
RUN composer require minishlink/web-push

RUN apt install -y iputils-ping

RUN apt install -y python3 python3-venv libaugeas0
RUN python3 -m venv /opt/certbot/
RUN /opt/certbot/bin/pip install --upgrade pip
RUN /opt/certbot/bin/pip install certbot certbot-apache

RUN ln -s /opt/certbot/bin/certbot /usr/bin/certbot
#RUN certbot --apache --non-interactive --agree-tos -m stearns.josiah@gmail.com --domains cadetnet.org

RUN a2enmod rewrite

#RUN a2dismod mpm_prefork
#RUN a2enmod mpm_worker

#RUN printf "<IfModule mpm_worker_module> \n\
#ServerLimit 250 \n\
#StartServers 10 \n\
#MinSpareThreads 75 \n\
#MaxSpareThreads 250 \n\
#ThreadLimit 64 \n\
#ThreadsPerChild 32 \n\
#MaxRequestWorkers 8000 \n\
#MaxConnectionsPerChild 10000 \n\
#</IfModule> \n\
#" > /etc/apache2/mods-available/mpm_worker.conf

#RUN echo "0 0,12 * * * root /opt/certbot/bin/python -c 'import random; import time; time.sleep(random.random() * 3600)' && certbot renew -q" | tee -a /etc/crontab > /dev/null