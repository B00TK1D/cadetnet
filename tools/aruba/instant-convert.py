import serial
import time


# TFTP Server IP
server_ip = '10.0.50.2'

# Firmware file name
firmware_file = 'iap335-6'

# Time delay to allow AP to boot
boot_delay = 8


# Calculate md5sum of serial number
def sha1(serial):
    import hashlib
    m = hashlib.sha1()
    m.update(serial.encode('utf-8'))
    return m.hexdigest()


def waitInterrupt(ser):
    buff = ''
    while True:
        chr = ser.read(256).decode('utf-8')
        print(chr, end = '')
        buff += chr
        if 'Hit <Enter> to stop autoboot:' in buff:
            break


def waitPreboot(ser):
    buff = ''
    while True:
        chr = ser.read(256).decode('utf-8')
        print(chr, end = '')
        buff += chr
        if 'apboot>' in buff:
            break


def clearSerial(ser):
    ser.write(b'\r\n')
    ser.read(ser.inWaiting()).decode('utf-8')

def sendCommand(ser, command):
    clearSerial(ser)
    waitPreboot(ser)
    ser.write(command.encode('utf-8'))
    ser.write(b'\r\n')


def hangOutput(ser):
    while True:
        print(ser.readline().decode('utf-8'), end='')


def main():

    # Get server ip address
    #server_ip = input('Enter firmware server ip address: ')

    # Get firmware file name
    #firmware_file = input('Enter firmware file name: ')

    #print('\n\n\n')
    print("***** Starting...")

    # Open the serial port
    ser = serial.Serial('/dev/tty.usbserial-10', 9600, timeout=1)


    # Wait for AP to start outputing over serial
    waitInterrupt(ser)  

    time.sleep(1)   
    print("***** AP Ready")


    # Interrupt boot sequence
    ser.write(b'\r\n')

    time.sleep(1)

    ser.write('mfginfo\r\n'.encode('utf-8'))

    # Print all output from serial port to console while there is output
    line = ser.readline().decode('utf-8')
    while 'Serial' not in line:
        print(line, end='')
        line = ser.readline().decode('utf-8')
        time.sleep(0.01)
    serial_number = line.split(": ")[1].split("\r")[0]

    print("***** Serial number: [" + serial_number + "]")

    # Calculate md5sum of serial number
    serial_sha1 = sha1('US-' + serial_number)

    # Send convert commands
    sendCommand(ser, 'proginv system ccode CCODE-US-' + serial_sha1)
    sendCommand(ser, 'invent -w')
    sendCommand(ser, 'dhcp')
    sendCommand(ser, 'setenv serverip ' + server_ip)
    sendCommand(ser, 'upgrade os 0 ' + firmware_file)
    sendCommand(ser, 'upgrade os 1 ' + firmware_file)
    sendCommand(ser, 'factory_reset')
    sendCommand(ser, 'saveenv')
    sendCommand(ser, 'reset')

    print("***** Done!")        
        

if __name__ == '__main__':
    while True:
        main()
        time.sleep(12)