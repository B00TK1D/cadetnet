import serial
import time


# TFTP Server IP
server_ip = '192.168.2.1'

# Firmware file name
firmware_file = 'iap335-6'

# Time delay to allow AP to boot
boot_delay = 8


# Calculate md5sum of serial number
def md5(serial):
    import hashlib
    m = hashlib.md5()
    m.update(serial.encode('utf-8'))
    return m.hexdigest()


def waitPreboot(ser):
    buff = ''
    while True:
        chr = ser.read(256).decode('utf-8')
        print(chr, end = '')
        buff += chr
        if 'apboot>' in buff:
            break

def clearSerial(ser):
    ser.write(b'\r\n')
    ser.read(ser.inWaiting()).decode('utf-8')

def sendCommand(ser, command):
    clearSerial(ser)
    waitPreboot(ser)
    ser.write(command.encode('utf-8'))
    ser.write(b'\r\n')


def hangOutput(ser):
    while True:
        print(ser.readline().decode('utf-8'), end='')


def main():

    # Get server ip address
    #server_ip = input('Enter firmware server ip address: ')

    # Get firmware file name
    #firmware_file = input('Enter firmware file name: ')

    #print('\n\n\n')
    print("Starting...")

    # Open the serial port
    ser = serial.Serial('/dev/tty.usbserial-10', 9600, timeout=1)

    # repeat forever
    while True:

        # Wait for AP to start outputing over serial
        while 'FIPS: passed' not in ser.readline().decode('utf-8'):
            time.sleep(0.01)        

        # Interrupt boot sequence
        ser.write(b'\r\n')

        time.sleep(1)

        ser.write('mfginfo\r\n'.encode('utf-8'))

        # Print all output from serial port to console while there is output
        while 'Date Code' not in ser.readline().decode('utf-8'):
            time.sleep(0.01)
        serial_number = ser.readline().decode('utf-8').split(": ")[1].split("\r")[0]

        print("Serial number: [" + serial_number + "]")

        # Calculate md5sum of serial number
        md5sum = md5(serial_number)

        hangOutput(ser)
        
        

if __name__ == '__main__':
    main()


# Check output
